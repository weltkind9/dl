def gcd(a, b):
    if a == 0: return b
    if b == 0: return a
    if a > b: return gcd(a % b, b)
    return gcd(b % a, a)
nod = 40
nok = 48000
res =[]
for i in range(1,10000):
    for j in range(1,10000):
        d = gcd(i, j)
        if d != nod:
            continue
        l = i*j/d
        if l != nok:
            continue
        res.append((i, j))

print(res)
