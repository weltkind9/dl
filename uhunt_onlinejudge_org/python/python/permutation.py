def swp(arr, i , j):
    t = arr[i]
    arr[i] = arr[j]
    arr[j] = t

def f(res, cur, arr, n, ch):
    if len(cur) == n:
        res.add(int(''.join(cur)))
        return
    for i in range(len(arr)):
        if ch[i]: continue
        ch[i] = True
        cur.append(arr[i])    
        f(res, cur, arr, n, ch)
        ch[i] = False        
        cur.pop()

d = ['2','1', '1', '1', '1', '9', '9', '7']#['1', '2', '3', '4', '5', '6','7','8']

res = set()
#res.add(int(''.join(d)))
ch = [False for i in range(len(d))]
f(res, [], d, len(d), ch)

print(len(res))