import time
import random

def generate_test():  
    ss = []
    n = 1000
    for _ in range(n):
        ss.append(str(random.randint(15, 20)))
    with open(__file__.replace('.py','t.txt'), 'w') as write:
        write.write(str(n) + '\n' + ' '.join(ss))

# Сортировка выбором (Selection Sort)
def main(input, print):    
    n = int(input())
    arr = list(map(int, input().split()))
    
    for i in range(n - 1):
        index_min_elem = i + 1
        for j in range(i + 1, n):
            if arr[index_min_elem] > arr[j]:
                index_min_elem = j

        if arr[i] > arr[index_min_elem]:
            tmp = arr[i]
            arr[i] = arr[index_min_elem]
            arr[index_min_elem] = tmp

    print(' '.join(map(str, arr)))

CREATE_TEST = False

if CREATE_TEST:
    generate_test()
else:
    t1 = time.time()
    #main(input, print)
    with open(__file__.replace('.py','t.txt')) as read:
        with open(__file__.replace('.py','_ans.txt'), 'w') as write:
            #main(read.readline, write.write)
            main(read.readline, print)
            #main(input, print)
    print(time.time() - t1)