import time
import math
import random


class InverseCounter:
    def __init__(self):
        self.count = 0


def generate_test():
    ss = []
    n = 100000
    for _ in range(n):
        ss.append(str(random.randint(100, 200)))
    with open(__file__.replace('.py','t.txt'), 'w') as write:
        write.write(str(n) + '\n' + ' '.join(ss))


def merge(a: list, b: list, counter: InverseCounter) -> list:
    n = len(a)
    m = len(b)
    merged_arr = [0] * (n + m)
    i = 0
    j = 0

    while i + j < n + m:
        if j == m or (i < n and a[i] <= b[j]):
            merged_arr[i + j] = a[i]
            i += 1
        else:
            if i < n and a[i] > b[j]:
                counter.count += n - i

            merged_arr[i + j] = b[j]
            j += 1

    return merged_arr


def merge_sort(arr: list, counter: InverseCounter) -> list:
    if len(arr) == 1:
        return arr

    halfIndex = len(arr) // 2
    left = arr[ : halfIndex]
    left = merge_sort(left, counter)

    right = arr[halfIndex : ]
    right = merge_sort(right, counter)
    
    return merge(left, right, counter)


def main(input, print):
    n = int(input())
    arr = list(map(int, input().split()))

    counter = InverseCounter()
    merge_sort(arr, counter)

    print(str(counter.count))



t1 = time.time()


#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)
        #generate_test()
        #main(input, print)
        #pass
print(time.time() - t1)