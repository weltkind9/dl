import time
import math
import random

def generate_test():
    ss = []
    n = 100000
    for _ in range(n):
        ss.append(str(random.randint(100, 200)))
    with open(__file__.replace('.py','t.txt'), 'w') as write:
        write.write(str(n) + '\n' + ' '.join(ss))

#generate_test()

def split(arr: list, left: int, right: int) -> int:
    #x = arr[left]
    rndIndex = random.randrange(left, right)
    x = arr[rndIndex]
    arr[rndIndex], arr[left] = arr[left], arr[rndIndex]
    m = left
    for i in range(left + 1, right):
        if arr[i] < x:
            m += 1
            arr[i], arr[m] = arr[m], arr[i]
    arr[left], arr[m] =  arr[m], arr[left]
    return m


def split3(arr: list, left: int, right: int, indexSplitElement: int):
    x = arr[indexSplitElement]
    arr[indexSplitElement], arr[left] = arr[left], arr[indexSplitElement]
    m1 = left
    m2 = left

    for i in range(left + 1, right):
        if arr[i] == x:
            m2 += 1
            arr[i] = arr[m2]
            arr[m2] = x

        elif arr[i] < x:
            m2 += 1
            arr[i], arr[m2] = arr[m2], arr[i]
            arr[m1] = arr[m2]
            arr[m2] = x
            m1 += 1

    return (m1, m2)

def test(arr, ind):
    print()
    print()
    print(ind)
    print(arr)
    (m1,m2) = split3(arr, 0, len(arr), ind)
    print( arr, m1,m2,)

test([1,2,3,4,5,6], 2,  )
test([1,2,3,4,5,6], 3,  )
test([6,5,4,3,2,1], 2,  )
test([6,5,4,3,2,1], 3,  )

test([1,2,3,4,5,6], 0,  )
test([1,2,3,4,5,6], 5,  )
test([6,5,4,3,2,1], 0,  )
test([6,5,4,3,2,1], 5,  )
test([2,2,3,4,1,1,2,2,1], 0)

def quick_sort(arr: list, left: int, right: int) -> None:
    if right - left <= 1:
        return
    
    m = split(arr, left, right)
    quick_sort(arr, left, m)
    quick_sort(arr, m + 1, right)
 
def quick_sort2(arr: list, left: int, right: int) -> None:
    while left < right:
        if right - left <= 1:
            return
    
        m = split(arr, left, right)
        quick_sort2(arr, left, m)
        #quick_sort2(arr, m + 1, right)
        l = m + 1


def quick_sort3(arr: list, left: int, right: int) -> None:
    if right - left <= 1:
        return
    #rndIndex = right-1#random.randrange(left, right)
    
    rndIndex = random.randrange(left, right)
    m1, m2 = split3(arr, left, right, rndIndex)
    quick_sort3(arr, left, m1)
    quick_sort3(arr, m2 + 1, right)
    
        
def main(input, print):
    n = int(input())
    arr = list(map(int, input().split()))
 
    quick_sort3(arr, 0, n)
    print(' '.join(map(str, arr)))
 
 
t1 = time.time()

with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)
        #generate_test()
        #main(input, print)
        #pass
print(time.time() - t1)