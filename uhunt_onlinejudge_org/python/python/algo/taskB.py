import time
import math
import random

def generate_test():
    ss = []
    n = 100000
    for _ in range(n):
        ss.append(str(random.randint(100, 200)))
    with open(__file__.replace('.py','t.txt'), 'w') as write:
        write.write(str(n) + '\n' + ' '.join(ss))


def merge(a: list, b: list) -> list:
    n = len(a)
    m = len(b)
    merged_arr = [0] * (n + m)
    i = 0
    j = 0

    while i + j < n + m:
        if j == m or (i < n and a[i] < b[j]):
            merged_arr[i + j] = a[i]
            i += 1
        else:
            merged_arr[i + j] = b[j]
            j += 1

    return merged_arr


def merge_sort(arr: list) -> list:
    if len(arr) == 1:
        return arr
    halfIndex = len(arr) // 2

    left = arr[ : halfIndex]
    left = merge_sort(left)

    right = arr[halfIndex : ]
    right = merge_sort(right)
    
    return merge(left, right)


def main(input, print):
    n = int(input())
    arr = list(map(int, input().split()))

    sortedArr = merge_sort(arr)
    print(' '.join(map(str, sortedArr)))



t1 = time.time()

print(merge([4,5,6], [1,2,3]))
print(merge([2,3,4], [1,5,6]))
print(merge([1,5,6], [2,3,4]))
print(merge([1,2,3], [4,5,6]))
print(merge([1,2,5], [3,4,6]))
print(merge([3,4,6], [1,2,5]))

print(merge([4,6], [1,2,3,5]))
print(merge([2,2,2], [1,3,4,5,6]))
print(merge([1,5], [2,3,3,4,6]))
print(merge([1,2,3], [4,5,6]))
print(merge([1,2,4,4,4,5], [3,6,6,6]))
print(merge([3,3,4,5,5,5,6], [1,2,2,2]))

print(merge_sort([1]))
print(merge_sort([1,2]))
print(merge_sort([2,1]))
print(merge_sort([1,2,3]))
print(merge_sort([1,3,2]))
print(merge_sort([2,1,3]))
print(merge_sort([2,3,1]))
print(merge_sort([3,2,1]))
print(merge_sort([3,1,2]))

print(merge_sort([1,2,3, 4]))
print(merge_sort([1,4,3,2]))
print(merge_sort([2,1,4,3]))
print(merge_sort([2,3,1,4]))
print(merge_sort([4,3,2,1]))
print(merge_sort([3,1,4,2]))



#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)
        #generate_test()
        #main(input, print)
        #pass
print(time.time() - t1)