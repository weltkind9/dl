import time
import random


class KingName:
    def __init__(self, name, number, romanNumber):
        self.name = name
        self.number = number
        self.romanNumber = romanNumber

    def __lt__(self, other):
        if self.name != other.name:
            return self.name < other.name
        else:
            return self.number < other.number


def roman_to_arabic(romanNum: str, storage: dict) -> int:
    if romanNum == "":
        return 0

    if romanNum in storage:
        return storage[romanNum]

    for key in storage:
        i = romanNum.find(key)
        if i > -1:
            return (
                -roman_to_arabic(romanNum[:i], storage)
                + storage[key]
                + roman_to_arabic(romanNum[i + 1 :], storage)
            )


def merge(a: list, b: list) -> list:
    n = len(a)
    m = len(b)
    merged_arr = [0] * (n + m)
    i = 0
    j = 0

    while i + j < n + m:
        if j == m or (i < n and a[i] < b[j]):
            merged_arr[i + j] = a[i]
            i += 1
        else:
            merged_arr[i + j] = b[j]
            j += 1

    return merged_arr


def merge_sort(arr: list) -> list:
    if len(arr) == 1:
        return arr
    halfIndex = len(arr) // 2

    left = arr[:halfIndex]
    left = merge_sort(left)

    right = arr[halfIndex:]
    right = merge_sort(right)

    return merge(left, right)


def main(input, print):
    n = int(input())
    names = []
    numberStorage = {"L": 50, "X": 10, "V": 5, "I": 1}
    for _ in range(n):
        kingName, kingNumberRoman = input().strip().split()

        kingNumber = roman_to_arabic(kingNumberRoman, numberStorage)
        numberStorage[kingNumberRoman] = kingNumber

        names.append(KingName(kingName, kingNumber, kingNumberRoman))

    namesSorted = merge_sort(names)

    for kn in namesSorted:
        print(kn.name, kn.romanNumber)


def test(input, print):
    dct = {}
    for i in range(1, 51):
        s = input().strip()
        dct[s] = i

    for i in range(10000):
        numStorage = {"L": 50, "X": 10, "V": 5, "I": 1}
        keys = list(dct.keys())
        random.shuffle(keys)

        for key in keys:
            kingNumber = roman_to_arabic(key, numStorage)
            assert kingNumber == dct[key]
            numStorage[key] = kingNumber


t1 = time.time()
# main(input, print)
with open(__file__.replace(".py", "t.txt")) as read:
    with open(__file__.replace(".py", "_ans.txt"), "w") as write:
        # main(read.readline, write.write)
        main(read.readline, print)
        # test(read.readline, print)
        # main(input, print)
print(time.time() - t1)
