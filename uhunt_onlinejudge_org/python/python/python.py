def f(arr, n):
    return (arr[n-1]+1)/arr[n-2]

arr = [2015, 2016]
for i in range(2, 2018):
    arr.append(f(arr, i))

print({(i, arr[i]) for i in range(len(arr))})
    
