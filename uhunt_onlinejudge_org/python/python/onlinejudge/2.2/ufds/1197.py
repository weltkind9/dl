class ufds():
    def __init__(self, n):
        self.parents = [i for i in range(n)]
        self.sizes = [1 for i in range(n)]

    def find(self, x):
        xp = x        
        children = []
        while xp != self.parents[xp]:
            children.append(xp)
            xp = self.parents[xp]
        for c in children:
            self.parents[c] = xp
            self.sizes[c] = 1
        return xp

    def union(self, a, b):
        ap = self.find(a)
        bp = self.find(b)
        if ap == bp:
            return
        if self.sizes[ap] >= self.sizes[bp]:
            self.parents[bp] = ap
            self.sizes[ap] += self.sizes[bp]
        else:
            self.parents[ap] = bp
            self.sizes[bp] += self.sizes[ap]

    def size(self, x):
        return self.sizes[self.find(x)]



def main(input, print):
    while 1:
        n, m = map(int, input().strip().split())
        if n == 0 and m == 0:
            break
        u = ufds(n)
        for _ in range(m):
            students = list(map(int, input().split()))
            for i in range(2, len(students)):
                u.union(students[1], students[i])
                u.find(students[1])
                u.find(students[i])
        print(u.size(0))


#main(input, print)
with open('E:/coding/codeforces/python/python/onlinejudge/2.2/ufds/1197t.txt') as read:
    with open('E:/coding/codeforces/python/python/onlinejudge/2.2/ufds/1197_ans.txt', 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)
