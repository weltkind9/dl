

class ufds():
    def __init__(self, n):
        self.parents = [i for i in range(n)]
        self.sizes = [1 for i in range(n)]

    def find(self, x):
        xp = x        
        children = []
        while xp != self.parents[xp]:
            children.append(xp)
            xp = self.parents[xp]
        for c in children:
            self.parents[c] = xp
            self.sizes[c] = 1
        return xp

    def union(self, a, b):
        ap = self.find(a)
        bp = self.find(b)
        if ap == bp:
            return
        if self.sizes[ap] >= self.sizes[bp]:
            self.parents[bp] = ap
            self.sizes[ap] += self.sizes[bp]
        else:
            self.parents[ap] = bp
            self.sizes[bp] += self.sizes[ap]

    def size(self, x):
        return self.sizes[self.find(x)]


def main(input, print):
    t = int(input().strip())
    for _ in range(t):
        u = ufds(100000)
        f = int(input().strip())
        dct = {}
        f_count = 0
        for i in range(f):
            s = input().strip().split()
            if s[0] not in dct:
                dct[s[0]] = f_count
                f_count += 1 
            if s[1] not in dct:
                dct[s[1]] = f_count
                f_count += 1
            
            k = dct[s[0]]
            l = dct[s[1]]
            u.union(l, k)
            print(u.size(l))


#main(input, print)
with open('E:/coding/codeforces/python/python/onlinejudge/2.2/ufds/11503t.txt') as read:
    with open('E:/coding/codeforces/python/python/onlinejudge/2.2/ufds/11503_ans.txt', 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)
