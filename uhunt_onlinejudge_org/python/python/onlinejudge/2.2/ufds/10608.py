
class ufds():
    def __init__(self, n):
        self.parents = [i for i in range(n)]
        self.sizes = [1 for i in range(n)]

    def find(self, x):
        xp = x        
        children = []
        while xp != self.parents[xp]:
            children.append(xp)
            xp = self.parents[xp]
        for c in children:
            self.parents[c] = xp
            self.sizes[c] = 1
        return xp

    def union(self, a, b):
        ap = self.find(a)
        bp = self.find(b)
        if ap == bp:
            return
        if self.sizes[ap] >= self.sizes[bp]:
            self.parents[bp] = ap
            self.sizes[ap] += self.sizes[bp]
        else:
            self.parents[ap] = bp
            self.sizes[bp] += self.sizes[ap]

    def size(self, x):
        return self.sizes[self.find(x)]


def main(input, print):
    t = int(input())
    for _ in range(t):
        n, m = map(int, input().strip().split())
        u = ufds(n)
        for __ in range(m):
            i, j = map(int, input().strip().split())
            u.union(i-1, j-1)

        print(max(u.sizes))


#main(input, print)
with open('E:/coding/codeforces/python/python/onlinejudge/2.2/ufds/10608t.txt') as read:
    with open('E:/coding/codeforces/python/python/onlinejudge/2.2/ufds/10608_ans.txt', 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)
