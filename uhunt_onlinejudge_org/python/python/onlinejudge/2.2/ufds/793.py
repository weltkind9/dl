class ufds():
    def __init__(self, n):
        self.parents = [i for i in range(n)]
        self.sizes = [1 for i in range(n)]

    def find(self, x):
        xp = x        
        children = []
        while xp != self.parents[xp]:
            children.append(xp)
            xp = self.parents[xp]
        for c in children:
            self.parents[c] = xp
            self.sizes[c] = 1
        return xp

    def union(self, a, b):
        ap = self.find(a)
        bp = self.find(b)
        if ap == bp:
            return
        if self.sizes[ap] >= self.sizes[bp]:
            self.parents[bp] = ap
            self.sizes[ap] += self.sizes[bp]
        else:
            self.parents[ap] = bp
            self.sizes[bp] += self.sizes[ap]

    def size(self, x):
        return self.sizes[self.find(x)]


import time

def ff(input, print):
    n = int(input())
    u = ufds(n)
    successful_answers = 0
    unsuccessful_answers = 0
    while 1:
        try:
            s = input()
            if s =='' or s == '\n':
                break
            query, c1, c2 = s.strip().split()
            if query == 'c':
                u.union(int(c1) - 1, int(c2) - 1)
            else:
                fc1 = u.find(int(c1)-1)
                fc2 = u.find(int(c2)-1)
                if fc1 == fc2:
                    successful_answers += 1
                else:
                    unsuccessful_answers += 1
        except:
            break
    return f'{successful_answers},{unsuccessful_answers}'


def main(input, print):
    t = int(input().strip())
    input()
    ss = []
    for _ in range(t):
        ss.append(ff(input, print))
    print('\n\n'.join(ss))


t = time.time()
#main(input, print)
with open('E:/coding/codeforces/python/python/onlinejudge/2.2/ufds/793t.txt') as read:
    with open('E:/coding/codeforces/python/python/onlinejudge/2.2/ufds/793_ans.txt', 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)
print(time.time()-t)




u = ufds(8)
u.union(1, 2)
print(u.find(1) == u.find(2))
print(u.find(1) != u.find(3))
print(u.find(2) != u.find(3))
print(u.size(1) == 2)
print(u.size(2) == 2)
print(u.size(3) == 1)
u.union(1, 3)
print(u.find(1) == u.find(2))
print(u.find(1) == u.find(3))
print(u.find(2) == u.find(3))
print(u.find(2) != u.find(4))
print(u.size(1) == 3)
print(u.size(2) == 3)
print(u.size(3) == 3)
u.union(2, 4)
print(u.find(1) == u.find(3))
print(u.find(2) == u.find(4))
print(u.size(1) == 4)
print(u.size(2) == 4)
u.union(2, 3)
print(u.size(1) == 4)
print(u.size(2) == 4)


u.union(5, 6)
u.union(6, 7)
u.union(7, 0)

u.union(1, 0)
u.find(1)
u.find(0)
u.find(6)
u.find(7)
print()