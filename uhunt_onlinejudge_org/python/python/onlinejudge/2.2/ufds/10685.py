
class ufds():
    def __init__(self, n):
        self.parents = [i for i in range(n)]
        self.sizes = [1 for i in range(n)]

    def find(self, x):
        xp = x        
        children = []
        while xp != self.parents[xp]:
            children.append(xp)
            xp = self.parents[xp]
        for c in children:
            self.parents[c] = xp
            self.sizes[c] = 1
        return xp

    def union(self, a, b):
        ap = self.find(a)
        bp = self.find(b)
        if ap == bp:
            return
        if self.sizes[ap] >= self.sizes[bp]:
            self.parents[bp] = ap
            self.sizes[ap] += self.sizes[bp]
        else:
            self.parents[ap] = bp
            self.sizes[bp] += self.sizes[ap]

    def size(self, x):
        return self.sizes[self.find(x)]


def main(input, print):
    while 1:
        c, r = map(int, input().strip().split())
        if c == 0 and r == 0:
            break
        u = ufds(c)
        dct = {}
        for i in range(c):
            s = input().strip()
            dct[s] = i

        for i in range(r):
            s = input().strip().split()
            k = dct[s[0]]
            l = dct[s[1]]
            u.union(l, k)
        input()

        print(max(u.sizes))


#main(input, print)
with open('E:/coding/codeforces/python/python/onlinejudge/2.2/ufds/10685t.txt') as read:
    with open('E:/coding/codeforces/python/python/onlinejudge/2.2/ufds/10685_ans.txt', 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)
