import time
import queue

def main(input, print):    
    isStack = 1
    isQueue = 2
    isPriorityQueue = 4
    isImpossible = 0
    while 1:
        try:
            t = int(input().strip())
            pq = queue.PriorityQueue()
            q = queue.Queue()
            stack = queue.LifoQueue()
            r = 7
            for _ in range(t):
                command, value = map(int, input().split())
                if command == 1:
                    pq.put(-1 * value)
                    q.put(value)
                    stack.put(value)
                else:
                    v = pq.get()
                    if -1 * v != value:
                        r = r & (~isPriorityQueue)
                    v = q.get()
                    if v != value:
                        r = r & (~isQueue)
                    v = stack.get()
                    if v != value:
                        r = r & (~isStack)                    

            if r == 0:
                print('impossible')
            elif r == isStack:
                print('stack')
            elif r == isQueue:
                print('queue')
            elif r == isPriorityQueue:
                print('priority queue')
            else:
                print('not sure')
        except:
            break


#main(input, print)
with open('E:/coding/codeforces/python/python/onlinejudge/2.2/11995t.txt') as read:
    with open('E:/coding/codeforces/python/python/onlinejudge/2.2/11995_ans.txt', 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)
#print(time.time()-t)

