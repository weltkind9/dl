

def ff(areas: list, waking: set, connections: dict):
    years = 0
    while 1:
        iterWaking = []
        for i in range(len(areas)-1, -1, -1):
            con = connections[areas[i]]
            c = 0
            for cc in con:
                if cc in waking: c += 1
            if c > 2:
                iterWaking.append(areas.pop(i))
        years += 1
        for w in iterWaking:
            waking.add(w)
        if len(iterWaking) == 0:
            if len(areas) > 0:
                return 'THIS BRAIN NEVER WAKES UP'
            else:
                return f'WAKE UP IN, {years}, YEARS'

def main(input, print):
    n = int(input().strip())
    m = int(input().strip())
    directly = set(input().strip())
    areas = set(directly)
    connections = {}
    for _ in range(m):
        ss = list(input().strip())
        if ss[0] not in connections:
            connections[ss[0]] = []
        connections[ss[0]].append(ss[1])
        
        if ss[1] not in connections:
            connections[ss[1]] = []
        connections[ss[1]].append(ss[0])

        areas.add(ss[0])
        areas.add(ss[1])

    print(ff(list(areas.difference(directly)), directly, connections))


#main(input, print)
with open('E:/coding/codeforces/python/python/onlinejudge/2.2/10507t.txt') as read:
    with open('E:/coding/codeforces/python/python/onlinejudge/2.2/10507_ans.txt', 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)
