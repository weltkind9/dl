import time
from skiplistcollections import SkipListDict

def ff(input):    
    sld = SkipListDict()
    cnt = 0
    sss = []
    while 1:
        try:
            s = input().strip()
            if s is None or s == '' or s == '\n':
                break
            if not (s in sld):
                sld.update({(s, 0)})
            sld[s] += 1
            cnt += 1
        except:
            break
    odd = sorted(od)
    for k in odd:
        d = round(od[k] / cnt * 100, 4)
        sss.append(f'{k} {d}')
    return '\n'.join(sss)


def main(input):
    s = input()
    t = int(s)
    input()
    rs = []
    for _ in range(t):
        rs.append(ff(input))
    print('\n\n'.join(rs))

t = time.time()
#main(input)
with open('E:/coding/codeforces/python/python/onlinejudge/2.2/10226t2.txt') as f:
    main(f.readline)
#print(time.time()-t)

