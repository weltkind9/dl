import struct
def main(input):
    while 1:
        try:
            s = input()
            if s is None or s == '' or s == '\n':
                break
            i = int(s)
            r = struct.unpack('>l', struct.pack('<l', i))[0]
            print(f'{i} converts to {r}')
        except:
            break
#main(input)
with open('E:/coding/codeforces/python/python/onlinejudge/2.2/bitset/594t.txt') as f:
    main(f.readline)
