import time
from collections import defaultdict
def ff(n, m, input, print):
    arr = list(map(int, input().split()))
    values = [[] for i in range(1000000)]
    for i in range(n):
        values[arr[i]-1].append(i)
    for i in range(m):
        k, v = map(int, input().split())
        vv = values[v-1]
        if len(vv) > k-1:
            print(vv[k-1]+1)
        else:
           print(0)


def main(input, print):
    while 1:
        try:
            n, m = map(int, input().split())
        except:
            break
        ff(n, m, input, print)


t = time.time()
#main(input, print)
with open('E:/coding/codeforces/python/python/onlinejudge/2.2/graph/11991t2.txt') as read:
    with open('E:/coding/codeforces/python/python/onlinejudge/2.2/graph/11991_ans.txt', 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)
print(time.time()-t)

