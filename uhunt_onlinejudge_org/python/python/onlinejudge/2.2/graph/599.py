def dfs(edges: dict, vertices: dict, path:list, cur_vertex):
    if vertices[cur_vertex]:
        return
    path.append(cur_vertex)
    vertices[cur_vertex] = True
    if cur_vertex in edges:
        for v in edges[cur_vertex]:
            dfs(edges, vertices, path, v)

def main(input, print):    
    t = int(input().strip())
    for _ in range(t):
        edges = {}
        while True:
            s = input()
            if '*' in s: break
            v1, v2 = s.rstrip(')\n').lstrip('(').split(',')
            if not v1 in edges:
                edges[v1] = set()
            if not v2 in edges:
                edges[v2] = set()
            edges[v1].add(v2)
            edges[v2].add(v1)

        s = input()
        vertices = {a: False for a in s.rstrip('\n').split(',')}
        components = []
        for v in vertices:
            path = []
            if vertices[v]:
                continue
            dfs(edges, vertices, path, v)
            components.append(path)
        acorns = [x for x in components if len(x) == 1]
        trees = [x for x in components if len(x) > 1]
        print(f'There are {len(trees)} tree(s) and {len(acorns)} acorn(s).')

#main(input, print)
with open('E:/coding/codeforces/python/python/onlinejudge/2.2/graph/599t.txt') as read:
    with open('E:/coding/codeforces/python/python/onlinejudge/2.2/graph/599_ans.txt', 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)
#print(time.time()-t)

