import time
from queue import PriorityQueue
from collections import namedtuple
Q = namedtuple('Q', ['value', 'name', 'period'])

def main(input, print):
    q = PriorityQueue()
    result = []
    while 1:
        s = input().strip()
        if s == '#':
            break
        ss = s.split()
        q.put(Q(int(ss[2]), int(ss[1]), int(ss[2])))

    k = int(input().strip())

    for i in range(k):
        if q.empty():
            break
        r = q.get()
        result.append(str(r.name))
        q.put(Q(r.value + r.period, r.name, r.period))
    print('\n'.join(result))

t = time.time()

#main(input, print)
with open('E:/coding/codeforces/python/python/onlinejudge/2.2/PriorityQueue/1203t.txt') as read:
    with open('E:/coding/codeforces/python/python/onlinejudge/2.2/PriorityQueue/1203_ans.txt', 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)
#print(time.time()-t)

