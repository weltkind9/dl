
from queue import PriorityQueue

q = PriorityQueue()

q.put(4)
q.put(2)
q.put(5)
q.put(1)
q.put(3)

while not q.empty():
    next_item = q.get()
    print(next_item)



q = PriorityQueue()

q.put((4, 'Read'))
q.put((2, 'Play'))
q.put((5, 'Write'))
q.put((1, 'Code'))
q.put((3, 'Study'))

while not q.empty():
    next_item = q.get()
    print(next_item)




from collections import namedtuple
Q = namedtuple('Q', ['value', 'name', 'period' ])


q = PriorityQueue()

q.put(Q(166, '166', 166))
q.put(Q(510, '510', 510))
q.put(Q(55, '55', 55))
q.put(Q(40, '40', 40))
q.put(Q(20, '20', 20))
k = 20
for _ in range(k):
    r = q.get()
    print(r)
    q.put(Q(r.value + r.period, r.name, r.period))

