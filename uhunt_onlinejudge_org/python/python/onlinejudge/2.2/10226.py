import time
from collections import defaultdict

def ff(input):    
    od = defaultdict()
    cnt = 0
    sss = []
    t1 = time.time()
    while 1:
        try:
            s = input().strip()
            if s == '':#s is None or s == '' or s == '\n':
                break
            od[s] = od.get(s, 0) + 1
            cnt += 1
        except:
            break
    #print(f'while {time.time() - t1}')
    t1 = time.time()
    odd = sorted(od)
    #print(f'sort {time.time() - t1}')
    t1 = time.time()
    for k in odd:
        d = round(od[k] / cnt * 100, 4)
        sss.append(f'{k} {d}')
    #print(f'for {time.time() - t1}')

    return '\n'.join(sss)


def main(input):
    s = input()
    t = int(s)
    input()
    rs = []
    for _ in range(t):
        rs.append(ff(input))
    print('\n\n'.join(rs))

t = time.time()
#main(input)
with open('E:/coding/codeforces/python/python/onlinejudge/2.2/10226t2.txt') as f:
    main(f.readline)
#print(time.time()-t)

