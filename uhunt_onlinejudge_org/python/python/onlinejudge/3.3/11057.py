
from bisect import bisect_left, bisect_right
import time


def binary_search(a, x, lo=0, hi=None):
    if hi is None: hi = len(a)
    pos = bisect_left(a, x, lo, hi)
    return pos if pos != hi and a[pos] == x else -1

def main(input, print):
    while 1:
        try:
            n = int(input())
            a = list(map(int, input().split()))
            money = int(input())            
            a.sort()
            min_diff = 1005000000
            i, j = 0, 0
            for ii in range(len(a)-1):
                x = a[ii]
                m = money - x
                fnd = binary_search(a, m, ii+1)
                if fnd == -1:
                    continue
                if abs(x - m) < min_diff:
                    i, j = min(x, m), max(x, m)
                    min_diff = abs(x - m)
            print(f'Peter should buy books whose prices are {i} and {j}.\n\n')
            input()
        except Exception as ex:
            break

t1 = time.time()
#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)
print(time.time() - t1)