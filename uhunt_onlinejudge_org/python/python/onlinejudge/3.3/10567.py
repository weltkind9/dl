import time
from bisect import bisect_left

def generate_test():
    frst = 's' * (10000 * 99) + 'a'*100
    ss = []
    n = 3500
    ss.append(str(n))
    for _ in range(n):
        ss.append('a'*100)
    with open(__file__.replace('.py','t.txt'), 'w') as write:
        write.write(frst + '\n' + '\n'.join(ss))

def binary_search(a, x, lo=0, hi=None):
    if hi is None: hi = len(a)
    pos = bisect_left(a, x, lo, hi)
    return pos if pos != hi and a[pos] == x else -1

def main(input, print, find):
    toPrint =[]
    ss = input().strip()
    t = int(input())
    for _ in range(t):
        s = input().strip()
        ls = list(s)
        ind = find(ss, ls[0])
        frst = ind
        if ind == -1:
            toPrint.append('Not matched')
        else:
            cur_ind = ind
            for i in range(1, len(ls)):
                cur_ind = find(ss, ls[i], ind + 1)
                if cur_ind == -1:
                    toPrint.append('Not matched')
                    break
                ind = cur_ind
            else:
                toPrint.append(f'Matched {frst} {cur_ind}')
    print('\n'.join(toPrint))

#generate_test()
t1 = time.time()
#main(input, print, str.find)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write, str.find)
        #main(read.readline, print, str.find)
print(time.time() - t1)