from bisect import bisect_left
import time

def binary_search(a, x):
    pos = bisect_left(a, x)
    return pos if pos != len(a) and a[pos] == x else -1

def main(input, print):
    toPrint =[]
    case = 1
    while 1:
        n, q = map(int, input().split())
        if n == 0 and q == 0:
            break
        a = []
        #input()
        for _ in range(n):
            a.append(int(input()))
        a.sort()
        s = f'CASE# {case}:\n'
        #input()
        ss = []
        for _ in range(q):
            x = int(input())
            ind = binary_search(a, x)
            if ind == -1:
                ss.append(f'{x} not found')
            else:
                ss.append(f'{x} found at {ind+1}')
        toPrint.append(s + '\n'.join(ss))
        case += 1
        #input()
    print('\n'.join(toPrint))

t1 = time.time()
#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)
print(time.time() - t1)