
from bisect import bisect_left, bisect_right
import time
import math
def f(p, q, r, s, t, u, x):
    return p*math.exp(-x) + q*math.sin(x) + r*math.cos(x) + s*math.tan(x) + t*x*x + u

def main(input, print):
    
    toPrint = []

    while 1:
        try:
            p, q, r, s, t, u = map(int, input().split())
            f0 = f(p, q, r, s, t, u, 0)
            f1 = f(p, q, r, s, t, u, 1)
            if f0 > 0 and f1 > 0: toPrint.append('No solution'); continue
            if f0 < 0 and f1 < 0: toPrint.append('No solution'); continue
            hi = 1
            lo = 0
            if f0 > f1:
                for _ in range(50):
                    x = (hi + lo) / 2
                    fx = f(p, q, r, s, t, u, x)
                    if abs(fx) < 0.0000001:
                        break
                    if fx < 0:
                        hi = x
                    else:
                        lo = x
            else:
                for _ in range(50):
                    x = (hi + lo) / 2
                    fx = f(p, q, r, s, t, u, x)
                    if abs(fx) < 0.0000001:
                        break
                    if fx < 0:
                        lo = x
                    else:
                        hi = x
            toPrint.append('{:0.4f}'.format(round(x, 4)))
        except Exception as ex:
            break
    print('\n'.join(toPrint))



t1 = time.time()
#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)
print(time.time() - t1)