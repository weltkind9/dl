import time
import math

def isMember(x):
    n = 3
    r = '0.'
    for i in range(30):
        x *= n
        cel = math.floor(x)
        if cel == 1:
            return False
        r += str(cel)
        x -= cel
    return True

def main(input, print):
    toPrint = []
    while 1:
        s = input()
        if 'END' in s:
            break
        x = float(s)
        if isMember(x):
            toPrint.append('MEMBER')
        else:
            toPrint.append('NON-MEMBER')
    print('\n'.join(toPrint))



t1 = time.time()
#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)
print(time.time() - t1)