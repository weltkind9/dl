
from bisect import bisect_left, bisect_right
import time

def find_lt(a, x):
    'Найти крайнее правое значение меньше x'
    i = bisect_left(a, x)
    if i:
        return a[i-1]
    return -1


def find_gt(a, x):
    'Найти крайнее левое значение больше, чем х'
    i = bisect_right(a, x)
    if i != len(a):
        return a[i]
    return -1

def main(input, print):
    toPrint = []
    while 1:
        try:
            n = int(input())
            a = list(map(int, input().split()))
            q = int(input())
            bb = list(map(int, input().split()))
            for x in bb:
                lt = find_lt(a, x)
                if lt == -1: s = 'X'
                else: s = str(lt)

                gt = find_gt(a, x)
                if gt == -1: s += ' X'
                else: s += f' {gt}'
                toPrint.append(s)
        except Exception as ex:
            break
    print('\n'.join(toPrint))

t1 = time.time()
#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)
print(time.time() - t1)