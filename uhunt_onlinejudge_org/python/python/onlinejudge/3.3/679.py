


from bisect import bisect_left, bisect_right
import time
import math

def isOn(S, j):
    return (S & (1<<j))
def setBit(S, j):
    return (S | (1<<j))
def clearBit(S, j):
    return (S & (~(1<<j)))

def main(input, print):    
    toPrint = []
    t = int(input())
    for _ in range(t):
        D, I = map(int, input().split())
        a = 0
        for i in range(I):
            shift = 1
            for d in range(D):
                if isOn(a, shift):
                    a = clearBit(a, shift)
                    shift = shift*2+1
                else:
                    a = setBit(a, shift)
                    shift *= 2
        toPrint.append(str(shift//2))

    print('\n'.join(toPrint))



t1 = time.time()
#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)
print(time.time() - t1)

