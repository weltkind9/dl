import time
from bisect import bisect_left, bisect_right

def find_ge(a, x):
    i = bisect_left(a, x)
    if i != len(a):
        return a[i]
    return -1


def prepare():
    a = []
    n = 32
    for k in range(n):
        for l in range(n):
            a.append(2**k * 3**l)
    a.sort()
    return a


def main(input, print):
    toPrint = []
    a = prepare()
    while 1:
        x = int(input())
        if x == 0:
            break
        q = find_ge(a, x)
        toPrint.append(str(q))
    print('\n'.join(toPrint))



t1 = time.time()
#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)
print(time.time() - t1)