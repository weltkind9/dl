from bisect import bisect_left, bisect_right
import time
import math

def find_le(a, x):#Найти индекс крайнее правое значение, меньшее или равное x
    i = bisect_right(a, x)
    if i:
        return i
    return -1
def find_ge(a, x):#Найти индекс крайний левый элемент больше или равно x
    i = bisect_left(a, x)
    if i != len(a):
        return i
    return -1

def main(input, print):
    
    toPrint = []
    while 1:
        n, m = map(int, input().split())
        if n == 0 and m == 0:
            break
        arr = []
        for i in range(n):
            arr.append(list(map(int, input().split())))

        q = int(input())
        for i in range(q):
            a, b = map(int, input().split())
            lo, hi = m+1, -1
            max_side = 0
            for j in range(n):
                lft = find_ge(arr[j], a)
                if lft == -1:
                    continue
                rght = find_le(arr[j], b)
                if rght == 0:
                    continue
                side_hor = rght - lft
                if side_hor < max_side:
                    continue
                #column = [arr[row][lft:lft+1][0] for row in range(n)]
                #btm = find_le(column, b)
                side_vert = min(side_hor, n - j)
                while side_vert >= 0:
                    if side_vert <= max_side:
                        break
                    if arr[j + side_vert - 1][lft] <= b:
                        break
                    side_vert -= 1                
                if side_vert < max_side:
                    continue
                side = min(side_hor, side_vert)
                if arr[j + side-1][lft + side-1] <= b:
                    max_side = side
                else:
                    while side >= 0:
                        if side <= max_side:
                            break
                        if arr[j + side-1][lft + side-1] <= b:
                            max_side = side
                            break
                        side -= 1
            toPrint.append(str(max_side))
        toPrint.append('-')
    print('\n'.join(toPrint))



t1 = time.time()
#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)
print(time.time() - t1)