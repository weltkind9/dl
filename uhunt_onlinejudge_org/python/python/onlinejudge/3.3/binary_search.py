
from bisect import bisect_left, bisect_right

def find_lt(a, x):
    '����� ������� ������ �������� ������ x'
    i = bisect_left(a, x)
    if i:
        return a[i-1]
    return -1


def find_gt(a, x):
    '����� ������� ����� �������� ������, ��� �'
    i = bisect_right(a, x)
    if i != len(a):
        return a[i]
    return -1


def find_le(a, x):
    '����� ������� ������ ��������, ������� ��� ������ x'
    i = bisect_right(a, x)
    if i:
        return a[i-1]
    return -1

def find_ge(a, x):
    '����� ������� ����� ������� ������ ��� ����� x'
    i = bisect_left(a, x)
    if i != len(a):
        return a[i]
    return -1

def binary_search(a, x, lo=0, hi=None):
    if hi is None: hi = len(a)
    pos = bisect_left(a, x, lo, hi)
    return pos if pos != hi and a[pos] == x else -1
