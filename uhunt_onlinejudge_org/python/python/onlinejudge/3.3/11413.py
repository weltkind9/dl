

from bisect import bisect_left, bisect_right
import time
import math
def fill(arr: list, count: int, volume: int):
    s = 0
    for a in arr:
        if a > volume:
            return False
        if s + a > volume:
            if count <= 1:
                return False
            else:
                count -= 1
                s = 0
        s += a
    return True



def main(input, print):    
    toPrint = []
    while 1:
        try:            
            n, m = map(int, input().split())
            arr = list(map(int, input().split()))

            hi = sum(arr)
            lo = max(arr)
            for i in range(50):
                if hi == lo:
                    break
                volume = (hi + lo) // 2
                isFill = fill(arr, m, volume)
                if isFill:
                    hi = volume
                else:
                    lo = volume
                
            toPrint.append(str(hi))
            #input()
        except Exception as ex:
            break
    print('\n'.join(toPrint))



t1 = time.time()
#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)
print(time.time() - t1)


print(fill([1,2,3,4,5], 3, 1))
print(fill([1,2,3,4,5], 3, 2))
print(fill([1,2,3,4,5], 3, 3))
print(fill([1,2,3,4,5], 3, 4))
print(fill([1,2,3,4,5], 3, 5))
print(fill([1,2,3,4,5], 3, 6))
print(fill([1,2,3,4,5], 3, 7))
print(fill([1,2,3,4,5], 3, 8))
print(fill([1,2,3,4,5], 3, 9))
print(fill([1,2,3,4,5], 3, 10))
print(fill([1,2,3,4,5], 3, 11))

print()
print()
print()
print(fill([4, 78, 9], 2, 10))
print(fill([4, 78, 9], 2, 20))
print(fill([4, 78, 9], 2, 30))
print(fill([4, 78, 9], 2, 40))
print(fill([4, 78, 9], 2, 50))
print(fill([4, 78, 9], 2, 60))
print(fill([4, 78, 9], 2, 70))
print(fill([4, 78, 9], 2, 77))
print(fill([4, 78, 9], 2, 78))
print(fill([4, 78, 9], 2, 79))
print(fill([4, 78, 9], 2, 80))
print(fill([4, 78, 9], 2, 81))
print()
print(fill([4, 78, 9], 2, 82))
print(fill([4, 78, 9], 2, 83))
print(fill([4, 78, 9], 2, 84))


print(fill([4, 78, 9], 2, 90))
print(fill([4, 78, 9], 2, 100))
print(fill([4, 78, 9], 2, 110))
print(fill([4, 78, 9], 2, 120))
print(fill([4, 78, 9], 2, 130))