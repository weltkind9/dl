import time

class Fraction():
    def __init__(self, chisl: int, znam: int):
        self.chisl = chisl
        self.znam = znam
        if self.znam > 0:
            self.val = self.chisl / self.znam
    def __add__(self, other):
        return Fraction(self.chisl + other.chisl, self.znam + other.znam)
    def __str__(self):
        return f'{self.chisl} / {self.znam}'
    def __eq__(self, other):
        return self.chisl == other.chisl and self.znam == other.znam
    def __gt__(self, other):
        return self.val > other.val    
    def __lt__(self, other):
        return self.val < other.val

def main(input, print):
    toPrint =[]
    while 1:
        m, n = map(int, input().split())
        if n == 1 and m == 1:
            break
        fnd = Fraction(m, n)

        ans = ''
        dwn = Fraction(0, 1)
        up = Fraction(1, 0)
        curr = Fraction(1, 1)
        while 1:
            if curr == fnd:
                break
            elif curr > fnd:
                up = curr
                curr = curr + dwn
                ans += 'L'
            else:
                dwn = curr
                curr = curr + up
                ans += 'R'
        toPrint.append(ans)
    print('\n'.join(toPrint))



t1 = time.time()
#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)
print(time.time() - t1)