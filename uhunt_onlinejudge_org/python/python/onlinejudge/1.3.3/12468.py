def main(input):
    while 1:
        a, b = map(int, input().split())
        if a == -1:
            break
        if a > b:
            r = min(a - b, 100 + b - a)
        else:
            r = min(b - a, 100 + a - b)
        print(r)
#main(input)

with open('E:/coding/codeforces/python/python/onlinejudge/1.3.3/12468_test.txt') as f:
    main(f.readline)
