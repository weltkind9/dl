t = int(input())
for i in range(t):
    a, b, c = map(int, input().split())
    if a > 20 or b > 20 or c > 20:
        print(f'Case {i + 1}: bad')
    else:
        print(f'Case {i + 1}: good')