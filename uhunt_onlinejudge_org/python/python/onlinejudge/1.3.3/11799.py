def main(input):
    t = int(input())

    for i in range(t):        
        w = list(map(int, input().split()))[1:]
        print(f'Case {i+1}: {max(w)}')
#main(input)

with open('E:/coding/codeforces/python/python/onlinejudge/1.3.3/11799_test.txt') as f:
    main(f.readline)
