t = int(input())
while t > 0:
    n = int(input())
    a = list(map(int, input().split()))
    a.sort()
    m = 20#a[len(a)//2]
    r = 0
    for i in a:
        r += abs(i - m)
    print(r)
    t -= 1