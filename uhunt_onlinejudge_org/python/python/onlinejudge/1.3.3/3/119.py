def main(input):
    res = []
    while 1:
        try:
            n = int(input())
            d = {}
            ss = input().split()
            for s in ss:
                d[s.strip()] = 0

            for i in range(n):
                ss = input().split()
                g = int(ss[1])
                c = int(ss[2])
                if c == 0:
                    continue
                gg = g // c
                for j in range(c):
                    d[ss[3 + j].strip()] += gg
                d[ss[0].strip()] -= g
                d[ss[0].strip()] += g - gg * c
            s = '\n'.join([f'{k} {d[k]}' for k in d])
            res.append(s)
            #for k in d:
            #    print(f'{k} {d[k]}')
        except Exception as ex:
            break
    print('\n\n'.join(res))

    
#main(input)

with open('E:/coding/codeforces/python/python/onlinejudge/1.3.3/3/119t.txt') as f:
    main(f.readline)
