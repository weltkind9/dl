def main(input):
    while 1:
        s = input()
        H, U, D, F = map(int, s.split())
        if H == 0:
            break
        h = 0
        i = 1
        while 1:
            dist_climb = U - (U / 100 * F) * (i-1)
            h += dist_climb
            if h > H:
                print(f'success on day {i}')
                break
            h -= D
            if h < 0:
                print(f'failure on day {i}')
                break
            i += 1

    
#main(input)

with open('E:/coding/codeforces/python/python/onlinejudge/1.3.3/3/573t.txt') as f:
    main(f.readline)
