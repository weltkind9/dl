def main(input):
    res = []
    sec = 1
    while 1:
        try:
            s = input()
            n, m, c = map(int, s.split())
        except Exception as ex:
            break
        if n == 0:
            break
        dev = []
        for i in range(n):
                d = int(input().strip())
                dev.append([d, 0])
        max_pw = 0
        pw = 0
        for i in range(m):
            if max_pw > c:
                continue
            d = int(input().strip())
            dv = dev[d-1]
            if dv[1] == 0:
                pw += dv[0]
                dv[1] = 1
            else:
                pw -= dv[0]
                dv[1] = 0
            if pw > max_pw:
                max_pw = pw

        if max_pw > c:
            res.append(f'Sequence {sec}\nFuse was blown.')
        else:
            res.append(f'Sequence {sec}\nFuse was not blown.\nMaximal power consumption was {max_pw} amperes.')
        sec += 1
    print('\n\n'.join(res))
#main(input)
with open('E:/coding/codeforces/python/python/onlinejudge/1.3.3/3/661t.txt') as f:
    main(f.readline)
