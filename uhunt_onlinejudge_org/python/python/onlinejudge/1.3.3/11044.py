import math
t = int(input())
while t > 0:
    n, m = map(int, input().split())
    r = (math.ceil((n-2)/3)) * math.ceil(((m-2)/3))
    print(r)
    t-=1