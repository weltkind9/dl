import math
def main(input):
    t = int(input())
    for i in range(t):        
        n = int(input())
        aa = map(float, input().split())
        m = 0
        j = 0
        for a in aa:
            mm = math.ceil(a / 30) * 10
            #print(f'mm {mm}')
            m += mm
            jj = math.ceil(a / 60) * 15
            #print(f'jj {jj}')
            j += jj
        if m == j:
            print(f'Case {i+1}: Mile Juice {m}')
        elif m < j:
            print(f'Case {i+1}: Mile {m}')
        else:
            print(f'Case {i+1}: Juice {j}')
#main(input)

with open('E:/coding/codeforces/python/python/onlinejudge/1.3.3/12157_test.txt') as f:
    main(f.readline)
