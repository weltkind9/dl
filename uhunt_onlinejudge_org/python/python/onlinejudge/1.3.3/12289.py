t = int(input())
while t > 0:
    s = input()
    if len(s) == 5: print(3)
    elif (s[0] == 'o' and s[1] == 'n') or (s[0] == 'o' and s[2] == 'e') or (s[1] == 'n' and s[2] == 'e'): print(1)
    else: print(2)
    t -= 1