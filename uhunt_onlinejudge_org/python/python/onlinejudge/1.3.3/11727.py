t = int(input())
for i in range(t):
    a = sorted(list(map(int, input().split())))
    print(f'Case {i+1}: {a[1]}')