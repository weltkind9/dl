def main(input):
    while 1:
        ss = input().strip()
        if ss == '0':
            break
        n = list(map(int, list(ss)))
        s = sum(n)
        while s > 9:
            n = list(map(int, list(str(s))))
            s = sum(n)
        print(s)
#main(input)

with open('E:/coding/codeforces/python/python/onlinejudge/1.3.3/11332_test.txt') as f:
    main(f.readline)

