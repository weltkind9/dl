def ff(w):
    if len(w) > 1:
        if w[0] > w[1]:
            for j in range(len(w) - 1):
                if w[j] < w[j+1]:
                    return 'Unordered'
        else:
            for j in range(len(w) - 1):
                if w[j] > w[j+1]:
                    return 'Unordered'
    return 'Ordered'

def main(input):
    t = int(input())

    for i in range(t):        
        w = list(map(int, input().split()))
        print(ff(w))

print('Lumberjacks:')
#main(input)

with open('E:/coding/codeforces/python/python/onlinejudge/1.3.3/11942_test.txt') as f:
    main(f.readline)