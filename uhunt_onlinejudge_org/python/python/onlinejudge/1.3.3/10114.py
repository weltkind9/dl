def main(input):
    while 1:
        months_loan, down_payment, amount_loan, number_records = map(float, input().split())
        if months_loan < 0:
            break
        depreciation_dict = {}
        for i in range(int(number_records)):
            m, d = map(float, input().split())
            depreciation_dict[m] = d

        car_price = down_payment + amount_loan
        pay_per_mount = amount_loan / months_loan
        depreciation_amount = 0
        if 0 in depreciation_dict:
            car_price -= car_price * depreciation_dict[0]

        for i in range(1, int(months_loan)):
            print(f'month: {i}; amount_loan: {amount_loan}; car_price: {car_price}')

            if i in depreciation_dict:
                depreciation_amount = depreciation_dict[i]
            amount_loan -= pay_per_mount
            car_price -= car_price * depreciation_amount


            if car_price > amount_loan:
                if i < 2:
                    print(f'{i} month')
                else:
                    print(f'{i} months')
                break 
#main(input)

with open('E:/coding/codeforces/python/python/onlinejudge/10114_test.txt') as f:
    main(f.readline)


 

        


