def main(input):
    n = int(input())
    for i in range(n):
        t = int(input())
        r = 0
        for j in range(t):
            s, a, p = map(int, input().split())
            r += s * p
        print(r)
main(input)

with open('E:/coding/codeforces/python/python/onlinejudge/10300_test.txt') as f:
    main(f.readline)


