def main(input):
    while 1:
        try:
            ss = input()

            if ss is None or ss == '' or ss == '\n':
                break
            N, P, H, W = map(int, ss.split())

            min_cost = None
            for h in range(H):
                p = int(input())
                w = list(map(int, input().split()))
                if max(w) < N:
                    continue
                cost = p * N
                if min_cost is None:
                    min_cost = cost
                elif cost < min_cost:
                    min_cost = cost

            if min_cost is None: print('stay home')
            elif min_cost > P: print('stay home')
            else: print(min_cost)
        except:
            break

#main(input)

with open('E:/coding/codeforces/python/python/onlinejudge/1.3.3/11559_test.txt') as f:
    main(f.readline)
