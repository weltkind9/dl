def main(input):
    while 1:
        B, N = map(int, input().split())
        if B == 0:
            break
        rs = list(map(int, input().split()))
        debs = []
        for i in range(N):
            d, c, v = map(int, input().split())
            rs[d - 1] -= v
            rs[c - 1] += v
        m = min(rs)
        if m < 0:
            print('N')
        else:
            print('S')

#main(input)

with open('E:/coding/codeforces/python/python/onlinejudge/1.3.3/11679_test.txt') as f:
    main(f.readline)
