i = 1
while 1:
    n = int(input())
    if n == 0: break
    a = list(map(int, input().split()))
    c = a.count(0)
    print(f'Case {i}: {n - c - c}')
    i += 1