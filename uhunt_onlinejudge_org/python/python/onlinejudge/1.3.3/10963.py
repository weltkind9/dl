def main(input):
    n = int(input())

    for i in range(n):
        input()
        t = int(input())
        gap = None
        r = 'yes'
        for j in range(t):
            y1, y2 = map(int, input().split())
            if gap is None:
                gap = y1 - y2
            else:
                if gap != y1 - y2:
                    r = 'no'
        print(r)
#main(input)

with open('E:/coding/codeforces/python/python/onlinejudge/10963_test.txt') as f:
    main(f.readline)

