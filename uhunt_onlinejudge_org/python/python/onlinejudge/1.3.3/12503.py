def main(input):
    t = int(input())

    for i in range(t):
        r = 0
        d = []
        n = int(input())
        for j in range(n):
            s = input()
            if 'SAME' in s:
                a = int(s.split()[2])
                s = d[a-1]
            d.append(s)
            if 'LEFT' in s:
                r -= 1
            else:
                r += 1

        print(r)
    
#main(input)

with open('E:/coding/codeforces/python/python/onlinejudge/1.3.3/12503_test.txt') as f:
    main(f.readline)
