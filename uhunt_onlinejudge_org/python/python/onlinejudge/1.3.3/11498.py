while 1:
    K = int(input())
    if K == 0:
        break
    dpX, dpY = map(int, input().split())
    while K > 0:
        x, y = map(int, input().split())
        if x == dpX or y == dpY:
            print('divisa')
        elif x < dpX:
            if y < dpY:
                print('SO')
            else:
                print('NO')
        else:
            if y < dpY:
                print('SE')
            else:
                print('NE')

        K -= 1
