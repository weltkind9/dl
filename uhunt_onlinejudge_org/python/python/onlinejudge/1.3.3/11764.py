def main(input):
    t = int(input())

    for i in range(t):
        n = int(input())
        w = list(map(int, input().split()))
        h = 0
        l = 0
        for j in range(len(w) - 1):
            if w[j] > w[j + 1]:
                h += 1
            else:
                l += 1

        print(f'Case {i+1}: {h} {l}')
#main(input)

with open('E:/coding/codeforces/python/python/onlinejudge/1.3.3/11764_test.txt') as f:
    main(f.readline)
