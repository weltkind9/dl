while 1:
    s = list(input())
    c = 0
    for i in range(len(s)-1, -1, -1):
        if s[i] == '"':
            if c % 2 == 0:
                s[i] = "'"
                s.insert(i, "'")
            else:
                s[i] = "`"
                s.insert(i, "`")        
            c += 1
    print(''.join(s))