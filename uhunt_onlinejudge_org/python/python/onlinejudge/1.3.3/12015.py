def main(input):
    t = int(input())

    for i in range(t):        
        dd = {}
        for j in range(10):
            d, n = input().split()
            n = int(n)
            if not n in dd:
                dd[n] = []
            dd[n].append(d)
        mk = max(dd.keys())
        r = '\n'.join(dd[mk])
        print(f'Case #{i+1}:\n{r}')
#main(input)

with open('E:/coding/codeforces/python/python/onlinejudge/1.3.3/12015_test.txt') as f:
    main(f.readline)
