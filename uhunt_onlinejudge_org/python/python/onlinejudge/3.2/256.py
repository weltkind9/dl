def prepare():    
    for n in (2,4,6,8):
        for i in range(int('9'*n)):
            s = str(i).rjust(n, '0')
            s1 = s[:n//2:]
            s2 = s[n//2::]
            i1 = int(s1)
            i2 = int(s2)
            if (i1+i2)*(i1+i2) == i:
                result[n].append(s)
                print(s)
    return result
#prepared = prepare()
precalc = {2:['00', '01', '81'],
           4:['0000', '0001', '2025', '3025', '9801'],
           6:['000000', '000001', '088209', '494209', '998001'],
           8:['00000000', '00000001', '04941729', '07441984', '24502500', '25502500', '52881984', '60481729', '99980001']}
def main(input, print):
    while 1:
        try:
            s = input()
            if s == '' or s == '\n':
                break
            n = int(s)
            print('\n'.join(precalc[n]))
        except:
            break


#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)