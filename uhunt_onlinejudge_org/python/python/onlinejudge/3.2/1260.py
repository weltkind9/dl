
import time

def main(input, print):
    t = int(input())
    for _ in range(t):
        n = int(input())
        aa = list(map(int, input().split()))
        bb = [0] * (n-1)

        for i in range(1, n):
            b = 0
            for j in range(0, i):
                if aa[j] <= aa[i]:
                    b += 1
            bb[i-1] = b

        print(sum(bb))



#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)

