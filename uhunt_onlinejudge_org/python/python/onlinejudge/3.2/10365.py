import time

def main(input, print):
    t = int(input())
    toPrint = []
    for _ in range(t):
        n = int(input())
        min_paper_area = 100000000
        for s_osn in range(1, n//2+2):
            h = n // s_osn
            if h * s_osn != n:
                continue

            for a in range(1, s_osn//2+2):
                b = s_osn // a
                if b * a != s_osn:
                    continue
                paper_area = 2 * (a+b) * h + 2*a*b
                if paper_area < min_paper_area:
                    min_paper_area = paper_area
        toPrint.append(str(min_paper_area))
    print('\n'.join(toPrint))

t1 = time.time()
#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)

print(time.time() - t1)