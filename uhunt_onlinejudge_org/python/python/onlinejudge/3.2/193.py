
import time
from collections import defaultdict


def isContains(node: int, seq :set):
    for c in seq:
        if node in seq:
            return True
    return False
count =0
def coloring(cur_index: int, vertices : list, la:dict, black:set, white:set, black_result:set, max_len: int):
    global count
    count+=1
    for i in range(cur_index, len(vertices)):
        cur_node = vertices[i]
        if cur_node in white or cur_node in black:
            continue
        neib = la[cur_node]
        for n in neib:
           if isContains(n, black):
               break
        else:
            coloring(cur_index+1, vertices, la, black.union((cur_node,)), white.union(neib), black_result, max_len)   
        white.add(cur_node)
        #coloring(cur_index+1, vertices, la, black, white, black_result)
    r = frozenset(sorted(black))
    black_result.add(r)
    if len(r) > max_len:
        max_len = len(r)


def main(input, print):
    toPrint=[]
    m = int(input())
    for _ in range(m):
        n, k = map(int, input().split())
        AL = defaultdict(set)
        vertices = set()
        black=set()
        white=set()
        black_result = set()

        for i in range(k):
            a, b = map(int, input().split())
            AL[a].add(b)
            AL[b].add(a)
            vertices.add(a)
            vertices.add(b)
        input()
        leafs = set(range(1,n+1)).difference(vertices)
        coloring(0, list(vertices), AL, black, white, black_result, 0)
        mx = sorted(black_result, key=lambda x:len(x))[-1]
        mx = mx.union(leafs)
        ss = ' '.join(map(str, mx))
        toPrint.append(f'{len(mx)}\n{ss}')

    print('\n'.join(toPrint))
    
t1 = time.time()

#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)
print(time.time()-t1)