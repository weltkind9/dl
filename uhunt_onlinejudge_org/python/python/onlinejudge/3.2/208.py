
import time
from collections import defaultdict
def floyed_warshall(AdjMat, n):
    for k in range(n):
        for i in range(n):
            for j in range(n):
                AdjMat[i][j] = min(AdjMat[i][j], AdjMat[i][k] + AdjMat[k][j])


def find_path(curr_node: int, curr_path: list, AL:dict, target: int, result: set):
    neib = AL[curr_node]
    for n in neib:
        if n in curr_path: continue

        curr_path.append(n)
        if n == target:
            result.add(tuple(curr_path))
        else:
            find_path(n, curr_path, AL, target, result)
        curr_path.remove(n)

    
def work(input, print, case):
    target = int(input())
    AL = defaultdict(set)
    AdjMat = [[100500]*22 for i in range(22)]
   
    while 1:
        a, b = map(int, input().split())
        if a == 0 and b == 0:
            break
        AL[a].add(b)
        AL[b].add(a)
        AdjMat[a][b] = 1
        AdjMat[b][a] = 1
    floyed_warshall(AdjMat, 22)

    ss = f'CASE {case}:\n'
    if target == 1:
        result = set((1,))
        ss += '1\n'
    elif AdjMat[1][target] == 100500:
        result = set()
    else:
        curr_path=[]
        curr_path.append(1)
        result = set()
        find_path(1, curr_path, AL,target,  result)
        rr = sorted(result)
        #ss += '\n'.join(rr)
        for r in rr:
        #    #ss += ' ' + ' '.join(map(str, r)) + '\n'
           ss += ' '.join(map(str, r)) + '\n'
        #ss+='\n'
    ss += f'There are {len(result)} routes from the firestation to streetcorner {target}.'
    return ss

def main(input, print):
    toPrint=[]
    case = 1
    while 1:
        try:
            ss = work(input, print, case)
            toPrint.append(ss)
            case += 1
        except Exception as ex:
            break
    print('\n'.join(toPrint))
    
t1 = time.time()

#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)
print(time.time()-t1)