import time

class Pop:
    def __init__(self, x, y, size):
        self.x = x
        self.y = y
        self.size = size


def main(input, print):
    t = int(input())
    toPrint = []
    for _ in range(t):
        d = int(input())
        n = int(input())
        ans = [[0]*1025 for i in range(1025)]
        for i in range(n):
            s = input().split()
            p = Pop(int(s[0]), int(s[1]), int(s[2]))
            for x in range(max(0, p.x-d), min(1025, p.x+d+1)):
                for y in range(max(0, p.y-d), min(1025, p.y+d+1)):
                    ans[x][y] += p.size
        input()
        maxSize = 0; maxX = 0; maxY = 0
        for x in range(1025):
            for y in range(1025):
                if ans[x][y] > maxSize:
                    maxX = x
                    maxY = y
                    maxSize = ans[x][y]
        toPrint.append(f'{maxX} {maxY} {maxSize}')
    print('\n'.join(toPrint))

t1 = time.time()
#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)

print(time.time() - t1)