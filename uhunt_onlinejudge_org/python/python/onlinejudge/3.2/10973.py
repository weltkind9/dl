import time
from collections import defaultdict

def main(input, print):
    t = int(input())
    toPrint=[]
    for _ in range(t):
        n, m = map(int, input().split())
        edges = set()
        vertices = set()
        
        AL = defaultdict(set) #initalize AL

        for i in range(m):
            a, b = map(int, input().split())
            edges.add((a,b))

            vertices.add(a)
            vertices.add(b)
            
            AL[a].add(b)
            AL[b].add(a)

        triangl = set()
        for e in edges:
            a, b = e[0], e[1]
            for v in vertices:
                if a == b or a == v or b == v:
                    continue
                if  v in AL[a] and v in AL[b]:
                    triangl.add(str(sorted([a, b, v])))
              
        toPrint.append(str(len(triangl)))
    print('\n'.join(toPrint))
    
t1 = time.time()
#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)

print(time.time()-t1)