import time
def work2(k):
    result = []
    for y in range(2, k*2+1):
        if y == k:
            continue
        x = k*y // (y - k)
        if x < 1:
            continue
        if x*y == k*(x+y):
            result.append(f'1/{k} = 1/{x} + 1/{y}')
    return result
def work(k):
    result = []
    for y in range(k+1, k*2+1):
        x = k*y // (y - k)
        if x < 1:
            continue
        if x*y == k*(x+y):
            result.append(f'1/{k} = 1/{x} + 1/{y}')
    return result

def main(input, print):
    while 1:
        try:
            s = input()
            if s == '' or s == '\n':
                break
            k = int(s)
            result = work(k)
            print(len(result))
            print('\n'.join(result))
        except Exception as ex:
            break


def time_test():
    for _ in range(100):
        result = work(10000)
        print(len(result))
        print('\n'.join(result))
t1 = time.time()
time_test()
print(time.time()-t1)

def correct_test():
     for i in range(100):
        result = work(10000-i)
        result2 = work2(10000-i)
        assert ''.join(result) == ''.join(result2)

        print(len(result))
        print('\n'.join(result))

correct_test()
#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)

