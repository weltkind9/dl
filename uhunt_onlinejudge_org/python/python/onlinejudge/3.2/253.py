def rotate(arr_cube2: list, cube1: str, cube2: str):
    result = set()
    for a in arr_cube2:
        c = list(a)
        result.add(''.join([c[2],c[1],c[5],c[0],c[4],c[3]]))
        result.add(''.join([c[5],c[1],c[3],c[2],c[4],c[0]]))
        result.add(''.join([c[3],c[1],c[0],c[5],c[4],c[2]]))    
 
        result.add(''.join([c[1],c[5],c[2],c[3],c[0],c[4]]))
        result.add(''.join([c[5],c[4],c[2],c[3],c[1],c[0]]))
        result.add(''.join([c[4],c[0],c[2],c[3],c[5],c[1]]))
       
        result.add(''.join([c[0],c[2],c[4],c[1],c[3],c[5]]))
        result.add(''.join([c[0],c[4],c[3],c[2],c[1],c[5]]))
        result.add(''.join([c[0],c[3],c[1],c[4],c[2],c[5]]))   
    
    return result

def work(cube1: str, cube2: str):
    if cube1 == cube2: return True
    c = list(cube2)
    if cube1 == ''.join([c[2],c[1],c[5],c[0],c[4],c[3]]): return True
    if cube1 == ''.join([c[5],c[1],c[3],c[2],c[4],c[0]]): return True
    if cube1 == ''.join([c[3],c[1],c[0],c[5],c[4],c[2]]): return True

    if cube1 == ''.join([c[1],c[5],c[2],c[3],c[0],c[4]]): return True
    if cube1 == ''.join([c[5],c[4],c[2],c[3],c[1],c[0]]): return True
    if cube1 == ''.join([c[4],c[0],c[2],c[3],c[5],c[1]]): return True

    if cube1 == ''.join([c[0],c[2],c[4],c[1],c[3],c[5]]): return True
    if cube1 == ''.join([c[0],c[4],c[3],c[2],c[1],c[5]]): return True
    if cube1 == ''.join([c[0],c[3],c[1],c[4],c[2],c[5]]): return True   
    
    return False

def main(input, print):
    t = 1
    result = []
    while 1:
        try:
            s = input()
            if s == '' or s == '\n' or len(s) < 12:
                break

            cube1 = s[:6]
            cube2 = s[6:12]

            res = set()
            res.add(cube2)
            while 1:
                r = rotate(res, cube1, cube2)
                nw = r - res
                if len(nw) == 0:
                    break
                res = res.union(r)

            for r in res:
                if r == cube1:
                    result.append("TRUE")
                    break
            else:
                result.append("FALSE")

        except Exception as ex:
            break
    print('\n'.join(result))
  


#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)

