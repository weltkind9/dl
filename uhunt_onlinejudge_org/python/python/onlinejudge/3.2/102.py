from collections import namedtuple
Bin = namedtuple('Bin', ['value', 'name'])
check = ['BGC', 'BCG', 'CBG', 'CGB', 'GBC', 'GCB']

def main(input, print):
    while 1:
        try:
            s = input()
            if s == '' or s == '\n':
                break
            result = []
            b1, g1, c1, b2, g2, c2, b3, g3, c3 = map(int, s.split())
            bins = [[b1, g1, c1],
                    [b2, g2, c2],
                    [b3, g3, c3]]
            b = sum([bins[i][0:1][0] for i in range(3)])
            g = sum([bins[i][1:2][0] for i in range(3)])
            c = sum([bins[i][2:3][0] for i in range(3)])


            for r in check:
                b_ind = r.index('B')
                g_ind = r.index('G')
                c_ind = r.index('C')
                result.append(Bin((b - bins[b_ind][0]) + (g - bins[g_ind][1]) + (c - bins[c_ind][2]), r))

            rr = sorted(result)
            print(f'{rr[0].name} {rr[0].value}')
            result.clear()
        except:
            break
    


#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)