def main(input, print):
    t = 1
    while 1:
        n = int(input())
        if n == 0:
            break

        print(f'Case {t}:')
        arr = [0] * n
        for i in range(n):
            arr[i] = int(input())

        sumArr = []
        for i in range(n-1):
            for j in range(i+1, n):
                sumArr.append(arr[i] + arr[j])

        m = int(input())

        for i in range(m):
            q = int(input())
            minSum = 1000000000
            for s in sumArr:
                if abs(s - q) < abs(minSum - q):
                    minSum = s

            print(f'Closest sum to {q} is {minSum}.')

        t += 1
  


#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)

