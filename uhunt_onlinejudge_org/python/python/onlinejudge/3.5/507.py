
import time

def main(input, print):
    
    toPrint = []
    t = int(input()) 
    for r in range(t):
        n = int(input())
        arr = []
        for i in range(n-1):
            arr.append(int(input()))
        way = [0] * (n-1)
        way[0] = arr[0]
        maxNice = arr[0]
        indMaxNice = []
        indMaxNice.append(0)
        for i in range(1, n-1):
            c = way[i-1] + arr[i]
            way[i] = max(c, arr[i])
            if way[i] >= maxNice:
                maxNice = way[i]
                indMaxNice.append(i)
        
        if len(indMaxNice) == 0  or max(way) < 0:
            toPrint.append(f'Route {r+1} has no nice parts')
        else:
            selEnd = indMaxNice[-1]
            selStart = indMaxNice[-1]

            for i in range(len(indMaxNice)-1, -1,-1):
                if way[indMaxNice[i]] < way[indMaxNice[-1]]:
                    continue
                ind = indMaxNice[i]
                end = ind
                start = ind
                while start-1 >= 0:
                    if way[start-1] < 0:
                        break
                    start -= 1
                if ((selEnd-selStart) <= (end-start)) and (start < selStart):
                    selEnd = ind
                    selStart = start

            toPrint.append(f'The nicest part of route {r+1} is between stops {selStart + 1} and {selEnd + 2}')

    print('\n'.join(toPrint))



t1 = time.time()
#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)
print(time.time() - t1)