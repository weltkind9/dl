from collections import defaultdict
import string

dd = defaultdict(int)
ltrs = string.ascii_lowercase
arr = []

def f5(arr, ltrs, dd, c):
    if len(arr) == 0: i = 0
    else: i = ltrs.index(arr[-1]) + 1

    for i1 in range(i, len(ltrs)):
        arr.append(ltrs[i1])

        if len(arr) == 0: j = 0
        else: j = ltrs.index(arr[-1]) + 1

        for j1 in range(j, len(ltrs)):
            arr.append(ltrs[j1])
            if len(arr) == 0: k = 0
            else: k = ltrs.index(arr[-1]) + 1

            for k1 in range(k, len(ltrs)):
                arr.append(ltrs[k1])
                if len(arr) == 0: l = 0
                else: l = ltrs.index(arr[-1]) + 1

                for l1 in range(l, len(ltrs)):
                    arr.append(ltrs[l1])
                    if len(arr) == 0: m = 0
                    else: m = ltrs.index(arr[-1]) + 1

                    for m1 in range(m, len(ltrs)):
                        arr.append(ltrs[m1])
                        k = ''.join(arr)
                        dd[k] = c
                        #print(f'{k} -> {dd[k]}')
                        c += 1
                        arr.pop()
                    arr.pop()
                arr.pop()
            arr.pop()

        arr.pop(0)

    return c
def f4(arr, ltrs, dd, c):
    if len(arr) == 0: j = 0
    else: j = ltrs.index(arr[-1]) + 1

    for j1 in range(j, len(ltrs)):
        arr.append(ltrs[j1])
        if len(arr) == 0: k = 0
        else: k = ltrs.index(arr[-1]) + 1

        for k1 in range(k, len(ltrs)):
            arr.append(ltrs[k1])
            if len(arr) == 0: l = 0
            else: l = ltrs.index(arr[-1]) + 1

            for l1 in range(l, len(ltrs)):
                arr.append(ltrs[l1])
                if len(arr) == 0: m = 0
                else: m = ltrs.index(arr[-1]) + 1

                for m1 in range(m, len(ltrs)):
                    arr.append(ltrs[m1])
                    k = ''.join(arr)
                    dd[k] = c
                    #print(f'{k} -> {dd[k]}')
                    c += 1
                    arr.pop()
                arr.pop()
            arr.pop()
        arr.pop(0)
    return c

def f3(arr, ltrs, dd, c):

    if len(arr) == 0: k = 0
    else: k = ltrs.index(arr[-1]) + 1
    
    for k1 in range(k, len(ltrs)):
        arr.append(ltrs[k1])
        if len(arr) == 0: l = 0
        else: l = ltrs.index(arr[-1]) + 1
    
        for l1 in range(l, len(ltrs)):
            arr.append(ltrs[l1])
            if len(arr) == 0: m = 0
            else: m = ltrs.index(arr[-1]) + 1
    
            for m1 in range(m, len(ltrs)):
                arr.append(ltrs[m1])
                k = ''.join(arr)
                dd[k] = c
                #print(f'{k} -> {dd[k]}')
                c += 1
                arr.pop()
            arr.pop()
        arr.pop(0)
    return c

def f2(arr, ltrs, dd, c):
    if len(arr) == 0: l = 0
    else: l = ltrs.index(arr[-1]) + 1

    for l1 in range(l, len(ltrs)):
        arr.append(ltrs[l1])
        if len(arr) == 0: m = 0
        else: m = ltrs.index(arr[-1]) + 1

        for m1 in range(m, len(ltrs)):
            arr.append(ltrs[m1])
            k = ''.join(arr)
            dd[k] = c
            #print(f'{k} -> {dd[k]}')
            c += 1
            arr.pop()
        arr.pop(0)
    return c


def f1(arr, ltrs, dd, c):
    if len(arr) == 0: m = 0
    else: m = ltrs.index(arr[-1]) + 1

    for m1 in range(m, len(ltrs)):
        arr.append(ltrs[m1])
        k = ''.join(arr)
        dd[k] = c
        #print(f'{k} -> {dd[k]}')
        c += 1
        arr.pop()
    return c
c1 = f1(list(''), ltrs, dd, 1)
c2 = f2(list(''), ltrs, dd, c1)
c3 = f3(list(''), ltrs, dd, c2)
c4 = f4(list(''), ltrs, dd, c3)
c5 = f5(list(''), ltrs, dd, c4)

def main(input, print):
    inp = []
    res = []
    while 1:
        try:
            s = input().strip()        
            if s == None or s == '' or s == '\n':
                break
            inp.append(s)
        except:
            break
    for s in inp:
        res.append(str(dd.get(s, 0)))
    print('\n'.join(res))
    


#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)