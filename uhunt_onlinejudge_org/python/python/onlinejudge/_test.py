from collections import OrderedDict
  
print("This is a Dict:\n")
d = {}
d['a'] = 1
d['d'] = 2
d['c'] = 3
d['b'] = 4
  
for key, value in d.items():
    print(key, value)
  
print("\nThis is an Ordered Dict:\n")
od = OrderedDict()
od['a'] = 1
od['d'] = 2
od['c'] = 3
od['b'] = 4
  
for key, value in od.items():
    print(key, value)

print("\nThis is an Ordered Dict 2:\n")

for key in od:
    print(key)


print("\nThis is an Ordered Dict 3:\n")
c = 10
print(''.join(od))
from sortedcontainers import SortedDict

from sortedcontainers import SortedDict
print()