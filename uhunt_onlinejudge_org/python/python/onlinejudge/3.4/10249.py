import time
import heapq
#[heapq.heappop(tables) for i in range(5)]
def seat(tables, teams, teams_count, tables_count):
    #tms = sorted([(teams[i], i) for i in range(teams_count)], key= lambda x: x[0], reverse= True)
    tables = [(-tables[i], i+1) for i in range(tables_count)]
    heapq.heapify(tables)
    result = [[] for i in range(teams_count)]
    for j in range(teams_count):
        
        team = teams[j]
        for i in range(team):
            #try:
            tmp = []
            tbl = heapq.heappop(tables)
            while tbl[1] in result[j]:
                try:
                    tmp.append(tbl)
                    tbl = heapq.heappop(tables)
                except Exception as ex:
                    return '0'

            #if abs(tbl[0]) < team:
            #    return '0'
            if tbl[0] == 0:
                return '0'
            result[j].append(tbl[1])
            for tt in tmp:
                heapq.heappush(tables, tt)
            heapq.heappush(tables, (tbl[0]+1, tbl[1]))
            #except Exception as ex:
            #        pass
    return '1\n' + '\n'.join([' '.join(map(str, sorted(r))) for r in result])

def main(input, print):
    
    toPrint = []

    while 1:
        teams_count, tables_count = map(int, input().split())
        if teams_count == 0 and tables_count == 0:
            break
        teams = list(map(int, input().split()))
        tables = list(map(int, input().split()))
        toPrint.append(seat(tables, teams, teams_count, tables_count))

    print('\n'.join(toPrint))



t1 = time.time()
#main(input, print)
with open(__file__.replace('.py','t.txt')) as read:
    with open(__file__.replace('.py','_ans.txt'), 'w') as write:
        main(read.readline, write.write)
        #main(read.readline, print)
print(time.time() - t1)