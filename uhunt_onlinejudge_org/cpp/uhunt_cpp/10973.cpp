// uhunt_cpp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include <ctime>
#include <chrono>

using namespace std;
using namespace std::chrono;

typedef high_resolution_clock Clock;
typedef Clock::time_point ClockTime;
void printExecutionTime(ClockTime start_time, ClockTime end_time);

void task10973();
int AM[3001][3001];

int main()
{
    ClockTime start_time = Clock::now();

    task10973();
    ClockTime end_time = Clock::now();
    printExecutionTime(start_time, end_time);
}
void task10973()
{
    ifstream cin("10973t.txt");
    ofstream cout("10973_ans.txt");

    int t;
    cin >> t;
    //scanf("%d", &t);
    while (t > 0)
    {
        int count, n, m;
        //scanf("%d %d", &n, &m);
        cin >> n >> m;
        int a, b;
        count = 0;
        for (int i = 0; i < n + 1; i++)
            for (int j = 0; j < n + 1; j++)
                AM[i][j] = 0;
        while (m > 0)
        {
            cin >> a >> b;
            //scanf("%d %d", &a, &b);
            AM[a][b] = 1;
            AM[b][a] = 1;
            m--;
        }

        for (int i = 1; i < n - 1; i++)
        {
            for (int j = i + 1; j < n; j++)
            {
                for (int k = j + 1; k < n + 1; k++)
                {
                    if (AM[i][j] == 1 && AM[i][k] == 1 && AM[j][k] == 1)
                        count++;
                }
            }
        }
        printf("%d\n", count);
        cout << count << endl;
        t--;
    }
}
