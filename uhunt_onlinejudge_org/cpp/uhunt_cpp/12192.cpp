// uhunt_cpp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cstdio>

// C
#ifndef _GLIBCXX_NO_ASSERT
#include <cassert>
#endif
#include <cctype>
#include <cerrno>
#include <cfloat>
#include <ciso646>
#include <climits>
#include <clocale>
#include <cmath>
#include <csetjmp>
#include <csignal>
#include <cstdarg>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

#if __cplusplus >= 201103L
#include <ccomplex>
#include <cfenv>
#include <cinttypes>
#include <cstdalign>
#include <cstdbool>
#include <cstdint>
#include <ctgmath>
#include <cwchar>
#include <cwctype>
#endif

// C++
#include <algorithm>
#include <bitset>
#include <complex>
#include <deque>
#include <exception>
#include <fstream>
#include <functional>
#include <iomanip>
#include <ios>
#include <iosfwd>
#include <iostream>
#include <istream>
#include <iterator>
#include <limits>
#include <list>
#include <locale>
#include <map>
#include <memory>
#include <new>
#include <numeric>
#include <ostream>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <streambuf>
#include <string>
#include <typeinfo>
#include <utility>
#include <valarray>
#include <vector>

#if __cplusplus >= 201103L
#include <array>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <forward_list>
#include <future>
#include <initializer_list>
#include <mutex>
#include <random>
#include <ratio>
#include <regex>
#include <scoped_allocator>
#include <system_error>
#include <thread>
#include <tuple>
#include <typeindex>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#endif

#include <ctime>

using namespace std;
using namespace std::chrono;

typedef high_resolution_clock Clock;
typedef Clock::time_point ClockTime;
void printExecutionTime(ClockTime start_time, ClockTime end_time);
void task12192();

int main()
{        
    ClockTime start_time = Clock::now();

    task12192();
    ClockTime end_time = Clock::now();
    printExecutionTime(start_time, end_time);

}
int binary_search_find_index_lower(std::vector<int> v, int data) {
    auto it = std::lower_bound(v.begin(), v.end(), data);
    if (it == v.end() /* || *it != data*/) {
        return -1;
    }
    else {
        std::size_t index = std::distance(v.begin(), it);
        return index;
    }
}
int binary_search_find_index_upper(std::vector<int> v, int data) {
    auto it = std::upper_bound(v.begin(), v.end(), data);
    if (it == v.end() /* || *it != data*/) {
        return -1;
    }
    else {
        std::size_t index = std::distance(v.begin(), it);
        return index;
    }
}
void task12192() {
    ifstream cin("12192t.txt");
    ofstream cout("12192_ans.txt");
    vector<vector<int>> vct;
    for (int i = 0; i < 501; i++) {
        vct.push_back(vector<int>());
        for (int j = 0; j < 501; j++)
        {
            vct[i].push_back(0);
        }
    }
    int n, m, q, a, b, hi, lo, max_side;
    int row = 0;
    while (1) {
        cin >> n >> m;
        if (n == 0 && m == 0)
            break;
        for (int i = 0; i < n; i++) {
            vct.push_back(vector<int>());
            for (int j = 0; j < m; j++)
            {
                cin >> vct[i][j];
            }
        }
        cin >> q;
        for (int i = 0; i < q; i++)
        {
            cin >> a >> b;
            lo = m + 1;
            hi = -1;
            max_side = 0;
            for (int j = 0; j < n; j++)
            {
                auto lft = lower_bound(vct[j].begin(), vct[j].begin()+m, a);
                if (lft == vct[j].begin() + m)
                    continue;
                int lft_ind = distance(vct[j].begin(), lft);
                auto rght = upper_bound(vct[j].begin(), vct[j].begin()+m, b);
                if (rght == vct[j].begin())
                    continue;
                int side_hor = rght - lft;
                if (side_hor < max_side)
                    continue;
                int side_vert = min(side_hor, n - j);
                while (side_vert >= 0) {
                    if (side_vert <= max_side)
                        break;
                    if (vct[j + side_vert - 1][lft_ind] <= b)
                        break;
                    side_vert--;
                }
                if (side_vert <= max_side)
                    continue;
                int side = min(side_vert, side_hor);
                while (side >= 0)
                {
                    if (side <= max_side)
                        break;
                    if (vct[j + side - 1][lft_ind + side - 1] <= b) {
                        max_side = side;
                        break;
                    }
                    side--;
                }
            }
            cout << max_side << endl;
            row++;
        }
        cout << "-" << endl;
        row++;
    }
}
void task12192_test() {
    ifstream cin("12192t.txt");
    //ofstream cout("12192_ans.txt");
    int n = 10, m = 5;
    int arr[n][m];
    vector<vector<int>> vct;
    /*for (int i = 0; i < n; i++) {

        for (int j = 0; j < m; j++)
        {
            arr[i][j] = i * 2 + j;
            cout << arr[i][j] << " ";
        }
        cout << endl;
    }*/
    for (int i = 0; i < n; i++) {
        vct.push_back(vector<int>());
        for (int j = 0; j < m; j++)
        {
            vct[i].push_back(i * 2 + j);
            cout << "j:" << j << " " << vct[i][j] << "       ";
        }
        cout << endl;
    }
    for (int i = 0; i < n; i++) {
        int   il = binary_search_find_index_lower(vct[i], 10);
        int   iu = binary_search_find_index_upper(vct[i], 10);
        if (il >= 0 && il < m)
            cout << "arr[" << il << "]=" << vct[i][il] << " ";
        else
            cout << -1 << " ";
        if (iu >= 0 && iu < m)
            cout << "arr[" << iu << "]=" << vct[i][iu] << endl;
        else
            cout << -1 << endl;
    }


}