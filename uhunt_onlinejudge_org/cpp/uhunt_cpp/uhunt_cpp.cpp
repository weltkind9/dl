// uhunt_cpp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cstdio>

// C
#ifndef _GLIBCXX_NO_ASSERT
#include <cassert>
#endif
#include <cctype>
#include <cerrno>
#include <cfloat>
#include <ciso646>
#include <climits>
#include <clocale>
#include <cmath>
#include <csetjmp>
#include <csignal>
#include <cstdarg>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

#if __cplusplus >= 201103L
#include <ccomplex>
#include <cfenv>
#include <cinttypes>
#include <cstdalign>
#include <cstdbool>
#include <cstdint>
#include <ctgmath>
#include <cwchar>
#include <cwctype>
#endif

// C++
#include <algorithm>
#include <bitset>
#include <complex>
#include <deque>
#include <exception>
#include <fstream>
#include <functional>
#include <iomanip>
#include <ios>
#include <iosfwd>
#include <iostream>
#include <istream>
#include <iterator>
#include <limits>
#include <list>
#include <locale>
#include <map>
#include <memory>
#include <new>
#include <numeric>
#include <ostream>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <streambuf>
#include <string>
#include <typeinfo>
#include <utility>
#include <valarray>
#include <vector>

#if __cplusplus >= 201103L
#include <array>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <forward_list>
#include <future>
#include <initializer_list>
#include <mutex>
#include <random>
#include <ratio>
#include <regex>
#include <scoped_allocator>
#include <system_error>
#include <thread>
#include <tuple>
#include <typeindex>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#endif

#include <ctime>

using namespace std;
using namespace std::chrono;

typedef high_resolution_clock Clock;
typedef Clock::time_point ClockTime;
void printExecutionTime(ClockTime start_time, ClockTime end_time);
void task10360();
void task10973_v1();
static int ans[1025][1025];

int main_v1()
{
    task10973_v1();
}

void task10973_v1() {



    ifstream cin("10973t.txt");
    ofstream cout("10973_ans.txt");
    int t;
    cin >> t;
    while (t>0)
    {
        ClockTime start_time = Clock::now();

        int count = 0, n, m;
        cin >> n >> m;
        unordered_map<int,set<int> > edges ;
        set<int > vertices ;
            int a, b;
        while (m > 0)
        {
            cin >> a >> b;
            if (edges.find(a) == edges.end())
                edges[a] = set<int>{};
            edges[a].insert(b);

            if (edges.find(b) == edges.end())
                edges[b] = set<int>{};
            edges[b].insert(a);

            vertices.insert(a);
            vertices.insert(b);
            m--;
        }
        ClockTime end_time = Clock::now();
        printExecutionTime(start_time, end_time);

        int vert[n];
        int i = 0;
        for (auto v : vertices)
            vert[i++] = v;
         end_time = Clock::now();
        printExecutionTime(start_time, end_time);
        printf("n: %d\n", n);

        for (int i = 0; i < n-2; i++)
        {
            for (int j = i + 1; j < n - 1; j++)
            {
                for (int k = j + 1; k < n; k++)
                {
                    set<int> a = edges[vert[i]], b = edges[vert[j]], c = edges[vert[k]];

                    if (a.find(vert[j]) != a.end() &&
                        b.find(vert[k]) != b.end() &&
                        c.find(vert[i]) != c.end())
                        count++;/**/
                }
            }
        }
         end_time = Clock::now();
        printExecutionTime(start_time, end_time);
        printf("%d\n", count);
        t--;
    }
}
void task10360()
{
    time_t begin, end;
    time(&begin);

    ClockTime start_time = Clock::now();

    ifstream cin("10360t.txt");
    ofstream cout("10360_ans.txt");
    int n = 0, d = 0, t = 0;
    cin >> t;
    for (int i = 0; i < t; i++)
    {
        cin >> d;
        cin >> n;
        for (int j = 0; j < 1025; j++)
            for (int k = 0; k < 1025; k++)
                ans[j][k] = 0;
        int x, y, size;
        for (int j = 0; j < n; j++)
        {
            cin >> x >> y >> size;
            int x1 = max(0, x - d), x2 = min(1025, x + d + 1),
                y1 = max(0, y - d), y2 = min(1025, y + d + 1);
            for (int xx = x1; xx < x2; xx++)
                for (int yy = y1; yy < y2; yy++)
                    ans[xx][yy] += size;
        }
        int maxSize = 0, maxX = 0, maxY = 0;
        for (int x = 0; x < 1025; x++) {
            for (int y = 0; y < 1025; y++)
            {
                if (ans[x][y] > maxSize)
                {
                    maxX = x;
                    maxY = y;
                    maxSize = ans[x][y];
                }
            }
        }
        cout << maxX << " " << maxY << " " << maxSize << endl;
    }

    ClockTime end_time = Clock::now();
    printExecutionTime(start_time, end_time);

    time(&end);
    double difference = difftime(end, begin);
    printf("time taken for function() %.4lf seconds.\n", difference);
}
void printExecutionTime(ClockTime start_time, ClockTime end_time)
{
    auto execution_time_ns = duration_cast<nanoseconds>(end_time - start_time).count();
    auto execution_time_ms = duration_cast<microseconds>(end_time - start_time).count();
    auto execution_time_sec = duration_cast<seconds>(end_time - start_time).count();
    auto execution_time_min = duration_cast<minutes>(end_time - start_time).count();
    auto execution_time_hour = duration_cast<hours>(end_time - start_time).count();

    cout << "\nExecution Time: ";
    if (execution_time_hour > 0)
        cout << "" << execution_time_hour << " Hours, ";
    if (execution_time_min > 0)
        cout << "" << execution_time_min % 60 << " Minutes, ";
    if (execution_time_sec > 0)
        cout << "" << execution_time_sec % 60 << " Seconds, ";
    if (execution_time_ms > 0)
        cout << "" << execution_time_ms % long(1E+3) << " MicroSeconds, ";
    if (execution_time_ns > 0)
        cout << "" << execution_time_ns % long(1E+6) << " NanoSeconds, ";
    cout << endl;
}