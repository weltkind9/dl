﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnakeLibs
{
  public  class Node
    {
        public Node(int _number)
        {
            number = _number;
        }

        public Node parent;
        public Node child;

        public int x;
        public int y;
        public int number;
    }

}
