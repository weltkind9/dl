﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SnakeLibs
{
    public class Table
    {
        Random rnd = new Random(DateTime.UtcNow.Second * DateTime.UtcNow.Minute);
        public int maxX, minX, maxY, minY;

        public int[,] apples;
        public Table(int minY, int maxY, int minX, int maxX)
        {
            head = new Node(1) { x = 0, y = 0 };
            this.maxX = maxX;
            this.minX = minX;
            this.maxY = maxY;
            this.minY = minY;
            tail = head;
            apples = new int[maxX - minX, maxY - minY];
        }
        public Node head, tail;
        public Direction curDir;
        public bool start = false;
        public void NewApple()
        {
            int x = rnd.Next(minX, maxX - minX - 1);
            int y = rnd.Next(minY, maxY - minY - 1);
            apples[x, y] = 1;
        }
        public void ChangeDirection(Direction newDir)
        {
            if ((newDir == Direction.down || newDir == Direction.up) && (curDir == Direction.down || curDir == Direction.up))
                return;
            if ((newDir == Direction.right || newDir == Direction.left) && (curDir == Direction.right || curDir == Direction.left))
                return;
            curDir = newDir;
        }
        public string Draw(Node head)
        {
            List<List<string>> snake = new List<List<string>>();
            List<List<string>> apps = new List<List<string>>();
            List<List<string>> walls = new List<List<string>>();
            for (int i = 0; i < maxX - minX; i++)
            {
                List<string> s1 = new List<string>();
                List<string> s2 = new List<string>();
                List<string> s3 = new List<string>();
                for (int j = 0; j < maxY - minY; j++)
                {
                    s1.Add(" ");
                    s2.Add(" ");
                    s3.Add(" ");
                }
                snake.Add(s1);
                apps.Add(s2);
                walls.Add(s3);
            }
            //Console.Clear();
            for (Node node = head; node != null; node = node.child)
            {
                //Console.SetCursorPosition(node.x, node.y);
                //Console.Write('#');
                snake[node.x - minX][node.y - minY] = "1";
            }
            for (int i = 0; i < maxX - minX; i++)
            {
                for (int j = 0; j < maxY - minY; j++)
                {
                    if (apples[j, i] == 1)
                    {
                        //Console.SetCursorPosition(minX + i, minY + j);
                        //Console.Write('+');
                        apps[j][i] = "1";
                    }

                }
            }
            string resSnake = "", resApple = "", resWall="";
            for (int i = 0; i < maxX - minX; i++)
            {
                for (int j = 0; j < maxY - minY; j++)
                {
                    resSnake += snake[j][i];
                    resApple += apps[j][i];
                    resWall+=walls[j][i]
                }
                resSnake += "\n";
                resApple += "\n";
                resWall += "\n";
            }
            //Console.WriteLine("================");
            Console.WriteLine(resSnake);
            Console.WriteLine(resApple);
            //Console.WriteLine("================");
            return $"snake:\n{resSnake}\napples:\n{resApple}\nwalls:\n{resWall}\n";
        }
        public void Start()
        {
            if (start == true)
                return;
            start = true;
            Task.Factory.StartNew(() =>
            {
                while (start)
                {
                    Work();
                    Thread.Sleep(300);
                }
            });
        }
        public void Stop() { start = false; }
        public string Work()
        {
            int newX = head.x, newY = head.y;
            switch (curDir)
            {
                case Direction.left: newX -= 1; break;
                case Direction.up: newY -= 1; break;
                case Direction.right: newX += 1; break;
                case Direction.down: newY += 1; break;
            }
            if (newX < minX) newX = maxX - Math.Abs(newX);
            if (newX >= maxX) newX = minX;

            if (newY < minY) newY = maxY - Math.Abs(newY);
            if (newY >= maxY) newY = minY;
            string reward = "0";
            if (apples[newX - minX, newY - minY] == 1)
            {
                apples[newX - minX, newY - minY] = 0;
                Node nn = new Node(tail.number + 1) { x = tail.x, y = tail.y };
                nn.parent = tail;
                tail.child = nn;
                tail = nn;
                reward = "1";
            }
            int tailX = tail.x, tailY = tail.y;
            for (Node node = tail; node.parent != null; node = node.parent)
            {
                node.x = node.parent.x;
                node.y = node.parent.y;
            }
            head.x = newX;
            head.y = newY;


            var ss= Draw(head);
            //Console.SetCursorPosition(tailX, tailY);
            //Console.Write(' ');
            string done = "False";
            for (Node node = head.child; node != null; node = node.child)
            {
                if (node.x == newX && node.y == newY)
                {
                    //Console.SetCursorPosition(head.x, head.y);
                    //Console.Write('X');
                    Stop();
                    done = "True";
                }
            }
            return $"{ss}reward:{reward}\ndone:{done}";
        }

        public void Move(ConsoleKeyInfo dir)
        {
            switch (dir.Key)
            {
                case ConsoleKey.Spacebar: Work(); break;
                case ConsoleKey.RightArrow: ChangeDirection(Direction.right); Work(); break;
                case ConsoleKey.UpArrow: ChangeDirection(Direction.up); Work(); break;
                case ConsoleKey.LeftArrow: ChangeDirection(Direction.left); Work(); break;
                case ConsoleKey.DownArrow: ChangeDirection(Direction.down); Work(); break;
                case ConsoleKey.S: Stop(); break;
                case ConsoleKey.W: Start(); break;
                case ConsoleKey.A: NewApple(); Work(); break;
            }
        }
    }

}