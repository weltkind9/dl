import re
import numpy as np
import websocket
import time

class EnvironmentSnake():
    def __init__(self, port, w, h):
        self.w=w
        self.h=h
        self.ws_connect(port)
        self.lastTable=None

    def ws_connect(self, port):
        self.ws=websocket.create_connection(f'ws://127.0.0.1:{port}')
   
    def step(self, action):
        minInd = np.argmin(action)
        if minInd == 0: self.ws.send('r')
        elif minInd == 1: self.ws.send('l')
        elif minInd == 2: self.ws.send('u')
        else: self.ws.send('d')
        time.sleep(0.02)

        msg1 = self.ws.recv()
        tbl1, appl1, wl1, rew, dn = self.getState(msg1)
        res = np.concatenate(np.reshape(self.lastTable, 1), np.reshape(tbl1, 1), np.reshape(appl2, 1), np.reshape(wl2, 1))
        self.lastTable = tbl1
        return res, rew, dn

    def reset(self):
        self.ws.send('reset')
        msg1=self.ws.recv()
        tbl1, appl1, wl1, rew, dn = self.getState(msg1)
        time.sleep(0.02)
        self.ws.send(' ')
        msg2=self.ws.recv()
        tbl2, appl2, wl2, rew, dn = self.getState(msg2)

        self.lastTable = tbl2
        res = np.concatenate(np.reshape(tbl1, 1), np.reshape(tbl2, 1), np.reshape(appl2, 1), np.reshape(wl2, 1))
        return res

    def toArr(self, splt):
        ar = np.zeros((self.w, self.h))
        i=0
        for row in splt:
            if len(row)==0: continue
            j=0
            for col in row:
                if col == '1':
                    ar[i][j]=1
                j+=1
            i+=1
        return ar 


    def getState(self, msg):
        wls = re.split('walls', msg)
        appls = re.split('apples', wls[0])
        tbls = re.split('tables', apps[0])
        rews = re.split('rewars', tbls[0])
        dns = re.split('done', rews[0])

        tbl=toArr(tbls[0])
        appl=toArr(appls[1])
        wl=toArr(wls[1])
        rew = int(rews[0].split())
        dn = bool(rews[1].split())
        return tbl, appl, wl, rew, dn