import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Bernoulli
from torch.autograd import Variable
from itertools import count
import matplotlib.pyplot as plt
import numpy as np
import pdb
import EnvironmentSnake

w=10
h=10
class PolicyNet(nn.Module):
    def __init__(self):
        super(PolicyNet, self).__init__()

        self.fc1 = nn.Linear(w*h*4, w*h*4*2)
        self.fc2 = nn.Linear(w*h*4*2, w*h*4*2)
        self.fc3 = nn.Linear(w*h*4*2, 4)  # Prob of Left

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.softmax(self.fc3(x))
        return x


def work():
    # Parameters
    num_episode = 500
    batch_size = 1
    learning_rate = 0.01
    gamma = 0.99
    env = EnvironmentSnake.EnvironmentSnake(1010, w, h)
    policy_net = PolicyNet()
    optimizer = torch.optim.RMSprop(policy_net.parameters(), lr=learning_rate)
    # Batch History
    state_pool = []
    action_pool = []
    reward_pool = []
    steps = 0
    for e in range(num_episode):
        state = env.reset()
        state = torch.from_numpy(state).float()
        state = Variable(state)
        while True:
            probs = policy_net(state)
            m = Bernoulli(probs)
            action = m.sample()
            action = action.data.numpy().astype(int)[0]
            next_state, reward, done = env.step(action)
            # To mark boundarys between episodes
            if done:
                reward = 0
            state_pool.append(state)
            action_pool.append(float(action))
            reward_pool.append(reward)
            state = next_state
            state = torch.from_numpy(state).float()
            state = Variable(state)
            steps += 1
            if done:
                break
        # Update policy
        if e > 0 and e % batch_size == 0:
            # Discount reward
            running_add = 0
            #print(f'reward_pool({len(reward_pool)}): {reward_pool}')
            for i in reversed(range(steps)):
                if reward_pool[i] == 0:
                    running_add = 0
                else:
                    running_add = running_add * gamma + reward_pool[i]
                    reward_pool[i] = running_add                    
                
            #print(f'reward_pool2({len(reward_pool)}): {reward_pool}')
            # Normalize reward
            #reward_mean = np.mean(reward_pool)
            #reward_std = np.std(reward_pool)
            #for i in range(steps):
            #    reward_pool[i] = (reward_pool[i] - reward_mean) / reward_std
            # Gradient Desent
            #print(f'reward_std: {reward_std}; reward_mean: {reward_mean}')
            #print(f'reward_pool3({len(reward_pool)}): {reward_pool}')
            optimizer.zero_grad()
            for i in range(steps):
                state = state_pool[i]
                action = Variable(torch.FloatTensor([action_pool[i]]))
                reward = reward_pool[i]
                probs = policy_net(state)
                m = Bernoulli(probs)
                a=m.log_prob(action)
                #print(f'a: {a}; action: {action}; probs:{probs}')
                loss = -a * reward  # Negtive score function x reward
                #print(f'loss: {loss}')
                loss.backward()
            optimizer.step()
            print(f'end episode {e} after {steps} steps, mean = {steps/batch_size}')
            state_pool = []
            action_pool = []
            reward_pool = []
            steps = 0
            print()




wsw=WebSocketWraper.WebSocketWraper()
while 1:
    s = input()
    wsw.send_message(s)
