﻿using SnakeLibs;
using SuperSocket.SocketBase;
using SuperSocket.WebSocket;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SnakeServer
{
    class Program
    {
       static void Main(string[] args)
        {
            WebSocketServer appServer = new WebSocketServer();
            SuperSocket.SocketBase.Config.RootConfig r = new SuperSocket.SocketBase.Config.RootConfig();
            SuperSocket.SocketBase.Config.ServerConfig s = new SuperSocket.SocketBase.Config.ServerConfig();
            SuperSocket.SocketEngine.SocketServerFactory f = new SuperSocket.SocketEngine.SocketServerFactory();
            s.Name = "SuperWebSocket";
            s.Ip = "Any";
            s.Port = 666;
            s.Mode = SocketMode.Tcp;
            s.LogAllSocketException = true;
            bool startSuccessfull = appServer.Setup(r, s, f);
            Console.WriteLine("START WebSOCKET SERVER -> setup : " + startSuccessfull);
            appServer.Start();

            Console.WriteLine("START WebSOCKET SERVER -> port : " + s.Port);
            appServer.NewSessionConnected += AppServer_NewSessionConnected;
            appServer.NewMessageReceived += new SessionHandler<WebSocketSession, string>(WebListener);
            appServer.SessionClosed += AppServer_SessionClosed;




            Console.CursorVisible = false;
            table = new Table() { curDir = Direction.left };
            for (; ; )
            {
                var dir = Console.ReadKey(true);
                table.Move(dir);
                Thread.Sleep(50);
            }
        }
        
        static List<WebSocketSession> clients = new List<WebSocketSession>();
        static Table table;
        static void WebListener(WebSocketSession session, string message)
        {
            switch (message)
            {
                case "r": table.Move(new ConsoleKeyInfo(' ', ConsoleKey.RightArrow,false,false, false));break;
                case "l": table.Move(new ConsoleKeyInfo(' ', ConsoleKey.LeftArrow, false, false, false)); break;
                case "u": table.Move(new ConsoleKeyInfo(' ', ConsoleKey.UpArrow, false, false, false)); break;
                case "d": table.Move(new ConsoleKeyInfo(' ', ConsoleKey.DownArrow, false, false, false)); break;
            }
        }
        static private void AppServer_SessionClosed(WebSocketSession session, CloseReason value)
        {
            clients.RemoveAll(x => x.SessionID == session.SessionID);
            Console.WriteLine($"{session.SessionID} disconnected");
        }

        static private void AppServer_NewSessionConnected(WebSocketSession session)
        {
            Console.WriteLine($"{session.SessionID} connected");
            clients.Add(session);
        }
    }
}