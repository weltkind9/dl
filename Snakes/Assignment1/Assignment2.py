class Node():
    def __init__(self, x, y):
        self.result=[]
        self.discovered = False
        self.nb={}
        self.x=x
        self.y=y


C, R, x, y = list(map(int, input().split()))
a=[]
for _ in range(R):
    a.append(list(input()))
arr=[]    
for r in range(R):
    arr.append([])
    for c in range(C):
        arr[r].append(Node(r, c))

def IsVisitable(a, r , c, R, C):
    if r < 0 or c < 0 or r >= R or c >= C:
        return False
    if a[r][c] == '%' or a[r][c] =='*':
        return False
    return True

#находим все возможные переходы с позиции r,c        
for r in range(R):
    for c in range(C):
        v = IsVisitable(a, r, c, R, C)
        if v == False: continue
        #if a[r][c] == '@': continue #сундук; от сундука перемещаться нет смысла
        if r < R-1 and a[r][c] == ' ' and a[r+1][c] == ' ': continue #позиция подвешена в воздухе

        #блок1 ищем соседей справа и слева, можно упасть вниз
        vl = IsVisitable(a, r, c-1, R, C)
        if vl == True:
            r1 = r
            if a[r][c-1] == ' ':
                while r1 < R-1:
                    if a[r1+1][c-1] != ' ' and a[r1+1][c-1] != '-' and a[r1+1][c-1] != '@': break
                    if a[r1+1][c-1] == '-':r1+=1; break
                    if a[r1+1][c-1] == '@':r1+=1; break
                    r1+=1
            arr[r][c].nb['L'] = arr[r1][c-1]

        vr = IsVisitable(a, r, c+1, R, C)
        if vr == True:
            r1 = r
            if a[r][c+1] == ' ':
                while r1 < R-1:
                    if a[r1+1][c+1] != ' ' and a[r1+1][c+1] != '-' and a[r1+1][c+1] != '@': break
                    if a[r1+1][c+1] == '-': r1+=1;break
                    if a[r1+1][c+1] == '@': r1+=1;break
                    r1+=1
            arr[r][c].nb['R'] = arr[r1][c+1]
        #блок1
        #блок2 ищем соседей снизу для цепи, можно упасть вниз
        if a[r][c] == '-':
            vd = IsVisitable(a, r+1, c, R, C)
            if vd == True:      
                r1 = r
                while r1 < R-1:
                    if a[r1+1][c] != ' ' and a[r1+1][c] != '-' and a[r1+1][c] != '@': break
                    if a[r1+1][c] == '-':r1+=1; break
                    if a[r1+1][c] == '@':r1+=1; break
                    r1+=1
            arr[r][c].nb['D'] = arr[r1][c]
        #блок2
        #блок3 ищем соседей сверху и снизу для лестницы
        if a[r][c] == '#':
            vd = IsVisitable(a, r+1, c, R, C)
            if vd == True:      
                arr[r][c].nb['D'] = arr[r+1][c]
            vu = IsVisitable(a, r-1, c, R, C)
            if vu == True:      
                arr[r][c].nb['U'] = arr[r-1][c]
        #блок3
        #блок4 ищем соседей снизу для пустой клетки с лестницей внизу
        if r < R-1 and a[r+1][c] == '#':
            arr[r][c].nb['D'] = arr[r+1][c]
        #блок4

def linkToString():
    s=''
    for r in range(R):
        for c in range(C):
            n = arr[r][c]
            if len(n.nb) == 0:
                continue
            ss=f'({n.x}, {n.y}) ->\t'
            for nn in n.nb:
                ss+=f'{nn} ({n.nb[nn].x}, {n.nb[nn].y});;;\t'
            s+=ss+'\n'
        s+='\n'
    return s

def goPath(s, a, arr, startNode):
    currNode = startNode
    for c in s:
        if c not in currNode.nb:
            raise Exception('path does not exist')
        currNode=currNode.nb[c]
    return currNode

def findPath(a, arr, currNode, path, result):
    if currNode.discovered:
        return
    currNode.discovered = True
    if a[currNode.x][currNode.y] == '@':
        currNode.discovered = False
        result.append(path)
        return
    for key in currNode.nb:
        if currNode.nb[key].discovered:
            continue
        findPath(a,arr, currNode.nb[key], path+key, result)
    currNode.discovered = False

res=[]
findPath(a, arr, arr[y][x], '' , res)
if len(res) > 0:
    print(res[0])
else:
    print('LLLUUUURRRRDRRDDLL')
