#1.8 ����� ��������
#
#����� ������� ����� ������ � ��� ����������. ��� ������ � ����� �� ���� ��������� ���������� 4747 �����. 
#������ ����������� ��������� ������, ���� � ������� �� �������� � ������� � 30% �������, 
#� ������� � � 40%, � ��������� � � 20%, � ������� � � 5%, ���������� 5% ���������� �� �������� 0�6? 
#� �������� ������ ������� ���������� �����.

#https://stepik.org/lesson/48665/step/6?unit=26437


import itertools
from decimal import *

#############################################################################
#������ 1
#r-length tuples, in sorted order, with repeated elements
#combinations_with_replacement('ABCD', 2) AA AB AC AD BB BC BD CC CD DD
perm = list(itertools.combinations_with_replacement((6,7,8,9,10), 5))

exact47 = list(itertools.filterfalse(lambda x: not(
   sum(x) == 47
   ), perm))
comb = []
for lst in exact47:
    r = set(itertools.permutations(lst, 5))
    comb.extend(r)
#############################################################################

#############################################################################
#������ 2
#cartesian product, equivalent to a nested for-loop
#product('ABCD', repeat=2) -> AA AB AC AD BA BB BC BD CA CB CC CD DA DB DC DD
perm = list(itertools.product((6,7,8,9,10),  repeat=5))
comb = list(itertools.filterfalse(lambda x: not(
   sum(x) == 47
   ), perm))
#############################################################################


result = []

ver = {10:Decimal(3)/Decimal(10),
       9: Decimal(4)/Decimal(10),
       8: Decimal(2)/Decimal(10),
       7: Decimal(5)/Decimal(100),
       6: Decimal(5)/Decimal(100)}

for cc in comb:
    r = Decimal(1)
    for c in cc:
        r *= ver[c]
    result.append(r)
print(sum(result))
input()