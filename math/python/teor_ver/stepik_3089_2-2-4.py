﻿#2.2 Математическое ожидание
#
#Случайная величина ξ принимает все натуральные значения от 1 до 1000.
#Вероятность значения k пропорциональна k. Найдите медиану и математическое ожидание ξ.
#В ответе укажите найденные числа, разделенные пробелами.

#https://stepik.org/lesson/48668/step/5?unit=26438

xx = list(range(1,1001))
sm = sum(xx)
mo = 0
med_1 = 0
med_2 = 0
avr_ver = 0.5
v1 = 0
for x in xx:
    mo += x * x / sm
    if v1 + x / sm < avr_ver:
        v1 += x / sm
    else:
        if med_1 == 0:
            med_1 = x
v2 = 0
for i in range(len(xx)-1, -1, -1):
    x = xx[i]
    if v2 + x / sm < avr_ver:
        v2 += x / sm
    else:
        if med_2 == 0:
            med_2 = x

print(avr_ver)
print(sm)
print()
print()
print(mo)
print(med_1)
print(med_2)