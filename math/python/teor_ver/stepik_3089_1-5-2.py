#1.5 �������� �����������
#
#������ �������� AA, BB, CC � DD ���������� � ������� � ��������� �������. �������

#�������� ����������� ����, ��� AA ������, ���� BB ���������;
#�������� ����������� ����, ��� AA ������, ���� AA �� ���������;
#�������� ����������� ����, ��� AA ������, ���� BB �� ���������;
#�������� ����������� ����, ��� AA ������, ���� BB ����� � ������� ����� AA;
#�������� ����������� ����, ��� AA ����� � ������� ������ BB, ���� ��������, ��� AA ������ CC.
#� �������� ������ ��������� ��������� ����������� � ���� ������������ ������, ����������� ���������.

#https://stepik.org/lesson/48662/step/4?unit=26433


import itertools
#r-length tuples, all possible orderings, no repeated elements
#permutations('ABCD', 2) -> AB AC AD BA BC BD CA CB CD DA DB DC
perm = list(itertools.permutations('ABCD', 4))

#1
a_first_and_b_last = list(itertools.filterfalse(lambda x: not(
   x[-1] == 'B' and x[0]=='A'
   ), perm))
b_last = list(itertools.filterfalse(lambda x: not(
   x[-1] == 'B'
   ), perm))


#2
a_not_last = list(itertools.filterfalse(lambda x: not(
    x[-1]!='A'
    ), perm))
a_first_and_a_not_last = list(itertools.filterfalse(lambda x: not(
    x[0]=='A' and x[-1]!='A'
    ), perm))

#3

b_not_last = list(itertools.filterfalse(lambda x: not(
    x[-1]!='B'
    ), perm))
a_first_b_not_last = list(itertools.filterfalse(lambda x: not(
    x[0]=='A' and x[-1]!='B'
    ), perm))
#4
b_after_a = list(itertools.filterfalse(lambda x: not(
    x.index('A') < x.index('B')
    ), perm))
a_first_b_after_a = list(itertools.filterfalse(lambda x: not(
    x[0]=='A' and (x.index('A') < x.index('B'))
    ), perm))

#5
a_before_c = list(itertools.filterfalse(lambda x: not(
    x.index('A') < x.index('C')
    ), perm))

a_before_b_and_a_before_c = list(itertools.filterfalse(lambda x: not(
    (x.index('A') < x.index('C')) and (x.index('A') < x.index('B'))
    ), perm))



input()