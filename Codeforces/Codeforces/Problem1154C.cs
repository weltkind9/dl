﻿using System;
using System.Linq;
partial class Program
{
    //static void Main(string[] args)
    //{
    //    Problem1154C();
    //    Console.Read();
    //}

    static void Problem1154C()
    {
        string s = Console.ReadLine();
        var arr = s.Split(' ').Select(x => Convert.ToInt32(x)).ToArray();
        Console.WriteLine(Problem1154C(arr[0], arr[1], arr[2]));
    }
    static int Problem1154C(int a, int b, int c)
    {
        int[] result = new int[7];
        int commonWeeks = new[] { a / 3, b / 2, c / 2 }.Min() - 1;

        for (int i = 0; i < 7; i++)
        {
            result[i] = Problem1154C(i, a - 3 * commonWeeks, b - 2 * commonWeeks, c - 2 * commonWeeks);
        }
        return commonWeeks * 7 + result.Max();
    }
    static int Problem1154C(int i, int a, int b, int c)
    {
        int count = 0;
        for (int j = i; ; j++)
        {
            var r = Problem1154C(j, ref a, ref b, ref c, ref count);
            if (r == true)
                return count;
        }

        throw new Exception();
    }

    static bool Problem1154C(int i, ref int a, ref int b, ref int c, ref int count)
    {
        switch (i % 7 + 1)
        {
            case 1:
            case 4:
            case 7:
                if (a == 0)
                    return true;
                else
                {
                    a--;
                    count++;
                    return false;
                }
            case 2:
            case 6:
                if (b == 0)
                    return true;
                else
                {
                    b--;
                    count++;
                    return false;
                }
            case 3:
            case 5:
                if (c == 0)
                    return true;
                else
                {
                    c--;
                    count++;
                    return false;
                }
        }
        throw new Exception();
    }
}