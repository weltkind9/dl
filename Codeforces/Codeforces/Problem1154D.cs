﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

partial class Program
{
    static int iter = 0;
    static void Main(string[] args)
    {
        iter = 0;
        int n = 20000, k = 1;
        int[] arr = new int[n];
        for (int i = 0; i < n; i++)
            arr[i] = n-i ;
        DateTime dt = DateTime.UtcNow;
        Problem1154D(n, k, arr);
        var ms = DateTime.UtcNow.Subtract(dt).TotalMilliseconds;
        Console.WriteLine(ms);
        //Console.Read();
    }
    class player
    {
        public player()
        { }
        public int index;
        public int skill;
        public int index_original;
        public override string ToString()
        {
            return $"{index} {skill}";
        }
    }
    static void Problem1154D()
    {
        string s = Console.ReadLine();
        var arr1 = s.Split(' ').Select(x => Convert.ToInt32(x)).ToArray();
        int n = arr1[0], k = arr1[1];
        s = Console.ReadLine();
        var arr2 = s.Split(' ').Select(x => Convert.ToInt32(x)).ToArray();
        int[] result = Problem1154D(n, k, arr2);
        Console.WriteLine(string.Join("", result));
    }
    static int[] Problem1154D(int n, int k, int[] arr)
    {
        List<player> plrs = new List<player>(n);
        for (int ii = 0; ii < n; ii++)
        {
            plrs.Add(new player() { index = ii, skill = arr[ii], index_original = ii });
        }
        int[] result = new int[n];
        int team = 1;
        for (; ; )
        {
            if (plrs.Count == 0)
                break;
            Problem1154D_Iter(plrs, k, result, team);
            team++;
        }
        return result;
    }
    static void Problem1154D_Iter(List<player> plrs, int k, int[] result, int team)
    {
        DateTime dt = DateTime.UtcNow;
        int n = plrs.Count;
        for (int ii = 0; ii < n; ii++)
        {
            plrs[ii].index = ii;
        }
        var plrs2 = plrs.OrderBy(x => x.skill).ToList();
        var currPl = plrs2[n - 1];
        var removeCount = Problem1154D_AddToRsultAndRemove(n, k, currPl, plrs, plrs2, team % 2 == 0 ? 2 : 1, result);
        var ms = DateTime.UtcNow.Subtract(dt).TotalMilliseconds;
        Console.WriteLine($"{iter++} {plrs.Count} {ms}");
    }
    static int Problem1154D_AddToRsultAndRemove(int n, int k, player currPl, List<player> plrs, List<player> plrs2, int team, int[] result)
    {
        List<int> toRemove = new List<int>(k * 2 + 1), toRemove2 = new List<int>(k * 2 + 1);
        var start = currPl.index + k;
        if (start > plrs2.Count - 1)
            start = plrs2.Count - 1;
        for (int j = start; j >= currPl.index - k && j >= 0; j--)
        {
            result[plrs[j].index_original] = team;
            toRemove2.Add(plrs[j].skill - 1);
            toRemove.Add(j);
        }
        toRemove = toRemove.OrderByDescending(x => x).ToList();
        toRemove2 = toRemove2.OrderByDescending(x => x).ToList();
        foreach (var i in toRemove)
            plrs.RemoveAt(i);
        foreach (var i in toRemove2)
            plrs2.RemoveAt(i);
        if (toRemove.Count != toRemove2.Count)
            throw new Exception();
        foreach (var pl in plrs)
        {
            foreach (var r in toRemove2)
                if (pl.skill > r)
                    pl.skill--;
        }
        return toRemove.Count;
    }
}