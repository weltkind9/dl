def splt(n, k, s):
    if k == 0:
        if len(s) == 0:
            return False
        else:
            return True

    for i in range(1, n//2+1):
        ss = s[:i] + s[i:-i] + s[:i][::-1]
        if ss == s:
            return splt(n-2*i, k - i, s[i:-i])
    return False
    

    
t = int(input())
while t > 0:
    n, k = list(map(int, input().split()))    
    s = input()

    r = splt(n, k, s)
    #if r: print("YES")
    #else: print("NO")

    t -= 1