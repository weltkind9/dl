def gcd(a, b):
    if a == 0: return b
    if b == 0: return a
    if a > b: return gcd(a % b, b)
    return gcd(b % a, a)

res = set()    
gcdd = {}
d ={1:101}
for i in range(2, 10000):
    a = 100 + i*i
    d[i] = a
    gcdd[i] = gcd(d[i-1], d[i])
    res.add(gcdd[i])
print(res)

