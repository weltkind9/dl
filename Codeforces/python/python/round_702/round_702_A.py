t = int(input())
while t > 0:
    n, k = list(map(int, input().split()))    
    if k % 2 == 0:
        res = [i for i in range(k//2, n+1)]
    else:
        res = [i for i in range(k//2+1, n+1)]

    if k in res:
        res.remove(k)
    print(len(res))
    print(' '.join(map(str, res)))
    t -= 1


def search(n, k, res, cur, arr):
    if cur == n +1:
        if sum(arr) == k:
            res.append(arr[:])
    else:
        arr.append(cur)
        search(n, k, res, cur+1, arr)
        arr.pop()
        search(n, k,  res, cur+1, arr)

res=[]
#search(10, 10, res, 1, [])
