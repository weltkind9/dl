import math
import time


class Tree:
    def __init__(self, arr):
        self.arr = arr
        self.n = len(arr)
        self.p2 = math.ceil(math.log2(self.n))
        self.nn = 2**self.p2
        self.tree = [-1 for i in range(self.nn)]
        self.tree.extend(arr)
        self.tree.extend([None] * (2 * self.nn - len(self.tree)))

    def __getitem__(self, i):
        #if i >= len(self.tree):
        #    if i % 2 == 0: return self.tree[-2]
        #    else: return self.tree[-1]
        #else:
        #    return self.tree[i]
        return self.tree[i]

    def __setitem__(self, i, value):
        #if i >= len(self.tree):
        #    if i % 2 == 0: self.tree[-2] = value
        #    else: self.tree[-1] = value
        #else:
        #    self.tree[i] = value
        self.tree[i] = value


    def mult(self, i):
        a = self.tree[2*i]
        b = self.tree[2*i+1]
        if a is None and b is None:
            self.tree[i] = None
        elif a is None:
            self.tree[i] = b
        elif b is None:
            self.tree[i] = a
        else:
            self.tree[i] = gcd(a, b)



def gcd(a, b):
    if a == 0: return b;
    if b == 0: return a
    if a > b: return gcd(a % b, b)
    else: return gcd(b % a, a)


n, q = 0, 2*10**5#list(map(int, input().split()))
arr = [1, 2, 3, 4]#*50000#list(map(int, input().split()))
tt = time.time()
tree = Tree(arr)
for i in range(tree.nn - 1, 0, -1):
    tree.mult(i)



for _ in range(q):
    i, value = 1, 2#list(map(int, input().split()))
    i -= 1
    i += tree.nn
    tree[i] *= value
    i = i // 2
    while i > 0:
        tree.mult(i)
        i = i // 2
   # print(tree[1] % (10**9+7))

print(time.time()-tt)

