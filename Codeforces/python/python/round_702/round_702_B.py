def isCorrect(_h, _m, h, m):
    shh, smm = str(_h), str(_m)
    if _h < 10:
        shh = '0' + shh
    if _m < 10:
        smm = '0' + smm

    s = f'{shh}:{smm}'
    if '3' in s or '4' in s or '6' in s or '7' in s or '9' in s:
        return False
    if _h >= h or _h < 0:
        return False
    if _m>=m or _m<0:
        return False
    sRevH = list(smm[::-1])
    if sRevH[0] == '2': sRevH[0] = '5'
    elif sRevH[0] == '5': sRevH[0] = '2'
    if sRevH[1] == '2': sRevH[1] = '5'
    elif sRevH[1] == '5': sRevH[1] = '2'
    revH = int(''.join(sRevH))
    if revH >= h or revH < 0:
        return False

    sRevM = list(shh[::-1])
    if sRevM[0] == '2': sRevM[0] = '5'
    elif sRevM[0] == '5': sRevM[0] = '2'
    if sRevM[1] == '2': sRevM[1] = '5'
    elif sRevM[1] == '5': sRevM[1] = '2'
    revM = int(''.join(sRevM))
    if revM >= m or revM < 0:
        return False
    return s

def nextS(s, _h, _m, h, m):
    newM = _m
    newH = _h
    while 1:
        res = isCorrect(newH, newM, h, m)
        if res:
            return res

        newM += 1
        if newM >= m:
            newM = 0
            newH += 1
            if newH >= h:
                newH = 0


t = int(input())
while t > 0:
    h, m = list(map(int, input().split()))    
    s = input()
    _h, _m = list(map(int, s.split(':'))) 

    print(nextS(s, _h,_m, h, m))
    t -= 1