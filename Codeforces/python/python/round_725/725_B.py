def main(input, print):
    t = int(input())
    for _ in range(t):
        n = int(input())
        arr = list(map(int, input().split()))
        sm = sum(arr)
        if sm % n != 0:
            print(-1)
            continue
        m = sm // n
        d = [a - m for a in arr if a - m > 0]
        print(len(d))


#main(input, print)
with open('E:/coding/codeforces/python/python/round_725/725_Bt2.txt') as read:
    with open('E:/coding/codeforces/python/python/round_725/725_B_ans.txt', 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)
