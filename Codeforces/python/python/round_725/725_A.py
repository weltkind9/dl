def main(input, print):
    t = int(input())
    for _ in range(t):
        n = int(input())
        arr = list(map(int, input().split()))
        lmn = arr.index(min(arr))
        lmx = arr.index(max(arr))
        rmx = n - lmx
        rmn = n - lmn
        r1 = min(lmn+1 + rmx,  rmn + lmx+1)
        r = min(max(lmn+1, lmx+1), max(rmn, rmx), r1)
        print(r)


#main(input, print)
with open('E:/coding/codeforces/python/python/round_725/725_At2.txt') as read:
    with open('E:/coding/codeforces/python/python/round_725/725_A_ans.txt', 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)
