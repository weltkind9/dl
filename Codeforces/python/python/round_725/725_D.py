def sieve(n):
    ans = [0] * (n + 1)
    for i in range(2, n+1):
        if ans[i]:
            continue
        for j in range(i, n+1, i):
            ans[j] = 1
    return ans

sv = sieve(20)
print(sv)
def factors(n):
    ans = []
    i = 2
    nn = n
    while i * i <= n:
        while nn % i == 0:
            ans.append(i)
            nn //= i
        i += 1
    if nn > 1:
        ans.append(nn)
    return ans
r = factors(12)
print(r)

r1 = factors(36)
print(r1)

r2 = factors(48)
print(r2)
def main(input, print):
    while 1:
        n, m = map(int, input().strip().split())
        if n == 0 and m == 0:
            break
        print(f'{n}, {m}\n')


#main(input, print)
with open('E:/coding/codeforces/python/python/round_725/725_Dt.txt') as read:
    with open('E:/coding/codeforces/python/python/round_725/725_D_ans.txt', 'w') as write:
        #main(read.readline, write.write)
        main(read.readline, print)
