def f(res, cur, arr, n):
    for a in arr:
        if len(cur) == 0 and a == '0':
            continue
        cur.append(a)
        r = int(''.join(cur))
        if r > n:
            cur.pop()
            return
        res.add(r)
        f(res, cur, arr, n)
        cur.pop()

n, x, y = map(int, input().split())
d = [str(x), str(y)]
d.sort()

res = set()
cur = []
f(res, cur, d, n)
print(len(res))