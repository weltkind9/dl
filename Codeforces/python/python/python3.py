def f(arr, n):
    return arr[n-1] + arr[n-2]

arr = [0, 1]
i = 2
while 1:
    arr.append(f(arr, i))
    i+=1
    if arr[-1] > 30000:
        break
a = int(input())
if a in arr:
    print('yes')
else:
    print('no')

