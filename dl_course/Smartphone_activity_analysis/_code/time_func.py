
import pandas as pd
import numpy

def convert(df, time_period):
    st = int(0.2*len(df))
    end = -int(0.2*len(df))
    df2 = df.iloc[st:end]  

    x = df2['gFx'].values
    y = df2['gFy'].values
    z = df2['gFz'].values
    t = df2['TgF'].values
    time = df2['time'].values
    time_diff = numpy.diff(time)
    
    x_conv = []; y_conv = []; z_conv = []; t_conv = []
    cur_time = 0
    cur_acc_x = []; cur_acc_y = []; cur_acc_z = []; cur_acc_t = []
    step = 0
    for td in time_diff:
        if cur_time + td > time_period:
            x_conv.append(numpy.mean(cur_acc_x))
            y_conv.append(numpy.mean(cur_acc_y))
            z_conv.append(numpy.mean(cur_acc_z))
            t_conv.append(numpy.mean(cur_acc_t))
            #print(len(cur_acc_x))
            cur_time = 0
            cur_acc_x = []; cur_acc_y = []; cur_acc_z = []; cur_acc_t = []
            
        cur_acc_x.append(x[step])
        cur_acc_y.append(y[step])
        cur_acc_z.append(z[step])
        cur_acc_t.append(t[step])
        cur_time += td
        step += 1
    return x_conv, y_conv, z_conv, t_conv
def repeat_track(track, ln):
    tr2 = []
    while len(tr2) < ln:
        tr2.extend(track)
    return tr2

def prepare_track(df, name):
    x = df['gFx'].values
    y = df['gFy'].values
    z = df['gFz'].values
    t = df['TgF'].values
    time = df['time'].values
    t_start = time[0]
    t_end = time[-1]
    l = 2500
    while l < 4001:
        time_period = (t_end-t_start) / l
        x_conv, y_conv, z_conv, t_conv = convert(df, time_period)
        if len(x_conv) > 2499:
            break
        l += 500
    if len(x_conv) < 2500:
        x_conv = repeat_track(x_conv, 2500)
        y_conv = repeat_track(y_conv, 2500)
        z_conv = repeat_track(z_conv, 2500)
        t_conv = repeat_track(t_conv, 2500)
        #print(len(x_conv), 'error', name)
        #return None, None, None,None
    x_conv = x_conv[:2500]
    y_conv = y_conv[:2500]
    z_conv = z_conv[:2500]
    t_conv = t_conv[:2500]
    if len(x_conv) != 2500 or len(y_conv) != 2500 or len(z_conv) != 2500 or len(t_conv) != 2500:
        print(len(x_conv), 'error', name)    
    if False:
        fig, ((ax1, ax2, ax3, ax4, ax5)) = plt.subplots(nrows=5, ncols=1)

        #ax1.plot(time, x, c='r');
        #ax1.plot(time, y, c='g');
        #ax1.plot(time, z, c='b');
        #ax1.plot(time, t, c='black');
        st=0; en=500
        ax1.plot(x_conv[st:en], c='r');
        ax1.plot(y_conv[st:en], c='g');
        ax1.plot(z_conv[st:en], c='b');
        ax1.plot(t_conv[st:en], c='black');
        st=500; en=1000
        
        ax2.plot(x_conv[st:en], c='r');
        ax2.plot(y_conv[st:en], c='g');
        ax2.plot(z_conv[st:en], c='b');
        ax2.plot(t_conv[st:en], c='black');
        st=1000; en=1500
        
        ax3.plot(x_conv[st:en], c='r');
        ax3.plot(y_conv[st:en], c='g');
        ax3.plot(z_conv[st:en], c='b');
        ax3.plot(t_conv[st:en], c='black');
        st=1500; en=2000
        
        ax4.plot(x_conv[st:en], c='r');
        ax4.plot(y_conv[st:en], c='g');
        ax4.plot(z_conv[st:en], c='b');
        ax4.plot(t_conv[st:en], c='black');
        st=2000; en=2501
        
        ax5.plot(x_conv[st:en], c='r');
        ax5.plot(y_conv[st:en], c='g');
        ax5.plot(z_conv[st:en], c='b');
        ax5.plot(t_conv[st:en], c='black');

        plt.show()
    r = []
    r.extend(x_conv)
    r.extend(y_conv)
    r.extend(z_conv)
    r.extend(t_conv)
    return r