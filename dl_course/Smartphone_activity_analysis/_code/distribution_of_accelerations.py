
import pandas as pd
import seaborn as sns
from matplotlib import pylab as plt

dd = [(i/100, (i+10)/100) for i in range(-840, 850, 10)]
ln = len(dd)
print(ln)
#dd.extend([(i, i+1) for i in range(-3, 20)])
#dd.extend([(i/10, (i+1)/10) for i in range(-30, 1, 1)])
#dd.extend([(i/10, (i+1)/10) for i in range(1, 30, 1)])
dd = sorted(dd)
def distr_one_axis(df, column):
    try:
        x = []
        count = df[column].count()
        c_test = 0
        for i in range(len(dd)):
            c = 0
            if i == 0:
                c = df[df[column] <= dd[0][0]][column].count()
                c_test += c
                c = c / count #* 100
                #if c != 0:
                #    print(f"-inf:{dd[i][0]} {c}%")
            elif i == len(dd)-1:
                c = df[df[column] >= dd[i][1]][column].count()
                c_test += c
                c = c / count #* 100
                #if c != 0:
                #    print(f'{dd[i][1]}:inf {c}%')
            else:
                c = df[(df[column] >= dd[i][0]) & (df[column] < dd[i][1])][column].count()
                c_test += c
                c = c / count #* 100
                #if c != 0:
                #    print(f"{dd[i]} {c}%")
            x.append(c)

        #print(c_test, count)
        assert abs(c_test - count)<1
        return x
    except Exception as ex:
        return None

def process_data(df, name = ""):
    #������ �� ��������, ������������� ��� ������ ��������� �� �����������
    print(name)
    x = distr_one_axis(df, 'gFx')
    y = distr_one_axis(df, 'gFy')
    z = distr_one_axis(df, 'gFz')
    t = distr_one_axis(df, 'TgF')
    #fig, ((ax1)) = plt.subplots(nrows=1, ncols=1)

    #ax1.plot(x, c='r')
    #ax1.plot(y, c='g')
    #ax1.plot(z, c='b')
    #ax1.plot(t, c='black')
    
    #plt.show()    
    return x, y, z, t

