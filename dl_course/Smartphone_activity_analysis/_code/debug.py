import os
import pandas as pd
import numpy as np
from matplotlib import pylab as plt
import load_data_helper
import distribution_of_accelerations
import actions
import time_func

work_wir = 'D:/coding/bitbucket_repo_dl/dl_course/Smartphone_activity_analysis/_v3/'

path_train = r'D:\coding\bitbucket_repo_dl\dl_course\Smartphone_activity_analysis\Public-data' 
Data_train, Target_train, files_name_train = load_data_helper.load_data(path_train, list(range(0,9)))
print(len(Data_train), len(Target_train), len(files_name_train))

path_test = r'D:\coding\bitbucket_repo_dl\dl_course\Smartphone_activity_analysis\kaggle_data_open'
Data_test, files_name_test = load_data_helper.load_data_test(path_test, 183)
print(len(Data_test), len(files_name_test))


max_value = 0
result_train = []
result_test = []
answer = []
i = -1
for df, target in zip(Data_train,Target_train):
    i += 1
    act = actions.get_action(target[1])
    answer.append(act)
    #r = time_func.prepare_track(df, name=f'{files_name_train[i]}')
    #r_abs = map(abs, r)
    #mx = max(r_abs)
    #if mx > max_value:
    #    max_value = mx
    x, y, z, t = distribution_of_accelerations.process_data(df, name=f'{files_name_train[i]}')
    x2 = np.array(x); y2 = np.array(y); z2 = np.array(z)
    result_train.append(np.concatenate((np.concatenate((x2,y2)), z2)))
    result_train.append(np.concatenate((np.concatenate((y2,x2)), z2)))
    result_train.append(np.concatenate((np.concatenate((z2,y2)), x2)))
    result_train.append(np.concatenate((np.concatenate((y2,z2)), x2)))
    result_train.append(np.concatenate((np.concatenate((x2,z2)), y2)))
    result_train.append(np.concatenate((np.concatenate((z2,x2)), y2)))
#    if i > 10:
#        break

i = -1
for df in Data_test:
    i += 1
    #r = time_func.prepare_track(df, name=f'{files_name_test[i]}')
    #r_abs = map(abs, r)
    #mx = max(r_abs)
    #if mx > max_value:
    #    max_value = mx
    x, y, z, t = distribution_of_accelerations.process_data(df, name=f'{files_name_test[i]}')
    x2 = np.array(x); y2 = np.array(y); z2 = np.array(z)
    result_train.append(np.concatenate((np.concatenate((x2,y2)), z2)))
    
#print(max_value)
#for r in result_train:
#    r /= max_value
    
#for r in result_test:
#    r /= max_value