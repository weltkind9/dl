
answers = {'сидение':0, 'стояние' : 0, 'покой':0, 'stay':0, 'stand':0,
           'ходьба' : 1, 'хотьба':1, 'шаг':1,
           'бег' : 2, 'running':2,
           'велосипед' : 3, 'вел_':3,
           'лестница' : 4, 'stairs':4, 'подъем':4,
           'автомобиль' : 5, 'автомобиле' : 5,
           'метро' : 6,
           'автобус' : 7,
           'самокат' : 8,
           'трамвай':9,
          'электричка':10}

def get_type(name):
    try:
        splt = name.split("_")
        if len(splt) >= 3:
            return splt[-2]
        else:
            return splt[-1]
    except Exception as ex:
        print(name)
    raise Exception('name')
    
def get_action(name):
    for act in answers:
        if name.lower().find(act) != -1:
            return answers[act]      
    raise Exception('name')
