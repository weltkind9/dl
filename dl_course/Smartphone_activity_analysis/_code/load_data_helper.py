import os
import pandas as pd
import actions
import datetime

def load_frame(my_file, delim):
    df = pd.read_csv(my_file, delimiter=delim)
    
    if df.shape[1] != 5:
        return None
    
    st = int(0.2*len(df))
    end = -int(0.2*len(df))#df.time.count()
    df = df.iloc[st:end]   
    if 'TgF' not in df.columns:
        df.columns = ['time', 'gFx', 'gFy', 'gFz', 'TgF']
    df = df.applymap(lambda x: str(x).replace(',','.'))
    if ':' in str(df['time'].values[0:1]):
        try:
            df['time_str'] = df['time'].astype(str)
            df['time'] = df['time_str'].apply(lambda x: datetime.datetime.strptime(x, '%Y-%m-%d %H:%M:%S.%f').timestamp()).astype(float)
        except Exception as ex:
            try:
                df['time'] = df['time_str'].apply(lambda x: datetime.datetime.strptime(x, '%H:%M:%S:%f').replace(year = 2020).timestamp()).astype(float)
            except Exception as ex:
                print(ex)
                raise(ex)
    else:
        df['time'] = df['time'].astype(float)
    df['gFx'] = df['gFx'].astype(float)
    df['gFy'] = df['gFy'].astype(float)
    df['gFz'] = df['gFz'].astype(float)
    df['TgF'] = df['TgF'].astype(float)
    #df['TgF'] = df['TgF'].apply(lambda x: x-1)
    return df

def load_data(path, posibl_types):
    Data = []
    Target = []
    files_name = []
    for dir_entry in sorted(os.listdir(path)):
        dir_entry_path = os.path.join(path, dir_entry)
        if os.path.isfile(dir_entry_path):            
            try:
                file_name = dir_entry_path.split("\\")[-1]
                act = actions.get_action(file_name)
                if not act in posibl_types:
                    continue
                with open(dir_entry_path, 'r') as my_file:
                    df = load_frame(my_file, ',')
                if df is None:
                    with open(dir_entry_path, 'r') as my_file:
                        df = load_frame(my_file, ';')
                assert not df is None
                Data.append(df)
                Target.append((act, file_name))
                files_name.append(dir_entry_path)
            except UnicodeDecodeError:
                print(file_name)
                pass
            except Exception as ex:
                print(file_name)
                print(ex)
        #break
    return Data, Target, files_name

def load_data_test(path, count):
    Data = [0]*count
    files_name = [0]*count
    for dir_entry in sorted(os.listdir(path)):
        dir_entry_path = os.path.join(path, dir_entry)
        if os.path.isfile(dir_entry_path):
            try:

                f_name = dir_entry_path.split("\\")[-1]
                ff = f_name.replace('track_','').replace('.csv','')
                num = int(ff)              
                with open(dir_entry_path, 'r') as my_file:
                    df = load_frame(my_file, ',')
                if df is None:
                    with open(dir_entry_path, 'r') as my_file:
                        df = load_frame(my_file, ';')
                assert not df is None
                     
                #print(num)
                files_name[num] = f_name
                Data[num] = df
            except UnicodeDecodeError:
                print(num, f_name)
                pass
            except Exception as ex:
                print(num, f_name)
                print(ex)
        #break
    return Data, files_name
        