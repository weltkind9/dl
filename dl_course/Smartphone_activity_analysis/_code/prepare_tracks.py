

import os
import pandas as pd
import load_data_helper
import numpy
import distribution_of_accelerations
from matplotlib import pylab as plt
from scipy.signal import savgol_filter

path_test = r'D:\coding\bitbucket_repo_dl\dl_course\Smartphone_activity_analysis\kaggle_data_open'
Data_test, files_name_test = load_data_helper.load_data_test(path_test, 183)
print(len(Data_test), len(files_name_test))

#path_train = r'D:\coding\bitbucket_repo_dl\dl_course\Smartphone_activity_analysis\Public-data' 
#Data_train, Target_train, files_name_train = load_data_helper.load_data(path_train, list(range(2,3)))
#print(len(Data_train), len(Target_train), len(files_name_train))


def convert(df, time_period):
    st = int(0.1*len(df))
    end = -int(0.1*len(df))
    df2 = df.iloc[st:end]  

    x = df2['gFx'].values
    y = df2['gFy'].values
    z = df2['gFz'].values
    t = df2['TgF'].values
    time = df2['time'].values
    time_diff = numpy.diff(time)
    
    x_conv = []; y_conv = []; z_conv = []; t_conv = []
    cur_time = 0
    cur_acc_x = []; cur_acc_y = []; cur_acc_z = []; cur_acc_t = []
    step = 0
    for td in time_diff:
        if cur_time + td > time_period:
            x_conv.append(numpy.mean(cur_acc_x))
            y_conv.append(numpy.mean(cur_acc_y))
            z_conv.append(numpy.mean(cur_acc_z))
            t_conv.append(numpy.mean(cur_acc_t))
            #print(len(cur_acc_x))
            cur_time = 0
            cur_acc_x = []; cur_acc_y = []; cur_acc_z = []; cur_acc_t = []
            
        cur_acc_x.append(x[step])
        cur_acc_y.append(y[step])
        cur_acc_z.append(z[step])
        cur_acc_t.append(t[step])
        cur_time += td
        step += 1
    return x_conv[:1500], y_conv[:1500], z_conv[:1500], t_conv[:1500]
def repeat_track(track, ln):
    tr2 = []
    while len(tr2) < ln:
        tr2.extend(track)
    return tr2

ln = []
def show_plot_time2(df, name):
    x = df['gFx'].values
    y = df['gFy'].values
    z = df['gFz'].values
    t = df['TgF'].values
    time = df['time'].values
    t_start = time[0]
    t_end = time[-1]
    l = 1000
    while l < 4001:
        time_period = (t_end-t_start) / l
        x_conv, y_conv, z_conv, t_conv = convert(df, time_period)
        if len(x_conv) > 999:
            break
        l += 500
    if len(x_conv) < 1000:
        x_conv2 = repeat_track(x_conv, 1000)
        y_conv2 = repeat_track(y_conv, 1000)
        z_conv2 = repeat_track(z_conv, 1000)
        t_conv2 = repeat_track(t_conv, 1000)
        print(len(x_conv), 'error', name)
        #return None, None, None,None
    #if False:
        fig, ((ax1, ax2, ax3)) = plt.subplots(nrows=1, ncols=3)

        ax1.plot(time, x, c='r');
        ax1.plot(time, y, c='g');
        ax1.plot(time, z, c='b');
        ax1.plot(time, t, c='black');

        ax2.plot(x_conv, c='r');
        ax2.plot(y_conv, c='g');
        ax2.plot(z_conv, c='b');
        ax2.plot(t_conv, c='black');

        ax3.plot(x_conv2, c='r');
        ax3.plot(y_conv2, c='g');
        ax3.plot(z_conv2, c='b');
        ax3.plot(t_conv2, c='black');

        plt.show()
    

rrr = []
i = -1
for df in Data_test:
    i += 1
    show_plot_time2(df, name=f'{files_name_test[i]}')
    #print('*')    
    #if i > 10:
    #    break
print('end')


def show_plot_time(df, name):
    fig, ((ax1)) = plt.subplots(nrows=1, ncols=1)    
    st = 0
    end = st+500
    x = df.iloc[st:end]['gFx'].values
    y = df.iloc[st:end]['gFy'].values
    z = df.iloc[st:end]['gFz'].values
    time = df.iloc[st:end]['time'].values
    dff = numpy.diff(time)
    #print(time)
    ax1.plot(time, x, c='g');
    tt = savgol_filter(x, 33, 2)
    ax1.plot(time, tt, c='r');
    
    plt.show()

def show_plot_time2(df, name):
    fig, ((ax1, ax2)) = plt.subplots(nrows=1, ncols=2)
    time_period = 0.25
    x_conv = []; y_conv = []; z_conv = []; t_conv = []
    st = 0
    end = st+500
    x = df['gFx'].values
    time = df['time'].values
    time_diff = numpy.diff(time)
    cur_time = 0
    cur_acc = []
    step = 0
    for td in time_diff:
        if cur_time + td > time_period:
            x_conv.append(numpy.mean(cur_acc))
            cur_time = 0
            cur_acc = []
        cur_acc.append(x[step])
        cur_time += td
        step += 1
            
    ax1.plot(time, x, c='r');
    ax2.plot(x_conv, c='g');
    #ax1.plot(time, z, c='b');
    #ax1.plot(time, t, c='black');
    
    plt.show()

i = -1
for df, target in zip(Data_train,Target_train):
    i += 1
    show_plot_time2(df, name=f'{target}\n{files_name_train[i]}')
    print('*')    
    if i >0:
        break

rrr = []
#answer = []
i = -1
for df, target in zip(Data_train,Target_train):
    i += 1
    #act = get_action(target)
    #answer.append(act)
    #assert act != -1
    #if key != act:
    #    continue
    x, y, z, t = distribution_of_accelerations.process_data(df, name=f'{target}\n{files_name_train[i]}')
    print('*')    
    if i >10:
        break



