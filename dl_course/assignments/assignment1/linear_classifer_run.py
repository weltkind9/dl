import numpy as np
import matplotlib.pyplot as plt
from dataset import load_svhn, random_split_train_val
from gradient_check import check_gradient
from metrics import multiclass_accuracy 
import linear_classifer
import linear_classifer_methods


batch_size = 2
num_classes = 2
num_features = 3
np.random.seed(42)
W = np.random.randint(-1, 3, size=(num_features, num_classes)).astype(np.float)
X = np.random.randint(-1, 3, size=(batch_size, num_features)).astype(np.float)
target_index = np.ones(batch_size, dtype=np.int)

loss, dW = linear_classifer.linear_softmax(X, W, target_index)
check_gradient(lambda w: linear_classifer.linear_softmax(X, w, target_index), W)
train_X, train_y, test_X, test_y = load_svhn("data", max_train=10000, max_test=500)    
train_X, test_X = linear_classifer_methods.prepare_for_linear_classifier(train_X, test_X)
# Split train into train and val
train_X, train_y, val_X, val_y = random_split_train_val(train_X, train_y, num_val = 500)

loss_history=[]
accuracy_history=[]

#learning_rates = [ 1e-1, 1e-2, 1e-3]#,0.9, 0.5, 0.3, 1e-4, 1e-5]
#reg_strengths = [ 1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7]
#results={}
#for lr in learning_rates:
#    for rg in reg_strengths:
#        classifier = linear_classifer.LinearSoftmaxClassifier(train_X.shape[1], 10)
#        accuracy_history=[]
#        pred=classifier.predict(val_X)
#        accuracy = multiclass_accuracy(pred, val_y)
#        print(f"lr:{lr} rg:{rg} Accuracy: {accuracy}" )        
#        accuracy_history.append(accuracy)
#        for i in range(20):
#            loss_history.extend(classifier.fit(i, train_X, train_y, epochs=5, learning_rate=lr, batch_size=500, reg=rg))
#            pred=classifier.predict(val_X)
#            accuracy = multiclass_accuracy(pred, val_y)
#            print(f"lr: {lr} rg: {rg} {i} Accuracy: {accuracy}" )
#            accuracy_history.append(accuracy)
#        pred=classifier.predict(val_X)
#        accuracy = multiclass_accuracy(pred, val_y)
#        print(f"lr:{lr} rg:{rg} {i} Accuracy: {accuracy}" )
#        results[(lr,rg)]=(accuracy,accuracy_history)



## let's look at the loss history!
#plt.plot(loss_history)


batch_size = 2
num_classes = 2
num_features = 3
np.random.seed(42)
W = np.random.randint(-1, 3, size=(num_features, num_classes)).astype(np.float)
X = np.random.randint(-1, 3, size=(batch_size, num_features)).astype(np.float)
target_index = np.ones(batch_size, dtype=np.int)

loss, dW = linear_classifer.linear_softmax(X, W, target_index)
check_gradient(lambda w: linear_classifer.linear_softmax(X, w, target_index), W)




X=np.array([1, 2])
W=np.array([[1.0, 0.0], [0.0, 1.0]])
loss, dW = linear_classifer.linear_softmax(X, W, 1)
check_gradient(lambda w: linear_classifer.linear_softmax(X, w, 1), W)

# TODO Implement combined function or softmax and cross entropy and produces gradient
loss, grad = linear_classifer.softmax_with_cross_entropy(np.array([1, 0.0001, 0.0001]), 1)
check_gradient(lambda x: linear_classifer.softmax_with_cross_entropy(x, 1), np.array([1, 0.0001, 0.00001], np.float))




# TODO Implement combined function or softmax and cross entropy and produces gradient
loss, grad = linear_classifer.softmax_with_cross_entropy(np.array([1, 0.0001, 0.0001]), 1)
check_gradient(lambda x: linear_classifer.softmax_with_cross_entropy(x, 1), np.array([1, 0.0001, 0.00001], np.float))


check_gradient(linear_classifer_methods.square, np.array([3.0]))
check_gradient(linear_classifer_methods.array_sum, np.array([3.0, 2.0]))
check_gradient(linear_classifer_methods.array_2d_sum, np.array([[3.0, 2.0], [1.0, 0.0]]))



W=np.array([[1.0, 2.0], [3.0, 4.0]])

# TODO Implement l2_regularization function that implements loss for L2 regularization
linear_classifer.l2_regularization(W, 0.01)
check_gradient(lambda w: linear_classifer.l2_regularization(w, 0.01), W)


# TODO: Implement LinearSoftmaxClassifier.fit function
classifier = linear_classifer.LinearSoftmaxClassifier()
loss_history = classifier.fit(train_X, train_y, epochs=10, learning_rate=1e-3, batch_size=300, reg=1e1)
# let's look at the loss history!
plt.plot(loss_history)
# Let's check how it performs on validation set
pred = classifier.predict(val_X)
accuracy = multiclass_accuracy(pred, val_y)
print("Accuracy: ", accuracy)

# Now, let's train more and see if it performs better
classifier.fit(train_X, train_y, epochs=100, learning_rate=1e-3, batch_size=300, reg=1e1)
pred = classifier.predict(val_X)
accuracy = multiclass_accuracy(pred, val_y)
print("Accuracy after training for 100 epochs: ", accuracy)




check_gradient(lambda x: linear_classifer.softmax_with_cross_entropy(x, 1), np.array([1, 2], np.float))

check_gradient(linear_classifer_methods.square, np.array([3.0]))

check_gradient(linear_classifer_methods.array_sum, np.array([3.0, 2.0]))

check_gradient(linear_classifer_methods.softmax, np.array([1,2], np.float))

check_gradient(lambda x: linear_classifer.softmax_with_cross_entropy(x, 1), np.array([1,2], np.float))

# TODO: Implement check_gradient function in gradient_check.py
# All the functions below should pass the gradient check
check_gradient(linear_classifer_methods.square, np.array([3.0]))
check_gradient(linear_classifer_methods.array_sum, np.array([3.0, 2.0]))
check_gradient(linear_classifer_methods.array_2d_sum, np.array([[3.0, 2.0], [1.0, 0.0]]))




# TODO Implement softmax and cross-entropy for single sample
probs = linear_classifer.softmax(np.array([-10, 0, 10]))

# Make sure it works for big numbers too!
probs = linear_classifer.softmax(np.array([1000, 0, 0]))
assert np.isclose(probs[0], 1.0)

probs = linear_classifer.softmax(np.array([-5, 0, 5]))
linear_classifer.cross_entropy_loss(probs, 1)

# TODO Implement combined function or softmax and cross entropy and produces gradient
loss, grad = linear_classifer.softmax_with_cross_entropy(np.array([1, 0, 0]), 1)
check_gradient(lambda x: linear_classifer.softmax_with_cross_entropy(x, 1), np.array([1, 0, 0], np.float))

# TODO Extend combined function so it can receive a 2d array with batch of samples
np.random.seed(42)
# Test batch_size = 1
num_classes = 4
batch_size = 1
predictions = np.random.randint(-1, 3, size=(batch_size, num_classes)).astype(np.float)
target_index = np.random.randint(0, num_classes, size=(batch_size, 1)).astype(np.int)
check_gradient(lambda x: linear_classifer.softmax_with_cross_entropy(x, target_index), predictions)

# Test batch_size = 3
num_classes = 4
batch_size = 3
predictions = np.random.randint(-1, 3, size=(batch_size, num_classes)).astype(np.float)
target_index = np.random.randint(0, num_classes, size=(batch_size, 1)).astype(np.int)
check_gradient(lambda x: linear_classifer.softmax_with_cross_entropy(x, target_index), predictions)

# Make sure maximum subtraction for numberic stability is done separately for every sample in the batch
probs = linear_classifer.softmax(np.array([[20,0,0], [1000, 0, 0]]))
assert np.all(np.isclose(probs[:, 0], 1.0))

# TODO Implement linear_softmax function that uses softmax with cross-entropy for linear classifier
batch_size = 2
num_classes = 2
num_features = 3
np.random.seed(42)
W = np.random.randint(-1, 3, size=(num_features, num_classes)).astype(np.float)
X = np.random.randint(-1, 3, size=(batch_size, num_features)).astype(np.float)
target_index = np.ones(batch_size, dtype=np.int)

loss, dW = linear_classifer.linear_softmax(X, W, target_index)
check_gradient(lambda w: linear_classifer.linear_softmax(X, w, target_index), W)

# TODO Implement l2_regularization function that implements loss for L2 regularization
linear_classifer.l2_regularization(W, 0.01)
check_gradient(lambda w: linear_classifer.l2_regularization(w, 0.01), W)

# TODO: Implement LinearSoftmaxClassifier.fit function
classifier = linear_classifer.LinearSoftmaxClassifier()
loss_history = classifier.fit(train_X, train_y, epochs=10, learning_rate=1e-3, batch_size=300, reg=1e1)

# let's look at the loss history!
plt.plot(loss_history)

# Let's check how it performs on validation set
pred = classifier.predict(val_X)
accuracy = multiclass_accuracy(pred, val_y)
print("Accuracy: ", accuracy)

# Now, let's train more and see if it performs better
classifier.fit(train_X, train_y, epochs=100, learning_rate=1e-3, batch_size=300, reg=1e1)
pred = classifier.predict(val_X)
accuracy = multiclass_accuracy(pred, val_y)
print("Accuracy after training for 100 epochs: ", accuracy)

num_epochs = 200
batch_size = 300

learning_rates = [1e-3, 1e-4, 1e-5]
reg_strengths = [1e-4, 1e-5, 1e-6]

best_classifier = None
best_val_accuracy = None

# TODO use validation set to find the best hyperparameters
# hint: for best results, you might need to try more values for learning rate and regularization strength 
# than provided initially

print('best validation accuracy achieved: %f' % best_val_accuracy)

test_pred = best_classifier.predict(test_X)
test_accuracy = multiclass_accuracy(test_pred, test_y)
print('Linear softmax classifier test set accuracy: %f' % (test_accuracy, ))
