import numpy as np


def softmax(predictions):
    '''
    Computes probabilities from scores

    Arguments:
      predictions, np array, shape is either (N) or (batch_size, N) -
        classifier output

    Returns:
      probs, np array of the same shape as predictions - 
        probability for every class, 0..1
    '''
    # TODO implement softmax
    # Your final implementation shouldn't have any loops    
    return np.exp(predictions-np.max(predictions))/np.sum(np.exp(predictions-np.max(predictions)))


def cross_entropy_loss(probs, target_index):
    '''
    Computes cross-entropy loss

    Arguments:
      probs, np array, shape is either (N) or (batch_size, N) -
        probabilities for every class
      target_index: np array of int, shape is (1) or (batch_size) -
        index of the true class for given sample(s)

    Returns:
      loss: single value
    '''
    # TODO implement cross-entropy
    # Your final implementation shouldn't have any loops    
    loss1 = -np.log(probs[target_index])
    p=np.zeros_like(probs)
    p[target_index]=1
    loss2 = -np.sum(p*np.log(probs))
    return loss2
     

def softmax_with_cross_entropy(predictions, target_index):
    '''
    Computes softmax and cross-entropy loss for model predictions,
    including the gradient

    Arguments:
      predictions, np array, shape is either (N) or (batch_size, N) -
        classifier output
      target_index: np array of int, shape is (1) or (batch_size) -
        index of the true class for given sample(s)

    Returns:
      loss, single value - cross-entropy loss
      dprediction, np array same shape as predictions - gradient of predictions by loss value
    '''
    # TODO implement softmax with cross-entropy
    # Your final implementation shouldn't have any loops
    probs=softmax(predictions)
    loss = cross_entropy_loss(probs, target_index)
    p=np.zeros_like(predictions)
    p[target_index]=1
    dprediction=probs-p
    return loss, dprediction


def l2_regularization(W, reg_strength):
    '''
    Computes L2 regularization loss on weights and its gradient

    Arguments:
      W, np array - weights
      reg_strength - float value

    Returns:
      loss, single value - l2 regularization loss
      gradient, np.array same shape as W - gradient of weight by l2 loss
    '''

    # TODO: implement l2 regularization and gradient
    # Your final implementation shouldn't have any loops
    loss= np.sum(W*W)
    grad=2*W

    return loss, grad
    

def linear_softmax(X, W, y):
    '''
    Performs linear classification and returns loss and gradient over W
    Arguments:
      X, np array, shape (num_batch, num_features) - batch of images
      W, np array, shape (num_features, classes) - weights
      target_index, np array, shape (num_batch) - index of target classes
    Returns:
      loss, single value - cross-entropy loss
      gradient, np.array same shape as W - gradient of weight by loss
    '''
    num_classes = W.shape[1]
    num_train = X.shape[0]
    loss = 0.0
    dW= np.zeros_like(W)
    for i in range(num_train):
        # loss
        scores = X[i].dot(W)
        # shift values for 'scores' for numeric reasons (over-flow cautious)
        scores -= scores.max()
        scores_expsum = np.sum(np.exp(scores))
        cor_ex = np.exp(scores[y[i]])
        loss += - np.log( cor_ex / scores_expsum)
        # grad
        # for correct class        
        dW[:, y[i]] += (-1) * (scores_expsum - cor_ex) / scores_expsum * X[i]
        for j in range(num_classes):
            # pass correct class gradient
            if j == y[i]:
                continue
            # for incorrect classes
            dW[:, j] += np.exp(scores[j]) / scores_expsum * X[i]
    loss /= num_train
    dW /= num_train
    return loss, dW


class LinearSoftmaxClassifier():
    def __init__(self, num_features, num_classes):
        self.W = 0.001 * np.random.randn(num_features, num_classes)

    def fit(self, cur_ep, X, y, batch_size=100, learning_rate=1e-7, reg=1e-5, epochs=1):
        '''
        Trains linear classifier
        
        Arguments:
          X, np array (num_samples, num_features) - training data
          y, np array of int (num_samples) - labels
          batch_size, int - batch size to use
          learning_rate, float - learning rate for gradient descent
          reg, float - L2 regularization strength
          epochs, int - number of epochs
        '''

        num_train = X.shape[0]
        num_features = X.shape[1]
        num_classes = np.max(y)+1

        loss_history = []
        for epoch in range(epochs):
            shuffled_indices = np.arange(num_train)
            np.random.shuffle(shuffled_indices)
            sections = np.arange(batch_size, num_train, batch_size)
            batches_indices = np.array_split(shuffled_indices, sections)
            
            for batch in batches_indices:            
                batch_X=X[batch]
                batch_y=y[batch]
                loss, dW = linear_softmax(batch_X, self.W, batch_y)
                loss2, dW2 = l2_regularization(self.W, reg)
                loss+=loss2
                dW+=dW2
                loss_history.append(loss)
                self.W -= learning_rate * dW

            print(f"lr: {learning_rate} rg: {reg} Epoch {cur_ep*5+epoch}, loss: {loss}")

        return loss_history

    def predict(self, X):
        '''
        Produces classifier predictions on the set
       
        Arguments:
          X, np array (test_samples, num_features)

        Returns:
          y_pred, np.array of int (test_samples)
        '''
        #y_pred = np.zeros(X.shape[0], dtype=np.int)

        # TODO Implement class prediction
        # Your final implementation shouldn't have any loops
        y_pred=np.zeros(X.shape[0])
        pred = X.dot(self.W)
        for j in range(X.shape[0]):
            probs = softmax(pred)
            max_i=0
            max_val=0
            for i in range(probs.shape[1]):
                if probs[j][i]>max_val:
                    max_val=probs[j][i]
                    max_i=i
            y_pred[j]=max_i

        return y_pred



                
                                                          

            

                
