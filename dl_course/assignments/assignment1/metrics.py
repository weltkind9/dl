def binary_classification_metrics(prediction, ground_truth):
    '''
    Computes metrics for binary classification

    Arguments:
    prediction, np array of bool (num_samples) - model predictions
    ground_truth, np array of bool (num_samples) - true labels

    Returns:
    precision, recall, f1, accuracy - classification metrics
    '''


    correct = 0
    tp=0
    fp=0
    fn=0
    count=ground_truth.shape[0]
    for i in range(count):
        if prediction[i]==ground_truth[i]: correct=correct+1
        if ground_truth[i] == False:
            fn=fn+1
            if prediction[i] == False:
                tp=tp+1
        if ground_truth[i] == True and prediction[i] == False:
            fp=fp+1

    precision = tp/(tp+fp+0.0000001)
    recall = tp/(tp+fn+0.0000001)
    accuracy = correct/count
    f1 = 2*precision*recall/(precision+recall+0.0000001)

   

    # TODO: implement metrics!
    # Some helpful links:
    # https://en.wikipedia.org/wiki/Precision_and_recall
    # https://en.wikipedia.org/wiki/F1_score
    
    return precision, recall, f1, accuracy


def multiclass_accuracy(prediction, ground_truth):
    '''
    Computes metrics for multiclass classification

    Arguments:
    prediction, np array of int (num_samples) - model predictions
    ground_truth, np array of int (num_samples) - true labels

    Returns:
    accuracy - ratio of accurate predictions to total samples
    '''
    # TODO: Implement computing accuracy
    correct=0
    for i in range(ground_truth.shape[0]):
        if prediction[i]==ground_truth[i]:
            correct+=1
    res=correct/ground_truth.shape[0]
    return res
