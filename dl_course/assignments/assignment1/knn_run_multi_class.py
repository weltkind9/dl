import numpy as np
import matplotlib.pyplot as plt
from dataset import load_svhn
from knn import KNN
from metrics import binary_classification_metrics, multiclass_accuracy

train_X, train_y, test_X, test_y = load_svhn("data", max_train=1000, max_test=100)
# Now let's use all 10 classes
train_X = train_X.reshape(train_X.shape[0], -1)
test_X = test_X.reshape(test_X.shape[0], -1)

knn_classifier = KNN(k=5)
knn_classifier.fit(train_X, train_y)

# TODO: Implement predict_labels_multiclass
predict = knn_classifier.predict(test_X, 2)

# TODO: Implement multiclass_accuracy
accuracy = multiclass_accuracy(predict, test_y)
print("Accuracy: %4.2f" % accuracy)

# Find the best k using cross-validation based on accuracy
num_folds = 5
train_folds_X = []
train_folds_y = []

# TODO: split the training data in 5 folds and store them in train_folds_X/train_folds_y

k_choices = [1, 2, 3, 5, 8, 10, 15, 20, 25, 50]
k_to_accuracy = {}

for cv_k in k_choices:
    # TODO: perform cross-validation
    # Go through every fold and use it for testing and all other folds for validation
    # Perform training and produce accuracy metric on the validation dataset
    # Average accuracy from all the folds and write it into k_to_accuracy
    cv_f=[]
    for i in range(num_folds):
        l=train_y.shape[0]
        l2=int(train_y.shape[0]/5)
        cv_mask_val=np.zeros(l, np.bool)
        cv_mask_train=np.zeros(l, np.bool)
        for j in range(l):
            cv_mask_train[j]=True
            if j>=i*l2 and j<(i+1)*l2:
                cv_mask_val[j]=True
                cv_mask_train[j]=False

        cv_train_X=train_X[cv_mask_train]
        cv_train_y=train_y[cv_mask_train]
        cv_val_X=train_X[cv_mask_val]
        cv_val_y=train_y[cv_mask_val]
        cv_knn_classifier = KNN(k=cv_k)
        cv_knn_classifier.fit(cv_train_X, cv_train_y)
        cv_prediction = cv_knn_classifier.predict(cv_val_X, 2)
        cv_accuracy = multiclass_accuracy(cv_prediction, cv_val_y)
        print("KNN with k = %s Accuracy: %4.2f" % (cv_k, cv_accuracy))
        cv_f.append(cv_accuracy)

    k_to_accuracy[cv_k]=sum(cv_f)/len(cv_f)

print()
print()
print()

for k in sorted(k_to_accuracy):
    print('k = %d, accuracy = %f' % (k, k_to_accuracy[k]))


print('end...')

