import numpy as np
import math

class neibor:
    def __init__(self, key, count):
        self.key=key
        self.count=count

class KNN:
    """
    K-neariest-neighbor classifier using L1 loss
    """
    def __init__(self, k=1):
        self.k = k

    def fit(self, X, y):
        self.train_X = X
        self.train_y = y

    def predict(self, X, num_loops=0):
        '''
        Uses the KNN model to predict clases for the data samples provided
        
        Arguments:
        X, np array (num_samples, num_features) - samples to run
           through the model
        num_loops, int - which implementation to use

        Returns:
        predictions, np array of ints (num_samples) - predicted class
           for each sample
        '''
        if num_loops == 0:
            dists = self.compute_distances_no_loops(X)
        elif num_loops == 1:
            dists = self.compute_distances_one_loop(X)
        else:
            dists = self.compute_distances_two_loops(X)

        if self.train_y.dtype == np.bool:
            return self.predict_labels_binary(dists)
        else:
            return self.predict_labels_multiclass(dists)

    def compute_distances_two_loops(self, X):
        '''
        Computes L1 distance from every sample of X to every training sample
        Uses simplest implementation with 2 Python loops

        Arguments:
        X, np array (num_test_samples, num_features) - samples to run
        
        Returns:
        dists, np array (num_test_samples, num_train_samples) - array
           with distances between each test and each train sample
        '''
        num_train = self.train_X.shape[0]
        num_test = X.shape[0]
        dists = np.zeros((num_test, num_train), np.float32)
        for i_test in range(num_test):
            for i_train in range(num_train):
                # TODO: Fill dists[i_test][i_train]
                #pass
                dists[i_test][i_train]=np.sum(np.abs(self.train_X[i_train] - X[i_test]))
        return dists

    def compute_distances_one_loop(self, X):
        '''
        Computes L1 distance from every sample of X to every training sample
        Vectorizes some of the calculations, so only 1 loop is used

        Arguments:
        X, np array (num_test_samples, num_features) - samples to run
        
        Returns:
        dists, np array (num_test_samples, num_train_samples) - array
           with distances between each test and each train sample
        '''
        num_train = self.train_X.shape[0]
        num_test = X.shape[0]
        dists = np.zeros((num_test, num_train), np.float32)
        for i_test in range(num_test):
            # TODO: Fill the whole row of dists[i_test]
            # without additional loops or list comprehensions
            pass

    def compute_distances_no_loops(self, X):
        '''
        Computes L1 distance from every sample of X to every training sample
        Fully vectorizes the calculations using numpy

        Arguments:
        X, np array (num_test_samples, num_features) - samples to run
        
        Returns:
        dists, np array (num_test_samples, num_train_samples) - array
           with distances between each test and each train sample
        '''
        num_train = self.train_X.shape[0]
        num_test = X.shape[0]
        # Using float32 to to save memory - the default is float64
        dists = np.zeros((num_test, num_train), np.float32)
        dists=np.absolute(self.train_X-X)
        return dists
        # TODO: Implement computing all distances with no loops!
        pass

    def predict_labels_binary(self, dists):
        '''
        Returns model predictions for binary classification case
        
        Arguments:
        dists, np array (num_test_samples, num_train_samples) - array
           with distances between each test and each train sample

        Returns:
        pred, np array of bool (num_test_samples) - binary predictions 
           for every test sample
        '''
        num_test = dists.shape[0]
        pred = np.zeros(num_test, np.bool)
        num_train = self.train_X.shape[0]
        srt = []
        for i in range(num_test):
            srt.append([])
            # TODO: Implement choosing best class based on k
            # nearest training samples
            for j in range(num_train):
                srt[i].append((dists[i][j],j))
        srt2 = []
        for i in range(num_test):
            srt2.append(sorted(srt[i],key=lambda s:s[0]))
        count=np.zeros((num_test, self.k), np.bool)
        for i in range(num_test):
            for j in range(self.k):
                count[i][j]=self.train_y[srt2[i][j][1]]

        for i in range(num_test):
            pos_count=0
            neg_count=0
            for j in range(self.k):
                if count[i][j]==True:
                    pos_count=pos_count+1
                else:
                    neg_count=neg_count+1
            if(pos_count>neg_count):
                pred[i]=True

        return pred

    def predict_labels_multiclass(self, dists):
        '''
        Returns model predictions for multi-class classification case
        
        Arguments:
        dists, np array (num_test_samples, num_train_samples) - array
           with distances between each test and each train sample

        Returns:
        pred, np array of int (num_test_samples) - predicted class index 
           for every test sample
        '''
        num_test = dists.shape[0]
        pred = np.zeros(num_test, np.int)
        num_train = self.train_X.shape[0]
        srt = []
        for i in range(num_test):
            # TODO: Implement choosing best class based on k
            # nearest training samples
            srt.append([])
            # TODO: Implement choosing best class based on k
            # nearest training samples
            for j in range(num_train):
                srt[i].append((dists[i][j],j))
        srt2 = []
        for i in range(num_test):
            srt2.append(sorted(srt[i],key=lambda s:s[0]))
        count=[]
        for i in range(num_test):
            count.append({})
            for j in range(self.k):
                key=self.train_y[srt2[i][j][1]]
                if key in count[i]:
                    count[i][key]+=1
                else:
                    count[i][key]=1

        for i in range(num_test):
            w_label=-1
            w_count=0
            for key in count[i]:
                if count[i][key]>w_count:
                    w_count=count[i][key]
                    w_label=key
            pred[i]=w_label

        return pred
