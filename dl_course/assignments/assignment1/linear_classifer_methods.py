import numpy as np

def prepare_for_linear_classifier(train_X, test_X):
    train_flat = train_X.reshape(train_X.shape[0], -1).astype(np.float) / 255.0
    test_flat = test_X.reshape(test_X.shape[0], -1).astype(np.float) / 255.0
    
    # Subtract mean
    mean_image = np.mean(train_flat, axis = 0)
    train_flat -= mean_image
    test_flat -= mean_image
    
    # Add another channel with ones as a bias term
    train_flat_with_ones = np.hstack([train_flat, np.ones((train_X.shape[0], 1))])
    test_flat_with_ones = np.hstack([test_flat, np.ones((test_X.shape[0], 1))])    
    return train_flat_with_ones, test_flat_with_ones
    

def f(x):
    """
    Computes function and analytic gradient at x

    x: np array of float, input to the function

    Returns:
    value: float, value of the function 
    grad: np array of float, same shape as x
    """
    ...

    return value, grad

def square(x):
    return float(x*x), 2*x

def array_sum(x):
    assert x.shape == (2,), x.shape
    return np.sum(x), np.ones_like(x)

def array_2d_sum(x):
    assert x.shape == (2,2)
    return np.sum(x), np.ones_like(x)

def softmax(x):
    assert x.shape == (2,), x.shape
    return np.max( np.exp(x-np.max(x))/np.sum(np.exp(x-np.max(x)))), np.ones_like(x)