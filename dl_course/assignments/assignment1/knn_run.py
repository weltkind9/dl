import numpy as np
import matplotlib.pyplot as plt
from dataset import load_svhn
from knn import KNN
from metrics import binary_classification_metrics, multiclass_accuracy

train_X, train_y, test_X, test_y = load_svhn("data", max_train=1000, max_test=100)
samples_per_class = 5  # Number of samples per class to visualize
plot_index = 1
for example_index in range(samples_per_class):
    for class_index in range(10):
        plt.subplot(5, 10, plot_index)
        image = train_X[train_y == class_index][example_index]
        plt.imshow(image.astype(np.uint8))
        plt.axis('off')
        plot_index += 1
        
# First, let's prepare the labels and the source data

# Only select 0s and 9s
binary_train_mask = (train_y == 0) | (train_y == 9)
binary_train_X = train_X[binary_train_mask]
binary_train_y = train_y[binary_train_mask] == 0

binary_test_mask = (test_y == 0) | (test_y == 9)
binary_test_X = test_X[binary_test_mask]
binary_test_y = test_y[binary_test_mask] == 0

# Reshape to 1-dimensional array [num_samples, 32*32*3]
binary_train_X = binary_train_X.reshape(binary_train_X.shape[0], -1)
binary_test_X = binary_test_X.reshape(binary_test_X.shape[0], -1)

# Create the classifier and call fit to train the model
# KNN just remembers all the data
knn_classifier = KNN(k=1)
knn_classifier.fit(binary_train_X, binary_train_y)

dists = knn_classifier.compute_distances_two_loops(binary_test_X)
assert np.isclose(dists[0, 10], np.sum(np.abs(binary_test_X[0] - binary_train_X[10])))

res=np.sum(np.abs(binary_test_X[0] - binary_train_X[10]))
ass = np.isclose(dists[0, 10], np.sum(np.abs(binary_test_X[0] - binary_train_X[10])))
prediction = knn_classifier.predict(binary_test_X, 2)
precision, recall, f1, accuracy = binary_classification_metrics(prediction, binary_test_y)
print("KNN with k = %s" % knn_classifier.k)
print("Accuracy: %4.2f, Precision: %4.2f, Recall: %4.2f, F1: %4.2f" % (accuracy, precision, recall, f1)) 


knn_classifier_3 = KNN(k=3)
knn_classifier_3.fit(binary_train_X, binary_train_y)
prediction2 = knn_classifier_3.predict(binary_test_X, 2)

precision, recall, f1, accuracy = binary_classification_metrics(prediction2, binary_test_y)
print("KNN with k = %s" % knn_classifier_3.k)
print("Accuracy: %4.2f, Precision: %4.2f, Recall: %4.2f, F1: %4.2f" % (accuracy, precision, recall, f1))
prediction_diff=np.zeros(binary_test_y.shape[0], np.bool)
for i in range(binary_test_y.shape[0]):
    if prediction[i] == prediction2[i]:
        prediction_diff[i]=True



# Find the best k using cross-validation based on F1 score
num_folds = 5
train_folds_X = []
train_folds_y = []

# TODO: split the training data in 5 folds and store them in train_folds_X/train_folds_y

k_choices = [1, 2, 3, 5, 8, 10, 15, 20, 25, 50]
k_to_f1 = {}  # dict mapping k values to mean F1 scores (int -> float)

for cv_k in k_choices:
    # TODO: perform cross-validation
    # Go through every fold and use it for testing and all other folds for training
    # Perform training and produce F1 score metric on the validation dataset
    # Average F1 from all the folds and write it into k_to_f1
    
    cv_f=[]
    for i in range(num_folds):
        l=binary_train_y.shape[0]
        l2=int(binary_train_y.shape[0]/5)
        cv_mask_val=np.zeros(l, np.bool)
        cv_mask_train=np.zeros(l, np.bool)
        for j in range(l):
            cv_mask_train[j]=True
            if j>=i*l2 and j<(i+1)*l2:
                cv_mask_val[j]=True
                cv_mask_train[j]=False

        cv_train_X=binary_train_X[cv_mask_train]
        cv_train_y=binary_train_y[cv_mask_train]
        cv_val_X=binary_train_X[cv_mask_val]
        cv_val_y=binary_train_y[cv_mask_val]
        cv_knn_classifier = KNN(k=cv_k)
        cv_knn_classifier.fit(cv_train_X, cv_train_y)
        cv_prediction = cv_knn_classifier.predict(cv_val_X, 2)
        cv_precision, cv_recall, cv_f1, cv_accuracy = binary_classification_metrics(cv_prediction, cv_val_y)
        print("KNN with k = %s Accuracy: %4.2f, Precision: %4.2f, Recall: %4.2f, F1: %4.2f" % (cv_k, cv_accuracy, cv_precision, cv_recall, cv_f1))
        cv_f.append(cv_f1)

    k_to_f1[cv_k]=sum(cv_f)/len(cv_f)

for k in sorted(k_to_f1):
    print('k = %d, f1 = %f' % (k, k_to_f1[k]))


print('end')