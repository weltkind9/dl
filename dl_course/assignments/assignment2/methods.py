import numpy as np
from gradient_check import check_gradient

def check():
    check_gradient(square, np.array([3.0]))
    check_gradient(array_sum, np.array([3.0, 2.0]))
    check_gradient(array_2d_sum, np.array([[3.0, 2.0], [1.0, 0.0]]))
    check_gradient(lambda x: softmax_with_cross_entropy2(x, 1), np.array([1, 0.0001, 0.00001], np.float))

    X=np.array([1, 2])
    W=np.array([[1.0, 0.0], [0.0, 1.0]])
    loss, dW = linear_softmax2(X, W, 1)
    check_gradient(lambda w: linear_softmax2(X, w, 1), W)
    
    loss, dW = l2_regularization(W, 0.01)
    check_gradient(lambda w: l2_regularization(w, 0.01), W)

    np.random.seed(42)
    # Test batch_size = 1
    num_classes = 4
    batch_size = 1
    predictions = np.random.randint(-1, 3, size=(batch_size, num_classes)).astype(np.float)
    target_index = np.random.randint(0, num_classes, size=(batch_size, 1)).astype(np.int)
    loss, dW = softmax_with_cross_entropy(predictions, target_index)
    check_gradient(lambda x: softmax_with_cross_entropy(x, target_index), predictions)

    # Test batch_size = 3
    num_classes = 4
    batch_size = 3
    predictions = np.random.randint(-1, 3, size=(batch_size, num_classes)).astype(np.float)
    target_index = np.random.randint(0, num_classes, size=(batch_size, 1)).astype(np.int)
    loss, dW = softmax_with_cross_entropy(predictions, target_index)
    check_gradient(lambda x: softmax_with_cross_entropy(x, target_index), predictions)

    batch_size = 2
    num_classes = 2
    num_features = 3
    np.random.seed(42)
    W = np.random.randint(-1, 3, size=(num_features, num_classes)).astype(np.float)
    X = np.random.randint(-1, 3, size=(batch_size, num_features)).astype(np.float)
    target_index = np.ones(batch_size, dtype=np.int)

    loss, dW = linear_softmax(X, W, target_index)
    check_gradient(lambda w: linear_softmax(X, w, target_index), W)
    
    check_gradient(lambda w: l2_regularization(w, 0.01), W)



def f(x):
    """
    Computes function and analytic gradient at x

    x: np array of float, input to the function

    Returns:
    value: float, value of the function 
    grad: np array of float, same shape as x
    """
    ...

    return value, grad

def square(x):
    return float(x*x), 2*x

def array_sum(x):
    assert x.shape == (2,), x.shape
    return np.sum(x), np.ones_like(x)

def array_2d_sum(x):
    assert x.shape == (2,2)
    return np.sum(x), np.ones_like(x)

def softmax(predictions):
    '''
    Computes probabilities from scores
    Arguments:
      predictions, np array, shape is either (N) or (batch_size, N) -
        classifier output
    Returns:
      probs, np array of the same shape as predictions - 
        probability for every class, 0..1
    '''
    mx=-np.max(predictions,axis=1)    
    x2 = np.add(predictions, mx.reshape(predictions.shape[0],1))
    x3=np.exp(x2)
    x4=np.sum(np.exp(x2), axis=1)
    r = x3 / x4.reshape(predictions.shape[0],1)
    return r

def softmax2(predictions):
    '''
    Computes probabilities from scores
    Arguments:
      predictions, np array, shape is either (N) or (batch_size, N) -
        classifier output
    Returns:
      probs, np array of the same shape as predictions - 
        probability for every class, 0..1
    '''
    # TODO implement softmax
    # Your final implementation shouldn't have any loops    
    return np.exp(predictions-np.max(predictions))/np.sum(np.exp(predictions-np.max(predictions)))

def cross_entropy_loss2(probs, target_index):
    '''
    Computes cross-entropy loss
    Arguments:
      probs, np array, shape is either (N) or (batch_size, N) -
        probabilities for every class
      target_index: np array of int, shape is (1) or (batch_size) -
        index of the true class for given sample(s)
    Returns:
      loss: single value
    '''
    loss = -np.log(probs[target_index])
    return loss

def softmax_with_cross_entropy2(predictions, target_index):
    probs=softmax2(predictions)
    loss = cross_entropy_loss2(probs, target_index)
    p=np.zeros_like(predictions)
    p[target_index]=1
    dprediction=probs-p
    return loss, dprediction

def softmax_with_cross_entropy(predictions, target_index):
    probs=softmax(predictions)
    p=np.zeros_like(predictions)
    loss=0
    for i in range(probs.shape[0]):
        loss += cross_entropy_loss2(probs[i], target_index[i])    
        p[i][target_index[i]]=1
    dprediction=probs-p
    return loss, dprediction

def l2_regularization(W, reg_strength):
    '''
    Computes L2 regularization loss on weights and its gradient
    Arguments:
      W, np array - weights
      reg_strength - float value
    Returns:
      loss, single value - l2 regularization loss
      gradient, np.array same shape as W - gradient of weight by l2 loss
    '''
    loss= np.sum(W*W)*reg_strength
    grad=2*W*reg_strength
    return loss, grad

def linear_softmax(X, W, y):
    '''
    Performs linear classification and returns loss and gradient over W
    Arguments:
      X, np array, shape (num_batch, num_features) - batch of images
      W, np array, shape (num_features, classes) - weights
      target_index, np array, shape (num_batch) - index of target classes
    Returns:
      loss, single value - cross-entropy loss
      gradient, np.array same shape as W - gradient of weight by loss
    '''
    num_classes = W.shape[1]
    num_train = X.shape[0]
    loss = 0.0
    dW= np.zeros_like(W)
    for i in range(num_train):
        # loss
        scores = X[i].dot(W)
        # shift values for 'scores' for numeric reasons (over-flow cautious)
        scores -= scores.max()
        scores_expsum = np.sum(np.exp(scores))
        cor_ex = np.exp(scores[y[i]])
        loss += - np.log( cor_ex / scores_expsum)
        # grad
        # for correct class        
        dW[:, y[i]] += (-1) * (scores_expsum - cor_ex) / scores_expsum * X[i]
        for j in range(num_classes):
            # pass correct class gradient
            if j == y[i]:
                continue
            # for incorrect classes
            dW[:, j] += np.exp(scores[j]) / scores_expsum * X[i]
    loss /= num_train
    dW /= num_train
    return loss, dW

def linear_softmax2(X, W, target_index):
    '''
    Performs linear classification and returns loss and gradient over W
    Arguments:
      X, np array, shape (num_features) - batch of images
      W, np array, shape (num_features, classes) - weights
      target_index, np array, shape (num_batch) - index of target classes
    Returns:
      loss, single value - cross-entropy loss
      gradient, np.array same shape as W - gradient of weight by loss
    '''
    num_classes = W.shape[1]
    loss = 0.0
    dW= np.zeros_like(W)
    # loss
    scores = X.dot(W)
    # shift values for 'scores' for numeric reasons (over-flow cautious)
    scores -= scores.max()
    scores_expsum = np.sum(np.exp(scores))
    cor_ex = np.exp(scores[target_index])
    loss += - np.log( cor_ex / scores_expsum)
    # grad
    # for correct class        
    dW[:, target_index] += (-1) * (scores_expsum - cor_ex) / scores_expsum * X
    for j in range(num_classes):
        # pass correct class gradient
        if j == target_index:
            continue
        # for incorrect classes
        dW[:, j] += np.exp(scores[j]) / scores_expsum * X
    return loss, dW