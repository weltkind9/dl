import numpy as np
import matplotlib.pyplot as plt
from dataset import load_svhn, random_split_train_val, prepare_for_neural_network, load_files
from gradient_check import check_layer_gradient, check_layer_param_gradient, check_model_gradient
from layers import FullyConnectedLayer, ReLULayer
import model as mdl
from trainer import Trainer, Dataset
from optim import SGD, MomentumSGD
from metrics import multiclass_accuracy, multiclass_accuracy_with_log
import methods
import gradient_check
import cv2
import os

train_X, train_y, test_X, test_y = load_svhn("data", max_train=10000, max_test=1000)

train_X, test_X = prepare_for_neural_network(train_X, test_X)
noutput = train_y.max()+1
# Split train into train and val
train_X, train_y, val_X, val_y = random_split_train_val(train_X, train_y, num_val = 1)

model = mdl.TwoLayerNet(n_input = train_X.shape[1], n_output = noutput, hidden_layer_size = 100, reg = 1e-1)
dataset = Dataset(train_X, train_y, val_X, val_y, test_X, test_y)
trainer = Trainer(model, dataset, MomentumSGD(), num_epochs=3000, batch_size=500, learning_rate=0.1, learning_rate_decay=0.99)

initial_learning_rate = trainer.learning_rate
loss_history, train_history, val_history = trainer.fit()

accuracy = multiclass_accuracy_with_log(model.predict(test_X), test_y)
print(f"final accuracy: {accuracy}" )
for i in range(len(test_X)):
    p = model.predict(test_X)
print("end...")
