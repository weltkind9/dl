import numpy as np
import methods

class Param:
    '''
    Trainable parameter of the model
    Captures both parameter value and the gradient
    '''
    def __init__(self, value):
        self.value = value
        self.grad = np.zeros_like(value)

        
class ReLULayer:
    def __init__(self):
        self.X=None

    def forward(self, X):
        # Hint: you'll need to save some information about X
        # to use it later in the backward pass
        self.X=X
        res=np.maximum(0,X)
        return res

    def backward(self, d_out):
        """
        Backward pass

        Arguments:
        d_out, np array (batch_size, num_features) - gradient
           of loss function with respect to output

        Returns:
        d_result: np array (batch_size, num_features) - gradient
          with respect to input
        """
        # Your final implementation shouldn't have any loops
        d_result=(self.X > 0) * d_out
        return d_result

    def params(self):
        # ReLU Doesn't have any parameters
        return {}

class ConvolutionalLayer:
    def __init__(self, in_channels, out_channels,
                 filter_size, padding):
        '''
        Initializes the layer
        
        Arguments:
        in_channels, int - number of input channels
        out_channels, int - number of output channels
        filter_size, int - size of the conv filter
        padding, int - number of 'pixels' to pad on each side
        '''

        self.filter_size = filter_size
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.W = Param(
            np.random.randn(filter_size, filter_size,
                            in_channels, out_channels)
        )

        self.B = Param(np.ones(out_channels))

        self.padding = padding


    def forward(self, X):
        self.W.grad=np.zeros_like(self.W.grad)
        self.B.grad=np.zeros_like(self.B.grad)

        batch_size, height, width, channels = X.shape
        out_height = height+2*self.padding-self.filter_size+1
        out_width = width+2*self.padding-self.filter_size+1

        self.X=X
        if self.padding>0:
            X1=np.zeros((batch_size, height+2*self.padding, width+2*self.padding, channels))
            X1[:,1:height+1,1:width+1,:]=X
            X_pad=X1
        else:
            X_pad=X
        result = np.zeros((batch_size, out_height, out_width, self.out_channels))
        
        # TODO: Implement forward pass
        # Hint: setup variables that hold the result
        # and one x/y location at a time in the loop below
        
        # It's ok to use loops for going over width and height
        # but try to avoid having any other loops
        for y in range(out_height):
            for x in range(out_width):
                # TODO: Implement forward pass for specific location
                #r = np.dot(X.reshape(batch_size,width*height* self.in_channels),
                #           self.W.value.reshape(self.filter_size*self. filter_size*self.in_channels, self.out_channels))
                r = np.dot(X_pad[:,y: y+self.filter_size,x:x+self.filter_size, :].reshape(batch_size,self.filter_size*self.filter_size* self.in_channels),
                           self.W.value.reshape(self.filter_size*self.filter_size*self.in_channels, self.out_channels))

                r += self.B.value
                result[:,y,x,:] = r
        return result


    def backward(self, d_out):
        # Hint: Forward pass was reduced to matrix multiply
        # You already know how to backprop through that
        # when you implemented FullyConnectedLayer
        # Just do it the same number of times and accumulate gradients

        batch_size, height, width, channels = self.X.shape
        _, out_height, out_width, out_channels = d_out.shape
        if self.padding>0:
            X1=np.zeros((batch_size, height+2*self.padding, width+2*self.padding, channels))
            X1[:,1:height+1,1:width+1,:]=self.X
            X_pad=X1
        else:
            X_pad=self.X
        # TODO: Implement backward pass
        # Same as forward, setup variables of the right shape that
        # aggregate input gradient and fill them for every location
        # of the output
        d_input_pad=np.zeros(X_pad.shape)
        d_input=np.zeros(self.X.shape)
        # Try to avoid having any other loops here too
        for y in range(out_height):
            for x in range(out_width):
                # TODO: Implement backward pass for specific location
                # Aggregate gradients for both the input and
                # the parameters (W and B)
                d_input_pad[:,y:y+self.filter_size,x:x+self.filter_size,:] += np.dot(
                   d_out[:,y,x,:],
                   self.W.value.reshape(self.filter_size*self. filter_size*self.in_channels, self.out_channels).transpose()
                   ) \
                   .reshape((batch_size, self.filter_size,self.filter_size, self.in_channels ))

                self.params()['W'].grad += np.dot(
                    X_pad[:,y:y+self.filter_size,x:x+self.filter_size,:].reshape(batch_size,self.filter_size*self.filter_size*self.in_channels).transpose(),
                    d_out[:,y,x,:]
                    ) \
                    .reshape(self.filter_size, self.filter_size, self.in_channels, self.out_channels)
                
                #self.params()['B'].grad += np.sum(d_out[:,y,x,:], axis=1)
                self.params()['B'].grad += np.sum(d_out[:,y,x,:], axis=0)
        d_input=d_input_pad[:,self.padding:self.padding+height,self.padding:self.padding+width,:]
        return d_input



    def params(self):
        return { 'W': self.W, 'B': self.B }
   
class ConvolutionalLayerGray:
    def __init__(self, filter_size, padding):
        '''
        Initializes the layer        
        Arguments:
        in_channels, int - number of input channels
        out_channels, int - number of output channels
        filter_size, int - size of the conv filter
        padding, int - number of 'pixels' to pad on each side
        '''
        self.filter_size = filter_size
        self.W = Param( np.random.randn(filter_size, filter_size) )
        self.B = Param(1.0)
        self.padding = padding


    def forward(self, X):
        self.W.grad=np.zeros_like(self.W.grad)
        self.B.grad=np.zeros_like(self.B.grad)

        batch_size, height, width = X.shape
        out_height = height+2*self.padding-self.filter_size+1
        out_width = width+2*self.padding-self.filter_size+1

        self.X=X
        if self.padding>0:
            X1=np.zeros((batch_size, height+2*self.padding, width+2*self.padding))
            X1[:,1:height+1,1:width+1]=X
            X_pad=X1
        else:
            X_pad=X
        result = np.zeros((batch_size, out_height, out_width))
        
        # TODO: Implement forward pass
        # Hint: setup variables that hold the result
        # and one x/y location at a time in the loop below
        
        # It's ok to use loops for going over width and height
        # but try to avoid having any other loops
        for y in range(out_height):
            for x in range(out_width):
                # TODO: Implement forward pass for specific location
                #r = np.dot(X.reshape(batch_size,width*height* self.in_channels),
                #           self.W.value.reshape(self.filter_size*self. filter_size*self.in_channels, self.out_channels))
                r = np.dot(X_pad[:,y: y+self.filter_size,x:x+self.filter_size].reshape(batch_size,self.filter_size*self.filter_size),
                           self.W.value.reshape(self.filter_size*self.filter_size))

                r += self.B.value
                result[:,y,x] = r
        return result


    def backward(self, d_out):
        # Hint: Forward pass was reduced to matrix multiply
        # You already know how to backprop through that
        # when you implemented FullyConnectedLayer
        # Just do it the same number of times and accumulate gradients

        batch_size, height, width = self.X.shape
        _, out_height, out_width = d_out.shape
        if self.padding>0:
            X1=np.zeros((batch_size, height+2*self.padding, width+2*self.padding))
            X1[:,1:height+1,1:width+1]=self.X
            X_pad=X1
        else:
            X_pad=self.X
        # TODO: Implement backward pass
        # Same as forward, setup variables of the right shape that
        # aggregate input gradient and fill them for every location
        # of the output
        d_input_pad=np.zeros(X_pad.shape)
        d_input=np.zeros(self.X.shape)
        # Try to avoid having any other loops here too
        for y in range(out_height):
            for x in range(out_width):
                # TODO: Implement backward pass for specific location
                # Aggregate gradients for both the input and
                # the parameters (W and B)
                d_input_pad[:,y:y+self.filter_size,x:x+self.filter_size] += \
                   d_out[:,y,x][0] * self.W.value.reshape(self.filter_size*self. filter_size).transpose() \
                   .reshape(( self.filter_size,self.filter_size ))

                self.params()['W'].grad += np.dot(
                    X_pad[:,y:y+self.filter_size,x:x+self.filter_size].reshape(batch_size,self.filter_size*self.filter_size).transpose(),
                    d_out[:,y,x]
                    ) \
                    .reshape(self.filter_size, self.filter_size)
                
                #self.params()['B'].grad += np.sum(d_out[:,y,x,:], axis=1)
                self.params()['B'].grad += np.sum(d_out[:,y,x], axis=0)
        d_input=d_input_pad[:,self.padding:self.padding+height,self.padding:self.padding+width]
        return d_input



    def params(self):
        return { 'W': self.W, 'B': self.B }
  
class FullyConnectedLayer:
    def __init__(self, n_input, n_output):
        self.W = Param(0.001 * np.random.randn(n_input, n_output))
        self.B = Param(0.001 * np.random.randn(1, n_output))
        self.X = None

    def forward(self, X):
        self.W.grad=np.zeros_like(self.W.grad)
        self.B.grad=np.zeros_like(self.B.grad)

        self.X=X
        return np.add(np.dot(X, self.W.value), self.B.value)

    def backward(self, d_out):
        """
        Backward pass
        Computes gradient with respect to input and
        accumulates gradients within self.W and self.B
        Arguments:
        d_out, np array (batch_size, n_output) - gradient
           of loss function with respect to output
        Returns:
        d_result: np array (batch_size, n_input) - gradient
          with respect to input
        """
        # Compute both gradient with respect to input
        # and gradients with respect to W and B
        # Add gradients of W and B to their `grad` attribute
        # It should be pretty similar to linear classifier from
        # the previous assignment
        self.params()['W'].grad = self.X.transpose().dot(d_out)
        self.params()['B'].grad = np.sum(d_out, axis=0).reshape(1,d_out.shape[1])
        d_input = d_out.dot(self.params()['W'].value.transpose())
        return d_input

    def params(self):
        return { 'W': self.W, 'B': self.B }


class MaxPoolingLayer:
    def __init__(self, pool_size, stride):
        '''
        Initializes the max pool

        Arguments:
        pool_size, int - area to pool
        stride, int - step size between pooling windows
        '''
        self.pool_size = pool_size
        self.stride = stride
        self.X = None

    #def forward(self, X):
    #    batch_size, height, width, channels = X.shape
    #    self.X=X
    #    # TODO: Implement maxpool forward pass
    #    # Hint: Similarly to Conv layer, loop on
    #    # output x/y dimension
    #    out_height = (height-self.pool_size) // self.stride + 1
    #    out_width = (width-self.pool_size) // self.stride + 1
    #    result = np.zeros((batch_size,out_height, out_width, channels))
    #    for y in range(out_height):
    #        for x in range(out_width):
    #            result[:,y,x,:]=np.max(X[:, y*self.stride:y*self.stride+self.pool_size, x*self.stride:x*self.stride+self.pool_size, :])
    #    return result

    def forward(self, X):
        batch_size, height, width, channels = X.shape
        self.X=X
        # TODO: Implement maxpool forward pass
        # Hint: Similarly to Conv layer, loop on
        # output x/y dimension
        out_height = (height-self.pool_size) // self.stride + 1
        out_width = (width-self.pool_size) // self.stride + 1
        result = np.zeros((batch_size,out_height, out_width, channels))
        s=self.stride
        ps=self.pool_size
        for b in range(batch_size):
            for y in range(out_height):
                for x in range(out_width):
                    for c in range(channels):
                        result[b,y,x,c]=np.max(self.X[b, y*s:y*s+ps, x*s:x*s+ps, c])
        return result

    def backward(self, d_out):
        # TODO: Implement maxpool backward pass
        batch_size, height, width, channels = self.X.shape
        out_height = (height-self.pool_size) // self.stride + 1
        out_width = (width-self.pool_size) // self.stride + 1
        s=self.stride
        ps=self.pool_size
        result=np.zeros_like(self.X)
        for b in range(batch_size):
            for y in range(out_height):
                for x in range(out_width):
                    for c in range(channels):
                        window=self.X[b, y*s:y*s+ps, x*s:x*s+ps, c]
                        zeros = window == np.max(window)
                        result[b, y*s:y*s+ps, x*s:x*s+ps, c] = d_out[b, y, x, c] * zeros

        return result

    def params(self):
        return {}


class MaxPoolingLayerGray:
    def __init__(self, pool_size, stride):
        '''
        Initializes the max pool

        Arguments:
        pool_size, int - area to pool
        stride, int - step size between pooling windows
        '''
        self.pool_size = pool_size
        self.stride = stride
        self.X = None

    #def forward(self, X):
    #    batch_size, height, width, channels = X.shape
    #    self.X=X
    #    # TODO: Implement maxpool forward pass
    #    # Hint: Similarly to Conv layer, loop on
    #    # output x/y dimension
    #    out_height = (height-self.pool_size) // self.stride + 1
    #    out_width = (width-self.pool_size) // self.stride + 1
    #    result = np.zeros((batch_size,out_height, out_width, channels))
    #    for y in range(out_height):
    #        for x in range(out_width):
    #            result[:,y,x,:]=np.max(X[:, y*self.stride:y*self.stride+self.pool_size, x*self.stride:x*self.stride+self.pool_size, :])
    #    return result

    def forward(self, X):
        batch_size, height, width = X.shape
        self.X=X
        # TODO: Implement maxpool forward pass
        # Hint: Similarly to Conv layer, loop on
        # output x/y dimension
        out_height = (height-self.pool_size) // self.stride + 1
        out_width = (width-self.pool_size) // self.stride + 1
        result = np.zeros((batch_size,out_height, out_width))
        s=self.stride
        ps=self.pool_size
        for b in range(batch_size):
            for y in range(out_height):
                for x in range(out_width):
                        result[b,y,x]=np.max(self.X[b, y*s:y*s+ps, x*s:x*s+ps])
        return result

    def backward(self, d_out):
        # TODO: Implement maxpool backward pass
        batch_size, height, width = self.X.shape
        out_height = (height-self.pool_size) // self.stride + 1
        out_width = (width-self.pool_size) // self.stride + 1
        s=self.stride
        ps=self.pool_size
        result=np.zeros_like(self.X)
        for b in range(batch_size):
            for y in range(out_height):
                for x in range(out_width):
                        window=self.X[b, y*s:y*s+ps, x*s:x*s+ps]
                        zeros = window == np.max(window)
                        result[b, y*s:y*s+ps, x*s:x*s+ps] = d_out[b, y, x] * zeros

        return result

    def params(self):
        return {}

class Flattener:
    def __init__(self):
        self.X_shape = None

    def forward(self, X):
        batch_size, height, width, channels = X.shape
        self.X_shape = X
        # TODO: Implement forward pass
        # Layer should return array with dimensions
        # [batch_size, hight*width*channels]
        return X.reshape(batch_size, height*width*channels)

    def backward(self, d_out):
        # TODO: Implement backward pass
        return d_out.reshape(self.X_shape.shape)

    def params(self):
        # No params!
        return {}

class FlattenerGray:
    def __init__(self):
        self.X_shape = None

    def forward(self, X):
        batch_size, height, width = X.shape
        self.X_shape = X
        # TODO: Implement forward pass
        # Layer should return array with dimensions
        # [batch_size, hight*width*channels]
        return X.reshape(batch_size, height*width)

    def backward(self, d_out):
        # TODO: Implement backward pass
        return d_out.reshape(self.X_shape.shape)

    def params(self):
        # No params!
        return {}
