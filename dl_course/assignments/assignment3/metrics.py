def multiclass_accuracy(prediction, ground_truth):
    """
    Computes metrics for multiclass classification
    Arguments:
    prediction, np array of int (num_samples) - model predictions
    ground_truth, np array of int (num_samples) - true labels
    Returns:
    accuracy - ratio of accurate predictions to total samples
    """
    correct=0
    for i in range(ground_truth.shape[0]):
        if prediction[i]==ground_truth[i]:
            correct+=1
    res=correct/ground_truth.shape[0]
    return res

def multiclass_accuracy_with_log(prediction, ground_truth):
    correct=0
    for i in range(ground_truth.shape[0]):
        if prediction[i]==ground_truth[i]:
            correct+=1
        else:
            print(f'ground_truth {ground_truth[i]} pred {prediction[i]}')
    res=correct/ground_truth.shape[0]
    return res