import numpy as np
from methods import softmax_with_cross_entropy, l2_regularization, softmax
from layers import (FullyConnectedLayer, ReLULayer, MaxPoolingLayerGray, FlattenerGray,
    ConvolutionalLayer, MaxPoolingLayer, Flattener, ConvolutionalLayerGray)


class ConvNet:
    """
    Implements a very simple conv net

    Input -> Conv[3x3] -> Relu -> Maxpool[4x4] ->
    Conv[3x3] -> Relu -> MaxPool[4x4] ->
    Flatten -> FC -> Softmax
    """
    def __init__(self, input_shape, n_output_classes, conv1_channels, conv2_channels):
        """
        Initializes the neural network

        Arguments:
        input_shape, tuple of 3 ints - image_width, image_height, n_channels
                                         Will be equal to (32, 32, 3)
        n_output_classes, int - number of classes to predict
        conv1_channels, int - number of filters in the 1st conv layer
        conv2_channels, int - number of filters in the 2nd conv layer
        """
        self.l1=ConvolutionalLayer(input_shape[2], 3,2, 1)
        self.l2= ReLULayer()
        self.l3=MaxPoolingLayer(4,4)
        self.l4=ConvolutionalLayer(3, 3,2, 1)
        self.l5= ReLULayer()
        self.l6=MaxPoolingLayer(4,4)
        self.l7=Flattener()
        #self.l8=FullyConnectedLayer(225,4)
        self.l8=FullyConnectedLayer(12, n_output_classes)
        self.reg=1e-1


    def compute_loss_and_gradients(self, X, y):
        """
        Computes total loss and updates parameter gradients
        on a batch of training examples
        Arguments:
        X, np array (batch_size, height, width, input_features) - input data
        y, np array of int (batch_size) - classes
        """
        # Before running forward and backward pass through the model,
        # clear parameter gradients aggregated from the previous pass
        # TODO Compute loss and fill param gradients
        # Don't worry about implementing L2 regularization, we will not
        # need it in this assignment
        N=X.shape[0]
        res1= self.l1.forward(X)
        res2= self.l2.forward(res1)
        res3=self.l3.forward(res2)
        res4=self.l4.forward(res3)
        res5=self.l5.forward(res4)
        res6=self.l6.forward(res5)
        res7=self.l7.forward(res6)
        res8=self.l8.forward(res7)

        loss, grad = softmax_with_cross_entropy(res8, y)
        #lN=loss
        loss/=N
        grad/=N

        #grad*=0.3

        #loss_reg8, grad_reg8 = l2_regularization(self.l8.W.value, self.reg)
        #loss_reg8 /= self.l8.W.value.size
        #grad_reg8 /= self.l8.W.value.size

        #loss += loss_reg8

        grad8=self.l8.backward(grad)
        grad7=self.l7.backward(grad8)
        grad6=self.l6.backward(grad7)
        grad5=self.l5.backward(grad6)
        grad4=self.l4.backward(grad5)
        grad3=self.l3.backward(grad4)
        grad2= self.l2.backward(grad3)
        grad1= self.l1.backward(grad2)


        #self.l8.params()['W'].value-=(self.l8.params()['W'].grad)
        #self.l8.params()['B'].value-=self.l8.params()['B'].grad

        #self.l4.params()['W'].value-=(self.l4.params()['W'].grad)
        #self.l4.params()['B'].value-=self.l4.params()['B'].grad

        #self.l1.params()['W'].value-=(self.l1.params()['W'].grad)
        #self.l1.params()['B'].value-=self.l1.params()['B'].grad
        return loss

    def predict(self, X):
        result = np.zeros(X.shape[0], np.int)
        res1= self.l1.forward(X)
        res2= self.l2.forward(res1)
        res3=self.l3.forward(res2)
        res4=self.l4.forward(res3)
        res5=self.l5.forward(res4)
        res6=self.l6.forward(res5)
        res7=self.l7.forward(res6)
        res8=self.l8.forward(res7)


        y_pred=np.zeros(X.shape[0])
        probs = softmax(res8)
        for i in range(X.shape[0]):
            result[i] = np.argmax(probs[i])
        return result

    def params(self):
        result = {
           'W1':self.l1.params()['W'],
           'B1':self.l1.params()['B'],
           'W4':self.l4.params()['W'],
           'B4':self.l4.params()['B'],
           'W8':self.l8.params()['W'],
           'B8':self.l8.params()['B']
           }
        return result










class ConvNetGray:
    """
    Implements a very simple conv net
    Input -> Conv[3x3] -> Relu -> Maxpool[4x4] ->
    Conv[3x3] -> Relu -> MaxPool[4x4] ->
    Flatten -> FC -> Softmax
    """
    def __init__(self, input_shape, n_output_classes, conv1_channels, conv2_channels):
        """
        Initializes the neural network
        Arguments:
        input_shape, tuple of 3 ints - image_width, image_height, n_channels Will be equal to (32, 32, 3)
        n_output_classes, int - number of classes to predict
        conv1_channels, int - number of filters in the 1st conv layer
        conv2_channels, int - number of filters in the 2nd conv layer
        """
        self.l1=ConvolutionalLayerGray(2, 1)
        self.l2= ReLULayer()
        self.l3=MaxPoolingLayerGray(4,4)
        self.l4=ConvolutionalLayerGray(2, 1)
        self.l5= ReLULayer()
        self.l6=MaxPoolingLayerGray(4,4)
        self.l7=FlattenerGray()
        self.l8=FullyConnectedLayer(75,4)
        #self.l8=FullyConnectedLayer(6,2)
        self.reg=1e-1


    def compute_loss_and_gradients(self, X, y):
        """
        Computes total loss and updates parameter gradients
        on a batch of training examples
        Arguments:
        X, np array (batch_size, height, width, input_features) - input data
        y, np array of int (batch_size) - classes
        """
        # Before running forward and backward pass through the model,
        # clear parameter gradients aggregated from the previous pass
        # TODO Compute loss and fill param gradients
        # Don't worry about implementing L2 regularization, we will not
        # need it in this assignment
        N=X.shape[0]
        res1= self.l1.forward(X)
        res2= self.l2.forward(res1)
        res3=self.l3.forward(res2)
        res4=self.l4.forward(res3)
        res5=self.l5.forward(res4)
        res6=self.l6.forward(res5)
        res7=self.l7.forward(res6)
        res8=self.l8.forward(res7)

        loss, grad = softmax_with_cross_entropy(res8, y)
        #lN=loss
        loss/=N
        grad/=N

        #grad*=0.3

        #loss_reg8, grad_reg8 = l2_regularization(self.l8.W.value, self.reg)
        #loss_reg8 /= self.l8.W.value.size
        #grad_reg8 /= self.l8.W.value.size

        #loss += loss_reg8

        grad8=self.l8.backward(grad)
        grad7=self.l7.backward(grad8)
        grad6=self.l6.backward(grad7)
        grad5=self.l5.backward(grad6)
        grad4=self.l4.backward(grad5)
        grad3=self.l3.backward(grad4)
        grad2= self.l2.backward(grad3)
        grad1= self.l1.backward(grad2)


        #self.l8.params()['W'].value-=(self.l8.params()['W'].grad)
        #self.l8.params()['B'].value-=self.l8.params()['B'].grad

        #self.l4.params()['W'].value-=(self.l4.params()['W'].grad)
        #self.l4.params()['B'].value-=self.l4.params()['B'].grad

        #self.l1.params()['W'].value-=(self.l1.params()['W'].grad)
        #self.l1.params()['B'].value-=self.l1.params()['B'].grad
        return loss

    def predict(self, X):
        result = np.zeros(X.shape[0], np.int)
        res1= self.l1.forward(X)
        res2= self.l2.forward(res1)
        res3=self.l3.forward(res2)
        res4=self.l4.forward(res3)
        res5=self.l5.forward(res4)
        res6=self.l6.forward(res5)
        res7=self.l7.forward(res6)
        res8=self.l8.forward(res7)


        y_pred=np.zeros(X.shape[0])
        probs = softmax(res8)
        for i in range(X.shape[0]):
            result[i] = np.argmax(probs[i])
        return result

    def params(self):
        result = {
           'W1':self.l1.params()['W'],
           'B1':self.l1.params()['B'],
           'W4':self.l4.params()['W'],
           'B4':self.l4.params()['B'],
           'W8':self.l8.params()['W'],
           'B8':self.l8.params()['B']
           }
        return result










class ConvNet2:
    """
    Implements a very simple conv net

    Input -> Conv[3x3] -> Relu -> Maxpool[4x4] ->
    Conv[3x3] -> Relu -> MaxPool[4x4] ->
    Flatten -> FC -> Softmax
    """
    def __init__(self, input_shape, n_output_classes, conv1_channels, conv2_channels):
        """
        Initializes the neural network

        Arguments:
        input_shape, tuple of 3 ints - image_width, image_height, n_channels
                                         Will be equal to (32, 32, 3)
        n_output_classes, int - number of classes to predict
        conv1_channels, int - number of filters in the 1st conv layer
        conv2_channels, int - number of filters in the 2nd conv layer
        """
        self.l1=ConvolutionalLayer(3, 3,3, 1)
        self.l2= ReLULayer()
        self.l3=MaxPoolingLayer(4,4)        
        self.l4=Flattener()
        self.l5=FullyConnectedLayer(192,10)
        self.reg=1e-1

    def compute_loss_and_gradients(self, X, y):
        """
        Computes total loss and updates parameter gradients
        on a batch of training examples
        Arguments:
        X, np array (batch_size, height, width, input_features) - input data
        y, np array of int (batch_size) - classes
        """
        # Before running forward and backward pass through the model,
        # clear parameter gradients aggregated from the previous pass
        # TODO Compute loss and fill param gradients
        # Don't worry about implementing L2 regularization, we will not
        # need it in this assignment
        N=X.shape[0]
        res1= self.l1.forward(X)
        res2= self.l2.forward(res1)
        res3=self.l3.forward(res2)
        res4=self.l4.forward(res3)
        res5=self.l5.forward(res4)

        loss, grad = softmax_with_cross_entropy(res5, y)
        lN=loss
        loss/=N
        grad/=N

        #grad*=0.3

        #loss_reg8, grad_reg8 = l2_regularization(self.l8.W.value, self.reg)
        #loss_reg8 /= self.l8.W.value.size
        #grad_reg8 /= self.l8.W.value.size

        #loss += loss_reg8
        grad5=self.l5.backward(grad)
        grad4=self.l4.backward(grad5)
        grad3=self.l3.backward(grad4)
        grad2= self.l2.backward(grad3)
        grad1= self.l1.backward(grad2)

        self.l5.params()['W'].value-=(self.l5.params()['W'].grad)
        self.l5.params()['B'].value-=self.l5.params()['B'].grad

        #self.l4.params()['W'].value-=(self.l4.params()['W'].grad)
        #self.l4.params()['B'].value-=self.l4.params()['B'].grad

        self.l1.params()['W'].value-=(self.l1.params()['W'].grad)
        self.l1.params()['B'].value-=self.l1.params()['B'].grad
        return loss

    def predict(self, X):
        result = np.zeros(X.shape[0], np.int)
        res1= self.l1.forward(X)
        res2= self.l2.forward(res1)
        res3=self.l3.forward(res2)
        res4=self.l4.forward(res3)
        res5=self.l5.forward(res4)


        y_pred=np.zeros(X.shape[0])
        probs = softmax(res5)
        for i in range(X.shape[0]):
            result[i] = np.argmax(probs[i])
        return result

    def params(self):
        result = {
           'W1':self.l1.params()['W'],
           'B1':self.l1.params()['B'],
           #'W4':self.l4.params()['W'],
           #'B4':self.l4.params()['B'],
           'W5':self.l5.params()['W'],
           'B5':self.l5.params()['B']
           }
        return result
