import numpy as np
import matplotlib.pyplot as plt
import methods
from dataset import load_svhn, random_split_train_val, prepare_for_neural_network, load_files
from gradient_check import check_layer_gradient, check_layer_param_gradient, check_model_gradient
from layers import FullyConnectedLayer, ReLULayer, ConvolutionalLayer, MaxPoolingLayer, Flattener
from model import ConvNet
from trainer import Trainer, Dataset
from optim import SGD, MomentumSGD
from metrics import multiclass_accuracy, multiclass_accuracy_with_log
import os
import cv2

data_size = 100000
train_X=np.ndarray((0, 16,16, 3))
train_y=np.ndarray((0), int)
test_X=np.ndarray((0, 16,16, 3))
test_y = np.ndarray((0), int)

path="D:/bg/screenshots/debug/16x16_b/"
dirs = os.listdir(path)
for i in range(len( dirs)):
    f=dirs[i]
    y=int(f)
    tX, ty, vX, vy = load_files(path+f, y)
    train_X=np.append(train_X, tX, axis=0)
    train_y=np.append(train_y, ty, axis=0)
    test_X=np.append(test_X, vX, axis=0)
    test_y=np.append(test_y, vy, axis=0)

noutput = 15#train_y.max()+1
train_X, test_X = prepare_for_neural_network(train_X, test_X)
# Split train into train and val
train_X, train_y, val_X, val_y = random_split_train_val(train_X, train_y, num_val = 20)

dataset = Dataset(train_X[:data_size], train_y[:data_size], val_X[:data_size], val_y[:data_size], test_X[:data_size], test_y[:data_size])
# TODO: Change any hyperparamers or optimizators to reach 1.0 training accuracy in 50 epochs or less
# Hint: If you have hard time finding the right parameters manually, try grid search or random search!
#learning_rates = [0.9,0.5,0.3,0.1,1e-2, 1e-3, 1e-4, 1e-5, 1e-6]
#learning_rate_decays = [ 1,0.99,0.9,0.5,0.3,0.1]
model = ConvNet(input_shape=(16,16,3), n_output_classes=noutput, conv1_channels=2, conv2_channels=2)
trainer = Trainer(model, dataset, MomentumSGD(), learning_rate=0.3, num_epochs=50000, batch_size=500,learning_rate_decay=0.99)
loss_history, train_history, val_history = trainer.fit()
accuracy = multiclass_accuracy_with_log(model.predict(test_X), test_y)
print(f"final accuracy: {accuracy}" )
print('end...')




