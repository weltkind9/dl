import numpy as np
import matplotlib.pyplot as plt
import methods
from dataset import load_svhn, random_split_train_val, prepare_for_neural_network
from gradient_check import check_layer_gradient, check_layer_param_gradient, check_model_gradient
from layers import FullyConnectedLayer, ReLULayer, ConvolutionalLayer, MaxPoolingLayer, Flattener
from model import ConvNet, ConvNet2, ConvNetGray
from trainer import Trainer, Dataset
from optim import SGD, MomentumSGD
from metrics import multiclass_accuracy
import os
import cv2
import random
## TODO: Implement ConvolutionaLayer that supports only 1 output and input channel

## Note: now you're working with images, so X is 4-dimensional tensor of
## (batch_size, height, width, channels)

#X = np.array([
#              [
#               [[1.0], [2.0]],
#               [[0.0], [-1.0]]
#              ]
#              ,
#              [
#               [[0.0], [1.0]],
#               [[-2.0], [-1.0]]
#              ]
#             ])

## Batch of 2 images of dimensions 2x2 with a single channel
#print("Shape of X:",X.shape)

#layer = ConvolutionalLayer(in_channels=1, out_channels=1, filter_size=2, padding=0)
#print("Shape of W", layer.W.value.shape)
#layer.W.value = np.zeros_like(layer.W.value)
#layer.W.value[0, 0, 0, 0] = 1.0
#layer.B.value = np.ones_like(layer.B.value)
#result = layer.forward(X)

#assert result.shape == (2, 1, 1, 1)
#assert np.all(result == X[:, :1, :1, :1] +1), "result: %s, X: %s" % (result, X[:, :1, :1, :1])



#result = layer.forward(X)
#d_input = layer.backward(np.ones_like(result))
#assert d_input.shape == X.shape

## Actually test the backward pass
## As usual, you'll need to copy gradient check code from the previous assignment
#layer = ConvolutionalLayer(in_channels=1, out_channels=1, filter_size=2, padding=0)
#assert check_layer_gradient(layer, X)

#layer = ConvolutionalLayer(in_channels=1, out_channels=1, filter_size=2, padding=0)
#assert check_layer_param_gradient(layer, X, 'W')
#layer = ConvolutionalLayer(in_channels=1, out_channels=1, filter_size=2, padding=0)
#assert check_layer_param_gradient(layer, X, 'B')



## Now let's implement multiple output channels
#layer = ConvolutionalLayer(in_channels=1, out_channels=2, filter_size=2, padding=0)
#result = layer.forward(X)
#assert result.shape == (2, 1, 1, 2)


## And now multple input channels!
#X = np.array([
#              [
#               [[1.0, 0.0], [2.0, 1.0]],
#               [[0.0, -1.0], [-1.0, -2.0]]
#              ]
#              ,
#              [
#               [[0.0, 1.0], [1.0, -1.0]],
#               [[-2.0, 2.0], [-1.0, 0.0]]
#              ]
#             ])

#print("Shape of X:", X.shape)
#layer = ConvolutionalLayer(in_channels=2, out_channels=2, filter_size=2, padding=0)
#result = layer.forward(X)
#assert result.shape == (2, 1, 1, 2)
## First test - check the shape is right
#layer = ConvolutionalLayer(in_channels=2, out_channels=2, filter_size=2, padding=0)
#result = layer.forward(X)
#d_input = layer.backward(np.ones_like(result))
#assert d_input.shape == X.shape

## Actually test the backward pass
## As usual, you'll need to copy gradient check code from the previous assignment
#layer = ConvolutionalLayer(in_channels=2, out_channels=2, filter_size=2, padding=0)
#assert check_layer_gradient(layer, X)

#layer = ConvolutionalLayer(in_channels=2, out_channels=2, filter_size=2, padding=0)
#assert check_layer_param_gradient(layer, X, 'W')
#layer = ConvolutionalLayer(in_channels=2, out_channels=2, filter_size=2, padding=0)
#assert check_layer_param_gradient(layer, X, 'B')

#layer = ConvolutionalLayer(in_channels=2, out_channels=2, filter_size=3, padding=1)
#result = layer.forward(X)
## Note this kind of layer produces the same dimensions as input
#assert result.shape == X.shape,"Result shape: %s - Expected shape %s" % (result.shape, X.shape)
#d_input = layer.backward(np.ones_like(result))
#assert d_input.shape == X.shape
#layer = ConvolutionalLayer(in_channels=2, out_channels=2, filter_size=3, padding=1)
#assert check_layer_gradient(layer, X)

#layer = ConvolutionalLayer(in_channels=2, out_channels=2, filter_size=3, padding=1)
#assert check_layer_param_gradient(layer, X, 'W')
#layer = ConvolutionalLayer(in_channels=2, out_channels=2, filter_size=3, padding=1)
#assert check_layer_param_gradient(layer, X, 'B')

#pool = MaxPoolingLayer(2, 2)
#result = pool.forward(X)
#assert result.shape == (2, 1, 1, 2)

#assert check_layer_gradient(pool, X)

#flattener = Flattener()
#result = flattener.forward(X)
#assert result.shape == (2,8)

#assert check_layer_gradient(flattener, X)

## TODO: In model.py, implement missed functions function for ConvNet model

## No need to use L2 regularization
##model = ConvNet(input_shape=(32,32,3), n_output_classes=10, conv1_channels=2, conv2_channels=2)
##loss = model.compute_loss_and_gradients(train_X[:2], train_y[:2])

## TODO Now implement backward pass and aggregate all of the params
##check_model_gradient(model, train_X[:2], train_y[:2])
shapes=[]
train_x_list=[]
train_y_list=[]
dataset={
    #"D:/bg/screenshots/dataset/gray/80/1/":1,
    #     "D:/bg/screenshots/dataset/gray/80/2/":2,
    #     "D:/bg/screenshots/dataset/gray/80/3/":3,
    #     "D:/bg/screenshots/dataset/gray/80/4/":4,
    #     "D:/bg/screenshots/dataset/gray/80/5/":5,
         #"D:/bg/screenshots/dataset/gray/80/6/":0,
         #"D:/bg/screenshots/dataset/gray/80/7/":1,
         "D:/bg/screenshots/dataset/gray/80/8/":2,
         "D:/bg/screenshots/dataset/gray/80/9/":3,
     #    "D:/bg/screenshots/dataset/gray/0/":0,
         }

for key in dataset:
    files = os.listdir(key)
    for i in range(len( files)):
        f=files[i]
        img = cv2.imread(key+f, cv2.IMREAD_GRAYSCALE)#
        if not img.shape in shapes: shapes.append(img.shape)
        img2=cv2.resize(img, (90,240))
        train_x_list.append(img2)
        train_y_list.append(dataset[key])

ln=len(train_x_list)
print(ln)
train_X=np.ndarray((ln, 240, 90),np.uint8)#,3
train_y=np.ndarray((ln),np.uint8)
test_X=np.ndarray((ln, 240, 90),np.uint8)#,3
test_y = np.ndarray((ln),np.uint8)
for i in range(ln):
    test_X[i]=train_x_list[i]
    test_y[i]=train_y_list[i]

    rx=random.sample([-5,-4,-3,-2,-1,1,2,3,4,5] ,1)[0]
    ry=random.sample([-5,-4,-3,-2,-1,1,2,3,4,5] ,1)[0]
    M = np.float32([[1,0,rx],[0,1,ry]])
    img2=cv2.warpAffine(train_x_list[i],M,(90,240))
    train_X[i]=img2
    train_y[i]=train_y_list[i]
    #cv2.imshow('sd', img2)
    #cv2.waitKey()
    #cv2.destroyAllWindows()
    #cv2.imshow('sd', train_X[i+j])
    #cv2.waitKey()
    #cv2.destroyAllWindows()
for i in range(ln):
    if train_y[i]!=train_y_list[i]:
        print(i)


#random.shuffle(train_X)
#for i in range(train_X.shape[1]-1,0,-1):
#    cv2.imshow('sd', train_X[i])
#    cv2.waitKey()
#    cv2.destroyAllWindows()

print('data ready...')
#train_X=np.ndarray((96, 26,28,3))
#train_y=np.ndarray((96), int)
#test_X=np.ndarray((2, 26,28,3))
#test_y = np.ndarray((2), int)

#path="D:/coding/dlcourse_ai/dlcourse_ai/assignments/assignment2/chips/55/"
#files = os.listdir(path)
#for i in range(len( files)):
#    f=files[i]
#    img = cv2.imread(path+f)
#    train_X[i]=img#.reshape(img.shape[0]*img.shape[1]*img.shape[2]))
#    train_y[i]=0

#path="D:/coding/dlcourse_ai/dlcourse_ai/assignments/assignment2/chips/66/"
#files = os.listdir(path)
#for i in range(len( files)):
#    f=files[i]
#    img = cv2.imread(path+f)
#    train_X[48+i]=img#.reshape(img.shape[0]*img.shape[1]*img.shape[2]))
#    train_y[48+i]=1

#img = cv2.imread("D:/coding/dlcourse_ai/dlcourse_ai/assignments/assignment2/chips/5.png")
#test_X[0]=img#.reshape(img.shape[0]*img.shape[1]*img.shape[2]))
#test_y[0]=0

#img = cv2.imread("D:/coding/dlcourse_ai/dlcourse_ai/assignments/assignment2/chips/6.png")
#test_X[1]=img#.reshape(img.shape[0]*img.shape[1]*img.shape[2]))
#test_y[1]=1

#train_X, test_X = prepare_for_neural_network(train_X, test_X)
## Split train into train and val
#train_X, train_y, val_X, val_y = random_split_train_val(train_X, train_y, num_val = 2)



data_size = 5000
model = ConvNetGray(input_shape=(32,32,3), n_output_classes=2, conv1_channels=2, conv2_channels=2)
dataset = Dataset(train_X[:data_size], train_y[:data_size], test_X[:data_size], test_y[:data_size], test_X[:data_size], test_y[:data_size])
# TODO: Change any hyperparamers or optimizators to reach 1.0 training accuracy in 50 epochs or less
# Hint: If you have hard time finding the right parameters manually, try grid search or random search!
trainer = Trainer(model, dataset, MomentumSGD(), learning_rate=0.05, num_epochs=1000, batch_size=100,  learning_rate_decay=1)

loss_history, train_history, val_history = trainer.fit()

accuracy = multiclass_accuracy(model.predict(test_X), test_y)
print(f"final accuracy: {accuracy}" )

print("end...")





