import sys
import os

def work(count, black, arr):
    max_sum = arr[0]
    for start in range(count):
        for end in range(start, start + count):
            curr_sum=0
            for i in range(start, end+1):
                ind = i % count
                if ind == black:
                    curr_sum=0
                    break
                #print(f'ind {ind}')
                curr_sum += arr[ind]
            #print(f'[{start}:{end}] {curr_sum}')
            if curr_sum > max_sum:
                max_sum=curr_sum
    print(max_sum)
txt ="""2\r\n
8 2\r\n
2 3 4 5 -30 6 -1 2\r\n
6 -1\r\n
1 -3 2 -2 3 4\r\n
"""
if __name__ == "__main__":
    print(f'argcomm ({len(sys.argv)}): {sys.argv}')
    for i in range(len(sys.argv)):
        print(sys.argv[i])
    #print(sys.argv)
    data = txt.split(os.linesep)
    for i in range(0, len(data), 2):
        f=data[i].split(' ')
        s=data[i+1].split()
        arr=()
        for a in s:
            arr.add(int(a))
        work(int(f[0]), int(f[1]), arr)

    #count=6
    #black = -1
    #arr=(1,-3,2,-2,3,4)
    #count=8
    #black = 2
    #arr=(2,3,4,5,-30,6,-1,2)




work(8,8,(2,3,4,5,-30,6,-1,2))
work(6,-1,(1,-3,2,-2,3,4))