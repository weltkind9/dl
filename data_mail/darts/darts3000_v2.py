import sys
import os

def work(count, black, arr):
    max_sum = arr[0]
    for start in range(count):
        for end in range(start, start + count):
            curr_sum=0
            for i in range(start, end+1):
                ind = i % count
                if ind == black:
                    curr_sum=0
                    break
                #print(f'ind {ind}')
                curr_sum += arr[ind]
            #print(f'[{start}:{end}] {curr_sum}')
            if curr_sum > max_sum:
                max_sum=curr_sum
    print(max_sum)
txt ="""2\r
8 2\r
2 3 4 5 -30 6 -1 2\r
6 -1\r
1 -3 2 -2 3 4"""
inp = txt.split(sys.argv[1])
data_count = int(inp[0])#int(sys.argv[0])
for i in range(1, len(inp), 2):
    frst=inp[i].split(' ')
    scnd=inp[i+1].split(' ')
    prms=[]
    for a in scnd:
        prms.append(int(a))
    work(int(frst[0]), int(frst[1]), prms)


#work(8,2,(2,3,4,5,-30,6,-1,2))
#work(6,-1,(1,-3,2,-2,3,4))