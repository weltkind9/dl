import sys

def maxSubArraySum(a, size):       
    max_so_far = -99999999999999999999999
    max_ending_here = 0       
    for i in range(0, size): 
        max_ending_here = max_ending_here + a[i] 
        if (max_so_far < max_ending_here): 
            max_so_far = max_ending_here 
  
        if max_ending_here < 0: 
            max_ending_here = 0 
    if max_so_far < 0:
        max_so_far=0
    print(int(max_so_far))
    return max_so_far

def kadane(a): 
    n = len(a) 
    max_so_far = 0
    max_ending_here = 0
    for i in range(0, n): 
        max_ending_here = max_ending_here + a[i] 
        if (max_ending_here < 0): 
            max_ending_here = 0
        if (max_so_far < max_ending_here): 
            max_so_far = max_ending_here 
    return max_so_far 
  
def maxCircularSum(a): 
    n = len(a) 
    max_kadane = kadane(a) 
    max_wrap = 0
    for i in range(0, n): 
        max_wrap += a[i] 
        a[i] = -a[i] 
    max_wrap = max_wrap + kadane(a) 
    if max_wrap > max_kadane: 
        res = max_wrap 
    else: 
        res = max_kadane
    print(res)
    return res
  
data_count=int(input())
for i in range(data_count):
    frst=input().split(' ')
    scnd=input().split(' ')

    prms=[]
    for a in scnd:
        prms.append(int(a))
    black = int(frst[1])

    if black >0:
        prms = prms[black+1:] + prms[:black]
        maxSubArraySum(prms, len(prms))
    else:
        maxCircularSum(prms)    
