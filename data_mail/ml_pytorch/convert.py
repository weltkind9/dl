import decimal
import numpy as np
# create a new context for this task
ctx = decimal.Context()

# 20 digits should be enough for everyone :D
ctx.prec = 20

def float_to_str(f):
    """
    Convert the given float to a string,
    without resorting to scientific notation
    """
    d1 = ctx.create_decimal(repr(f))
    return format(d1, 'f')

def save_csv2(fileName, arr):
    file = open(fileName, 'w')
    for a in arr:
        file.write(float_to_str(round(a, 8)))
    file.close()

arr=np.ndarray((2000), np.float)
i=0
with open('result/result_04.08.2020_15.37.31.csv') as csvfile:
    spamreader = csv.reader(csvfile)
    for row in spamreader:
        arr[i]=np.array(row)
        i+=1

save_csv2('result/result_04.08.2020_15.37.31_convert.csv', Y)