import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data.sampler import SubsetRandomSampler, Sampler
import numpy as np
from torchvision import transforms
from sklearn.metrics import roc_auc_score
import matplotlib.pyplot as plt
import Task1_MADE_Dataset
import csv
import datetime
from methods import compute_accuracy
import Flattener


def save_csv(file, arr):
    i=0
    with open(file, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter='\n')
        spamwriter.writerow(arr)

def load_scv(csv_fileX, data_count, feature_count):
    X=np.ndarray((data_count, feature_count), np.float)
    i=0
    with open(csv_fileX) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            X[i]=np.array(row, np.float)
            i+=1
    return torch.FloatTensor(X)
# First, lets load the dataset
data_train = Task1_MADE_Dataset.Task1_MADE_Dataset(10000, 30, 'data/train.csv', 'data/train-target.csv',
                       transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize(mean=[0.43,0.44,0.47],
                                               std=[0.20,0.20,0.20])                           
                       ])
                      )

batch_size = 500

data_size = data_train.X.shape[0]
validation_split = .2
split = int(np.floor(validation_split * data_size))
indices = list(range(data_size))
np.random.shuffle(indices)

train_indices, val_indices = indices[split:], indices[:split]

train_sampler = SubsetRandomSampler(train_indices)
val_sampler = SubsetRandomSampler(val_indices)

train_loader = torch.utils.data.DataLoader(data_train, batch_size=batch_size, sampler=train_sampler)
val_loader = torch.utils.data.DataLoader(data_train, batch_size=batch_size, sampler=val_sampler)

nn_model = nn.Sequential(
            Flattener.Flattener(),
            nn.Linear(30, 100),
            nn.ReLU(inplace=True),
            nn.Linear(100, 2),
            nn.Softmax(1)
         )
nn_model.type(torch.FloatTensor)

# We will minimize cross-entropy between the ground truth and
# network predictions using an SGD optimizer
loss = nn.CrossEntropyLoss().type(torch.FloatTensor)
optimizer = optim.SGD(nn_model.parameters(), lr=1e-2, weight_decay=1e-1)

def train_model(model, train_loader, val_loader, loss, optimizer, num_epochs):    
    loss_history = []
    train_history = []
    val_history = []
    for epoch in range(num_epochs):
        model.train() # Enter train mode
        
        loss_accum = 0
        train_accuracies=[]
        for i_step, (x, y) in enumerate(train_loader):
            prediction = model(x)    
            loss_value = loss(prediction, y)
            optimizer.zero_grad()
            loss_value.backward()
            optimizer.step()
            train_accuracy= roc_auc_score(y, prediction.detach()[:,1:])
            train_accuracies.append(train_accuracy)
            
            loss_accum += loss_value

        ave_loss = loss_accum / (i_step + 1)
        train_accuracy = np.mean(train_accuracies)
        val_accuracy = compute_accuracy(model, val_loader)
        
        loss_history.append(float(ave_loss))
        train_history.append(train_accuracy)
        val_history.append(val_accuracy)
        
        print("%i Average loss: %f, Train accuracy: %f, Val accuracy: %f" % (epoch, ave_loss, train_accuracy, val_accuracy))
        if train_accuracy > 0.99:
            break
    return loss_history, train_history, val_history

loss_history, train_history, val_history = train_model(nn_model, train_loader, val_loader, loss, optimizer, 500)
test= load_scv('data/test.csv',2000,30);
prediction = nn_model(test)
result =[]
for i in prediction[:,1:]:
    result.append(i.item())
save_csv(f'data/result_{datetime.datetime.now().strftime("%d.%m.%Y_%H.%M.%S")}.csv', result)

input('done..')