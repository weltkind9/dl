import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data.sampler import SubsetRandomSampler, Sampler
import numpy as np
from torchvision import transforms
from sklearn.metrics import roc_auc_score
import matplotlib.pyplot as plt
import Dataset_feature_selection
import csv
import datetime
import Flattener
from methods import compute_accuracy
import collections
import time
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2,  f_classif
import decimal

# create a new context for this task
ctx = decimal.Context()

# 20 digits should be enough for everyone :D
ctx.prec = 20

def float_to_str(f):
    """
    Convert the given float to a string,
    without resorting to scientific notation
    """
    d1 = ctx.create_decimal(repr(f))
    return format(d1, 'f')

def save_csv(file, arr):
    with open(file, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter='\n')
        spamwriter.writerow(np.round(arr, 8))

def save_csv2(fileName, arr):
    file = open(fileName, 'w')
    for a in arr:
        file.write(float_to_str(round(a, 8))+'\n')
    file.close()


def train_model(model, train_loader, val_loader, loss, optimizer, num_epochs, msg):    
    loss_history = []
    train_history = []
    val_history = []mnkjmnmn 
            nn.Softmax(1)
         )
    nn_model.type(torch.FloatTensor)

    loss = nn.CrossEntropyLoss().type(torch.FloatTensor)
    optimizer = optim.SGD(nn_model.parameters(), lr=1e-2, weight_decay=1e-1)

    train_accuracy, val_accuracy = train_model(nn_model, train_loader, val_loader, loss, optimizer, 5000, msg)
    tt = torch.FloatTensor(test)
    prediction = nn_model(tt)
    result =[]
    for i in prediction[:,1:]:
        result.append(i.item())
    save_csv2(f'result_300_300_300_300_3/result_{msg}_{datetime.datetime.now().strftime("%d.%m.%Y_%H.%M.%S")}.csv', result)

    return train_accuracy, val_accuracy

  

def start_learn(X, Y, testX, featureK, trans):
    # feature extraction
    test = SelectKBest(score_func= f_classif, k=featureK)
    fit = test.fit(X, Y)
    # summarize scores
    #np.set_printoptions(precision=3)
    #print(fit.scores_)
    features = fit.transform(X)
    # summarize selected features
    #print(features[0:featureK+1,:])
    ind=[]
    for i in range(feature_count):
        for j in range(featureK):
            if X[0][i] == features[0][j]:
                ind.append(i)

    tX=testX[:,ind]
    train_accuracy, val_accuracy = learn_and_write_result(features,Y, tX, f'{featureK}')
    trans.append(transition(k=featureK, t_acc= round(train_accuracy,3), v_acc=round(val_accuracy,3)))

trans=[]
t1=time.time()
for i in range(30, 23, -1):
    start_learn(X, Y, testX, i, trans)

t2 = time.time()
print(f'time: {round(t2-t1)} sec')
s = ''
for t in trans:
    s+=str(t)+'\n'
file = open(f'result_300_300_300_300_3/stat_{datetime.datetime.now().strftime("%d.%m.%Y_%H.%M.%S")}.txt', 'w')
file.write(s)
file.close()

input('done....')
