import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data.sampler import SubsetRandomSampler, Sampler
import numpy as np
from torchvision import transforms
from sklearn.metrics import roc_auc_score
import matplotlib.pyplot as plt
import Dataset_feature_selection
import csv
import datetime
import Flattener
from methods import compute_accuracy
import collections
import time
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2,  f_classif
import decimal

# create a new context for this task
ctx = decimal.Context()

# 20 digits should be enough for everyone :D
ctx.prec = 20

def float_to_str(f):
    """
    Convert the given float to a string,
    without resorting to scientific notation
    """
    d1 = ctx.create_decimal(repr(f))
    return format(d1, 'f')

def save_csv(file, arr):
    with open(file, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter='\n')
        spamwriter.writerow(np.round(arr, 8))

def save_csv2(fileName, arr):
    file = open(fileName, 'w')
    for a in arr:
        file.write(float_to_str(round(a, 8))+'\n')
    file.close()


def train_model(model, train_loader, val_loader, loss, optimizer, num_epochs, msg):    
    loss_history = []
    train_history = []
    val_history = []
    for epoch in range(num_epochs):
        model.train() # Enter train mode
        
        loss_accum = 0
        train_accuracies=[]
        for i_step, (x, y) in enumerate(train_loader):
            prediction = model(x)    
            loss_value = loss(prediction, y)
            optimizer.zero_grad()
            loss_value.backward()
            optimizer.step()
            train_accuracy= roc_auc_score(y, prediction.detach()[:,1:])
            train_accuracies.append(train_accuracy)
            
            loss_accum += loss_value

        ave_loss = loss_accum / (i_step + 1)
        train_accuracy = np.mean(train_accuracies)
        val_accuracy = compute_accuracy(model, val_loader)
        
        loss_history.append(float(ave_loss))
        train_history.append(train_accuracy)
        val_history.append(val_accuracy)
        
        print("%i %s Average loss: %f, Train accuracy: %f, Val accuracy: %f" % (epoch, msg, ave_loss, train_accuracy, val_accuracy))
        if train_accuracy > 0.99:
            break
    file = open(f'result_300_300_300_300_3/history_{msg}_{datetime.datetime.now().strftime("%d.%m.%Y_%H.%M.%S")}.csv', 'w')
    for i in range(len(val_history)):
        file.write(f'tr: {round(train_history[i], 4)} \t\t val: {round(val_history[i], 4)}\n')
    file.close()

    return train_accuracy, val_accuracy


transition = collections.namedtuple("transition", ["k", "t_acc", "v_acc"])

data_count=10000
test_data_count=2000
feature_count=30

X=np.ndarray((data_count, feature_count), np.float)
i=0
with open('data/train.csv') as csvfile:
    spamreader = csv.reader(csvfile)
    for row in spamreader:
        X[i]=np.array(row, np.float)
        i+=1

Y=np.ndarray((data_count), np.float)
i=0
with open('data/train-target.csv') as csvfile:
    spamreader = csv.reader(csvfile)
    for row in spamreader:
        Y[i]=np.array(row)
        i+=1
testX=np.ndarray((test_data_count, feature_count), np.float)
i=0
with open('data/test.csv') as csvfile:
    spamreader = csv.reader(csvfile)
    for row in spamreader:
        testX[i]=np.array(row, np.float)
        i+=1

#maxTrainX= np.max(np.abs(X))
#X /= maxTrainX
#testX /= maxTrainX

def learn_and_write_result(X, Y, test, msg):
    data_train = Dataset_feature_selection.Dataset_feature_selection(X, Y, X.shape[0])
    batch_size = 500
    data_size = data_train.X.shape[0]
    validation_split = .2
    split = int(np.floor(validation_split * data_size))
    indices = list(range(data_size))
    np.random.shuffle(indices)
    train_indices, val_indices = indices[split:], indices[:split]
    train_sampler = SubsetRandomSampler(train_indices)
    val_sampler = SubsetRandomSampler(val_indices)

    train_loader = torch.utils.data.DataLoader(data_train, batch_size=batch_size, sampler=train_sampler)
    val_loader = torch.utils.data.DataLoader(data_train, batch_size=batch_size, sampler=val_sampler)
    feature_count=X.shape[1]
    nn_model = nn.Sequential(
            Flattener.Flattener(),
            nn.Linear(feature_count, 300),
            nn.ReLU(inplace=True),
            nn.Linear(300, 300),
            nn.ReLU(inplace=True),
            nn.Linear(300, 2),
            nn.Softmax(1)
         )
    nn_model.type(torch.FloatTensor)

    loss = nn.CrossEntropyLoss().type(torch.FloatTensor)
    optimizer = optim.SGD(nn_model.parameters(), lr=1e-2, weight_decay=1e-1)

    train_accuracy, val_accuracy = train_model(nn_model, train_loader, val_loader, loss, optimizer, 5000, msg)
    tt = torch.FloatTensor(test)
    prediction = nn_model(tt)
    result =[]
    for i in prediction[:,1:]:
        result.append(i.item())
    save_csv2(f'result_300_300_300_300_3/result_{msg}_{datetime.datetime.now().strftime("%d.%m.%Y_%H.%M.%S")}.csv', result)

    return train_accuracy, val_accuracy

  

def start_learn(X, Y, testX, featureK, trans):
    # feature extraction
    test = SelectKBest(score_func= f_classif, k=featureK)
    fit = test.fit(X, Y)
    # summarize scores
    #np.set_printoptions(precision=3)
    #print(fit.scores_)
    features = fit.transform(X)
    # summarize selected features
    #print(features[0:featureK+1,:])
    ind=[]
    for i in range(feature_count):
        for j in range(featureK):
            if X[0][i] == features[0][j]:
                ind.append(i)

    tX=testX[:,ind]
    train_accuracy, val_accuracy = learn_and_write_result(features,Y, tX, f'{featureK}')
    trans.append(transition(k=featureK, t_acc= round(train_accuracy,3), v_acc=round(val_accuracy,3)))

trans=[]
t1=time.time()
for i in range(30, 23, -1):
    start_learn(X, Y, testX, i, trans)

t2 = time.time()
print(f'time: {round(t2-t1)} sec')
s = ''
for t in trans:
    s+=str(t)+'\n'
file = open(f'result_300_300_300_300_3/stat_{datetime.datetime.now().strftime("%d.%m.%Y_%H.%M.%S")}.txt', 'w')
file.write(s)
file.close()

input('done....')