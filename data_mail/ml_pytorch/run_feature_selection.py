import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data.sampler import SubsetRandomSampler, Sampler
import numpy as np
from torchvision import transforms
from sklearn.metrics import roc_auc_score
import matplotlib.pyplot as plt
import Dataset_feature_selection
import csv
import datetime
import Flattener
from methods import compute_accuracy
import collections
import time

def train_model(model, train_loader, val_loader, loss, optimizer, num_epochs, msg):    
    loss_history = []
    train_history = []
    val_history = []
    for epoch in range(num_epochs):
        model.train() # Enter train mode
        
        loss_accum = 0
        train_accuracies=[]
        for i_step, (x, y) in enumerate(train_loader):
            prediction = model(x)    
            loss_value = loss(prediction, y)
            optimizer.zero_grad()
            loss_value.backward()
            optimizer.step()
            train_accuracy= roc_auc_score(y, prediction.detach()[:,1:])
            train_accuracies.append(train_accuracy)
            
            loss_accum += loss_value

        ave_loss = loss_accum / (i_step + 1)
        train_accuracy = np.mean(train_accuracies)
        val_accuracy = compute_accuracy(model, val_loader)
        
        loss_history.append(float(ave_loss))
        train_history.append(train_accuracy)
        val_history.append(val_accuracy)
        
        print("%i %s Average loss: %f, Train accuracy: %f, Val accuracy: %f" % (epoch, msg, ave_loss, train_accuracy, val_accuracy))
        if train_accuracy > 0.99:
            break
    return train_accuracy, val_accuracy



data_count=10000
feature_count=30
X=np.ndarray((data_count, feature_count), np.float)
i=0
with open('data/train.csv') as csvfile:
    spamreader = csv.reader(csvfile)
    for row in spamreader:
        X[i]=np.array(row, np.float)
        i+=1

Y=np.ndarray((data_count), np.float)
i=0
with open('data/train-target.csv') as csvfile:
    spamreader = csv.reader(csvfile)
    for row in spamreader:
        Y[i]=np.array(row)
        i+=1

def learn(X, Y, msg):
    data_train = Dataset_feature_selection.Dataset_feature_selection(X, Y, X.shape[0])
    batch_size = 500
    data_size = data_train.X.shape[0]
    validation_split = .2
    split = int(np.floor(validation_split * data_size))
    indices = list(range(data_size))
    np.random.shuffle(indices)
    train_indices, val_indices = indices[split:], indices[:split]
    train_sampler = SubsetRandomSampler(train_indices)
    val_sampler = SubsetRandomSampler(val_indices)

    train_loader = torch.utils.data.DataLoader(data_train, batch_size=batch_size, sampler=train_sampler)
    val_loader = torch.utils.data.DataLoader(data_train, batch_size=batch_size, sampler=val_sampler)
    feature_count=X.shape[1]
    nn_model = nn.Sequential(Flattener.Flattener(),nn.Linear(feature_count, 100),nn.ReLU(inplace=True),nn.Linear(100, 2),nn.Softmax(1))
    nn_model.type(torch.FloatTensor)

    loss = nn.CrossEntropyLoss().type(torch.FloatTensor)
    optimizer = optim.SGD(nn_model.parameters(), lr=1e-2, weight_decay=1e-1)

    train_accuracy, val_accuracy = train_model(nn_model, train_loader, val_loader, loss, optimizer, 1, msg)
    return train_accuracy, val_accuracy


transition = collections.namedtuple("transition", ["step", "t_acc", "v_acc"])
step = collections.namedtuple("step", ["d", "i"])
  

def start_learn(X, Y, depth, max_depth, trans):
    list_ind=[]
    for i in range(X.shape[1]):
        x=np.delete(X, i, axis = 1)
        train_accuracy, val_accuracy = learn(x,Y, f'd:{depth}; i:{i}')
        list_ind.append(i)
        trans.append(transition(step=step( d= depth, i = np.array( list_ind)), t_acc=round(train_accuracy,3), v_acc=round(val_accuracy,3)))
       
        if depth < max_depth:
            start_learn(x,Y, depth+1, max_depth, trans)


trans = []
t1=time.time()
start_learn(X, Y, 0, 1, trans)
t2 = time.time()
print(f'time: {round(t2-t1)} sec')
s = ''
for t in trans:
    s+=str(t)+'\n'
file = open(f'data/result_{datetime.datetime.now().strftime("%d.%m.%Y_%H.%M.%S")}.txt', 'w')
file.write(s)
file.close()
print(s)
print(trans)
input('done....')