from torch.utils.data import Dataset
import csv
import numpy as np
import torch

class Task1_MADE_Dataset(Dataset):
    def __init__(self, data_count, feature_count, csv_fileX, csv_fileY, transform=None):        
        self.data_count=data_count
        X=np.ndarray((data_count, feature_count), np.float)
        i=0
        with open(csv_fileX) as csvfile:
            spamreader = csv.reader(csvfile)
            for row in spamreader:
                X[i]=np.array(row, np.float)
                i+=1
        self.X=torch.FloatTensor(X)
        Y=np.ndarray((data_count), np.float)
        i=0
        with open(csv_fileY) as csvfile:
            spamreader = csv.reader(csvfile)
            for row in spamreader:
                Y[i]=np.array(row)
                i+=1
        self.Y=torch.LongTensor(Y)
        self.transform = transform

    def __len__(self):
        return self.data_count

    def __getitem__(self, idx):

        return self.X[idx], self.Y[idx]