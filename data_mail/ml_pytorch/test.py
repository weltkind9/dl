import torch.nn as nn
import torch
import numpy as np
import sklearn
from sklearn.feature_selection import VarianceThreshold

from sklearn.datasets import make_classification

x_data_generated, y_data_generated = make_classification()

print(x_data_generated.shape)

print(VarianceThreshold(.7).fit_transform(x_data_generated, y_data_generated).shape)

print(VarianceThreshold(.8).fit_transform(x_data_generated, y_data_generated).shape)

print(VarianceThreshold(.9).fit_transform(x_data_generated, y_data_generated).shape)
print(x_data_generated.shape)

import csv
from sklearn.feature_selection import SelectKBest

from sklearn.feature_selection import chi2

data_count=10000
test_data_count=2000
feature_count=30

X=np.ndarray((data_count, feature_count), np.float)
i=0
with open('data/train.csv') as csvfile:
    spamreader = csv.reader(csvfile)
    for row in spamreader:
        X[i]=np.array(row, np.float)
        i+=1
Y=np.ndarray((data_count), np.float)
i=0
with open('data/train-target.csv') as csvfile:
    spamreader = csv.reader(csvfile)
    for row in spamreader:
        Y[i]=np.array(row)
        i+=1

testX=np.ndarray((test_data_count, feature_count), np.float)
i=0
with open('data/test.csv') as csvfile:
    spamreader = csv.reader(csvfile)
    for row in spamreader:
        testX[i]=np.array(row, np.float)
        i+=1

minTrainX= np.min(X)
X -= minTrainX
testX -= minTrainX


# feature extraction
featureK=15
test = SelectKBest(score_func=chi2, k=featureK)

fit = test.fit(X, Y)

# summarize scores

np.set_printoptions(precision=3)

print(fit.scores_)

features = fit.transform(X)

# summarize selected features

print(features[0:featureK+1,:])
ind=[]
for i in range(feature_count):
    for j in range(featureK):
        if X[0][i] == features[0][j]:
            ind.append((i,j))

ind1=[]
for i in range(feature_count):
    for j in range(featureK):
        if X[10][i] == features[10][j]:
            ind1.append((i,j))

ind2=[]
for i in range(feature_count):
    for j in range(featureK):
        if X[20][i] == features[20][j]:
            ind2.append((i,j))
input('done..')