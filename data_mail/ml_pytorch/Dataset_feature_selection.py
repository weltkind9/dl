from torch.utils.data import Dataset
import csv
import numpy as np
import torch

class Dataset_feature_selection(Dataset):
    def __init__(self, X, Y, data_count):        
        self.data_count=data_count
        self.X=torch.FloatTensor(X)
        self.Y=torch.LongTensor(Y)

    def __len__(self):
        return self.data_count

    def __getitem__(self, i):
        return self.X[i], self.Y[i]
