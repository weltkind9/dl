from sklearn.metrics import roc_auc_score
import numpy as np

def compute_accuracy(model, loader):
    """
    Computes accuracy on the dataset wrapped in a loader
    
    Returns: accuracy as a float value between 0 and 1
    """
    model.eval() # Evaluation mode
    # TODO: Implement the inference of the model on all of the batches from loader,
    #       and compute the overall accuracy.
    # Hint: PyTorch has the argmax function!
    scores=[]
    for i_step, (x, y) in enumerate(loader):
        prediction = model(x)  
        score = roc_auc_score(y, prediction.detach()[:,1:])
        scores.append(score)
    return np.mean(scores)

