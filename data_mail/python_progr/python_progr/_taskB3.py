import random
import time
def find_all_pathX(start):
    path = []
    if not start in rightX.keys():
        return []
    firstX = rightX[start]
    for x1 in firstX:
        if not x1 in downY.keys():
            continue
        firstY = downY[x1]
        for y1 in firstY:
            if not y1 in leftX.keys():
                continue
            secondX = leftX[y1]
            for x2 in secondX:
                if not x2 in upY.keys():
                    continue
                secondY = upY[x2]
                for y2 in secondY:
                    if y2 == start:
                        path.append([start, x1,y1,x2,y2])
    return path

data_count = 1#int(input())
for i in range(data_count):
    dot_count = 2000#int(input())
    dots = []
    rightX = {}
    leftX = {}
    downY = {}
    upY = {}
    for j in range(dot_count):
        dt = list((random.randint(-10,10 ) , random.randint(-10,10))) #list(map(int, input().split(' ')))
        dots.append(dt)
    #dots = [[1,7], [4,7], [6,7], [1,5], [4,5], [6,5], [3,3],[1,1], [4,1]]
    t1=time.time()
    print(t1)
    for i in range(len(dots)):
        for j in range(i+1, len(dots)):
            if abs(dots[i][0] - dots[j][0])==0:
               if dots[i][1] < dots[j][1]:
                   if not i in upY.keys():upY[i]=[]
                   if not j in downY.keys():downY[j]=[]
                   upY[i].append(j)
                   downY[j].append(i)
               else:
                   if not i in downY.keys():downY[i]=[]
                   if not j in upY.keys():upY[j]=[]
                   downY[i].append(j)
                   upY[j].append(i)

            if abs(dots[i][1] - dots[j][1])==0:
               if dots[i][0] < dots[j][0]:
                   if not i in rightX.keys():rightX[i]=[]
                   if not j in leftX.keys():leftX[j]=[]
                   rightX[i].append(j)
                   leftX[j].append(i)
               else:
                   if not i in leftX.keys():leftX[i]=[]
                   if not j in rightX.keys():rightX[j]=[]
                   leftX[i].append(j)
                   rightX[j].append(i)
            
    t2=time.time()
    print(t2-t1)        
    count=0
    for i in range(len(dots)):
        x = find_all_pathX(i)
        count+=len(x)
    print(count)
    t3=time.time()
    print(t3-t2)        
