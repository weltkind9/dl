#import numpy as np
import time
class Fraction():
    def __init__(self, chisl, znam):
        if znam<0:
            chisl *= -1
            znam *= -1
        self.chisl=chisl
        self.znam=znam
        #self.normalize()

    def __add__(self, other):
        chisl = self.chisl * other.znam + other.chisl * self.znam
        znam = self.znam * other.znam
        return Fraction(chisl, znam)

    def __mul__(self, other):
        chisl = self.chisl * other.chisl
        znam = self.znam * other.znam
        return Fraction(chisl, znam)

    def __str__(self):
        self.normalize()
        return f'{self.chisl} {self.znam}'
    
    def normalize(self):
        if self.znam == 1: return
        i=2
        while abs(self.chisl) % 2 ==0 and self.znam % 2 == 0:
            self.chisl = self.chisl // i
            self.znam = self.znam // i

half = Fraction(1,2)
dp = []
for i in range(64):
    dp.append([])
    for j in range(i+1):
        dp[i].append(None)
for i in range(64):
    for j in range(i + 1):
        if i == 0:
            dp[0][0] = Fraction(1,1)
        elif i == 1:
            dp[1][0] = Fraction(1,2)
            dp[1][1] = Fraction(1,2)
        elif j == 0:
            dp[i][j] = dp[i-1][0] * half
        elif i==j:
            dp[i][j] = dp[i-1][i-1] * half
        else:
            dp[i][j] = half * dp[i-1][j-1] + half * dp[i-1][j]
            dp[i][j].normalize()


data_count = int(input().strip())
for _ in range(data_count):
    h = int(input())
    sum = Fraction(0, 1)
    counter = 0
    for i in range(h):
        cells = list(map(int, input().split(' ')))
        for j in range(len(cells)):
            f = Fraction(cells[j], 1)
            w = dp[i][j]
            sum += w * f
            if counter > 1000:
                sum.normalize()
                counter = 0
            counter += 1
    print(sum)

#for i in range(10):
#    ff = list(map(int, input().split(' ')))
#    f1 = Fraction(ff[0], ff[1])
#    print(f'f1: {f1}')

#    ff = list(map(int, input().split(' ')))
#    f2 = Fraction(ff[0], ff[1])
#    print(f'f2: {f2}')

#    f3 = f1+f2
#    print(f'plus: {f3}')

#    f4 = f1*f2
#    print(f'mul: {f4}')