#Объединяем ближайшие вектора пользователей и баннеров в один и присваиваем новые ид
import csv
import collections
import numpy as np
import pickle

dot = collections.namedtuple("dot", ["id", "X", "near_dots"])
train_row = collections.namedtuple("train_row", ["user_id", "item_id", "like"])
def load_scv_dict(csv_file):
    X={}
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0: i+=1; continue
            X[int(row[0])] = np.array(row[1:], np.float)
    return X

def load_train(csv_file):
    X=[]
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            X.append(train_row(user_id=int(row[0]), item_id = int(row[1]), like = int(row[2])))
    return X

def load_scv_list(csv_file):
    X=[]
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            X.append(dot(id= int(row[0]), X= np.array(row[1:], np.float), near_dots=[]) )
    return X
items_dict_orig = load_scv_dict('data/item-features.csv')
users_dict_orig = load_scv_dict('data/user-features.csv')


items = load_scv_list('data/item-features.csv')
cur_id=1000; rmv=True; item_value=0.000000000001
while rmv:
    rmv=False; i=len(items)-1
    while i>0:
        j=i-1
        while j>=0:
            new_X = sum( np.abs( items[i].X - items[j].X ))
            if new_X <= item_value:
                rmv=True
                new_dot=dot(X = (items[i].X + items[j].X)/2, id=cur_id, near_dots=[])
                cur_id+=1
                if items[i].id>=1000: new_dot.near_dots.extend(items[i].near_dots)
                else: new_dot.near_dots.append(items[i].id)
                if items[j].id>=1000: new_dot.near_dots.extend(items[j].near_dots)
                else: new_dot.near_dots.append(items[j].id)

                t1=items.pop(i)
                t2=items.pop(j)
                items.append(new_dot)
                break

            j-=1
        i-=1
items_helper={}#для каждой точки сохраняем контейнер в котором она находится
items = items
items_dict = {}
items_check_count=0
for i in items:
    items_dict[i.id] = i
    if i.id>=1000:
        items_check_count+=len(i.near_dots)
        for n in i.near_dots:
            items_helper[n] = i.id
    else:
        items_check_count+=1



users = load_scv_list('data/user-features.csv')
cur_id=2000; rmv=True; user_value=0.000000000001
while rmv:
    rmv=False; i=len(users)-1
    while i>0:
        j=i-1
        while j>=0:
            new_X = sum( np.abs( users[i].X - users[j].X ))
            if new_X <= user_value:
                rmv=True
                new_dot=dot(X = (users[i].X + users[j].X)/2, id=cur_id, near_dots=[])
                cur_id+=1
                if users[i].id>=2000: new_dot.near_dots.extend(users[i].near_dots)
                else: new_dot.near_dots.append(users[i].id)
                if users[j].id>=2000: new_dot.near_dots.extend(users[j].near_dots)
                else: new_dot.near_dots.append(users[j].id)

                t1=users.pop(i)
                t2=users.pop(j)
                users.append(new_dot)
                break

            j-=1
        i-=1
users_helper={}#для каждой точки сохраняем контейнер в котором она находится
users = users
users_dict = {}
users_check_count=0
for u in users:
    users_dict[u.id] = u
    if u.id>=2000:        
        users_check_count+=len(u.near_dots)
        for n in u.near_dots:
            users_helper[n] = u.id
    else:
        users_check_count+=1




with open(f'data2/train.csv', 'wt') as f:
    f.write('user_id,item_id,like\n')

train = load_train('data/train.csv')
for t in train:
    if t.user_id in users_helper:
        newU = users_helper[t.user_id]
    else:
        newU = t.user_id
    if t.item_id in items_helper:
        newI = items_helper[t.item_id]
    else:
        newI = t.item_id
    with open(f'data2/train.csv', 'at') as f:
        f.write(f'{newU},{newI},{t.like}\n')


with open('data2/items_helper.txt', 'wb') as handle:
  pickle.dump(items_helper, handle)

with open('data2/users_helper.txt', 'wb') as handle:
  pickle.dump(users_helper, handle)

with open('data2/items_dict.txt', 'wb') as handle:
  pickle.dump(items_dict, handle)

with open('data2/users_dict.txt', 'wb') as handle:
  pickle.dump(users_dict, handle)

#with open('file.txt', 'rb') as handle:
#  b = pickle.loads(handle.read())
