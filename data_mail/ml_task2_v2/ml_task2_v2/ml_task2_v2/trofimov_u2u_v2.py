import numpy as np
from scipy import sparse as sp
import collections
import implicit
import sys
import csv
import datetime
import random
from sklearn.decomposition import TruncatedSVD
from sklearn.neighbors import NearestNeighbors

train_row = collections.namedtuple("train_row", ["user_id", "item_id", "like"])
def load_train(csv_file):
    X=[]
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            X.append(train_row(user_id=int(row[0]), item_id = int(row[1]), like = int(row[2])))
    return X

def load_test(csv_file):
    X=[]
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            X.append(train_row(user_id=int(row[0]), item_id = 0, like = 0))
    return X

rank_matrix=np.zeros((497,444))
train = load_train('data/train.csv')


for t in train:
    rank_matrix[t.user_id, t.item_id] += t.like*2-1

X_sparse = sp.coo_matrix(rank_matrix)
X_stored = X_sparse.tocsr()
test = load_test('data/test.csv')
dt=f'{datetime.datetime.now().strftime("%d.%m.%Y_%H.%M.%S")}'
with open(f'result/trofimov/u2u_result_{dt}.csv', 'wt') as f:
    f.write('user_id,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19\n')

aggr_result = {}
i=0
while i<3000:
    print(i)
    comp = random.randint(10, 50)
    num_neighbours = random.randint(1, 20)

    knn = NearestNeighbors(n_neighbors=num_neighbours, metric="cosine")
    knn.fit(X_sparse)

    for t in test:
        row_sparse = sp.coo_matrix(rank_matrix[t.user_id])

        knn_result = knn.kneighbors(row_sparse, n_neighbors=num_neighbours)
        neighbors = knn_result[1]
        scores = np.asarray(X_stored[neighbors[0]].sum(axis=0)[0]).flatten()
        top_indices = np.argsort(-scores)
        top_indices=top_indices[:20]

        if not t.user_id in aggr_result:
            aggr_result[t.user_id] = {}
        dct = aggr_result[t.user_id]
        j=0
        for rec in top_indices:
            item=rec
            if not item in dct:
                dct[item]=0
            dct[item] += 20 - j
            j+=1
    i+=1



for k in aggr_result:
    s=''
    dct2 = sorted(aggr_result[k].items(), key=lambda x: -x[1])
    i=0
    for l in dct2:
        if i > 19:
            break
        s+=f'{l[0]},'
        i+=1
    s=s.strip(',')
    with open(f'result/trofimov/u2u_result_{dt}.csv', 'at') as f:
        f.write(f'{k},{s}\n')