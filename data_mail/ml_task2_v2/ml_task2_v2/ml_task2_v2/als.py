import pandas as pd
import scipy.sparse as sparse
import numpy as np
import random
import implicit
from sklearn.preprocessing import MinMaxScaler
import csv
import collections
import datetime
train_row = collections.namedtuple("train_row", ["user_id", "item_id", "like"])
aggr_result = {}
def work(alpha, ft, reg, iter):
    df = pd.read_csv('data/train.csv')
    #print(df.sample(3))
    df.drop(['timestamp'], axis=1, inplace=True)

    #print(df['like'].value_counts())

    df = df.drop_duplicates()
    #print(df.sample(3))
    grouped_df = df.groupby(['user_id', 'item_id']).sum().reset_index()
    #print(grouped_df.sample(3))

    grouped_df['user_id'] = grouped_df['user_id'].astype("category")
    grouped_df['item_id'] = grouped_df['item_id'].astype("category")
    grouped_df['user_id'] = grouped_df['user_id'].cat.codes
    grouped_df['item_id'] = grouped_df['item_id'].cat.codes

    sparse_content_person = sparse.csr_matrix((grouped_df['like'].astype(float), (grouped_df['item_id'], grouped_df['user_id'])))
    sparse_person_content = sparse.csr_matrix((grouped_df['like'].astype(float), (grouped_df['user_id'], grouped_df['item_id'])))

    model = implicit.als.AlternatingLeastSquares(factors=ft, regularization=reg, iterations=iter)
    data = (sparse_content_person * alpha).astype('double')
    model.fit(data)


    def recommend(person_id, sparse_person_content, person_vecs, content_vecs, num_contents=20):
        # Get the interactions scores from the sparse person content matrix
        person_interactions = sparse_person_content[person_id,:].toarray()
        # Add 1 to everything, so that articles with no interaction yet become equal to 1
        person_interactions = person_interactions.reshape(-1) + 1
        # Make articles already interacted zero
        person_interactions[person_interactions > 1] = 0
        # Get dot product of person vector and all content vectors
        rec_vector = person_vecs[person_id,:].dot(content_vecs.T).toarray()
    
        # Scale this recommendation vector between 0 and 1
        min_max = MinMaxScaler()
        rec_vector_scaled = min_max.fit_transform(rec_vector.reshape(-1,1))[:,0]
        # Content already interacted have their recommendation multiplied by zero
        recommend_vector = person_interactions * rec_vector_scaled
        # Sort the indices of the content into order of best recommendations
        content_idx = np.argsort(recommend_vector)[::-1][:num_contents]
    
        # Start empty list to store titles and scores
        scores = []
        items = []
        for idx in content_idx:
            # Append titles and scores to the list
            items.append(idx)
            scores.append(recommend_vector[idx])
        #items.sort()
        recommendations = pd.DataFrame({'item': items, 'score': scores})

        return recommendations
    
    # Get the trained person and content vectors. We convert them to csr matrices
    person_vecs = sparse.csr_matrix(model.user_factors)
    content_vecs = sparse.csr_matrix(model.item_factors)


    def load_test(csv_file):
        X=[]
        i=0
        with open(csv_file) as csvfile:
            spamreader = csv.reader(csvfile)
            for row in spamreader:
                if i==0:
                    i+=1
                    continue
                X.append(train_row(user_id=int(row[0]), item_id = 0, like = 0))
        return X
    test = load_test('data/test.csv')


    for u in range(len(test)):
        user_id=test[u].user_id
        recommendations = recommend(user_id, sparse_person_content, person_vecs, content_vecs)

        if not user_id in aggr_result:
            aggr_result[user_id] = {}
        dct = aggr_result[user_id]
        j=0
        for rec in recommendations['item']:
            item=rec
            if not item in dct:
                dct[item]=0
            dct[item] += 20 - j
            j+=1
n=3000
i=0
while i<n:
    a = random.randint(1, 15)
    ft = random.randint(16, 24)
    r = random.uniform(0, 0.15)
    it = random.randint(5,50)
    print(i)
    work(a, ft, r, it)        
    i+=1
dt=f'{datetime.datetime.now().strftime("%d.%m.%Y_%H.%M.%S")}'
with open(f'result/als_{n}_iter_result_{dt}.csv', 'wt') as f:
    f.write('user_id,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19\n')

for k in aggr_result:
    s=''
    dct2 = sorted(aggr_result[k].items(), key=lambda x: -x[1])
    i=0
    for l in dct2:
        if i > 19:
            break
        s+=f'{l[0]},'
        i+=1
    s=s.strip(',')
    with open(f'result/als_{n}_iter_result_{dt}.csv', 'at') as f:
        f.write(f'{k},{s}\n')