
import numpy as np
from scipy import sparse as sp
import collections
import implicit
import sys
import csv
import datetime
import random
import pickle

dot = collections.namedtuple("dot", ["id", "X", "near_dots"])
train_row = collections.namedtuple("train_row", ["user_id", "item_id", "like"])
items_helper={}#для каждой точки сохраняем контейнер в котором она находится
items_dict = {}
users_helper={}#для каждой точки сохраняем контейнер в котором она находится
users_dict = {}
with open('data2/items_helper.txt', 'rb') as handle:
  items_helper = pickle.loads(handle.read())

with open('data2/items_dict.txt', 'rb') as handle:
  items_dict = pickle.loads(handle.read())
with open('data2/users_helper.txt', 'rb') as handle:
  users_helper = pickle.loads(handle.read())
with open('data2/users_dict.txt', 'rb') as handle:
  users_dict = pickle.loads(handle.read())


def load_train(csv_file):
    X=[]
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            X.append(train_row(user_id=int(row[0]), item_id = int(row[1]), like = int(row[2])))
    return X

def load_test(csv_file):
    X=[]
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            X.append(train_row(user_id=int(row[0]), item_id = 0, like = 0))
    return X
rank_matrix=np.zeros((116,77))
train = load_train('data/train.csv')

u=0;i=0
user_dict_to_index={}; item_dict_to_index={};item_dict_to_index_revert={}

for t in train:
    u1 = t.user_id
    if u1 in users_helper: uu=users_helper[u1]
    else: uu=u1
    i1 = t.item_id
    if i1 in items_helper: ii=items_helper[i1]
    else: ii=i1
    if not uu in user_dict_to_index:
        user_dict_to_index[uu] = u; u+=1
    if not ii in item_dict_to_index:
        item_dict_to_index[ii] = i
        item_dict_to_index_revert[i] = ii
        i+=1
    
    rank_matrix[user_dict_to_index[uu], item_dict_to_index[ii]] += t.like*101-1


test = load_test('data/test.csv')
dt=f'{datetime.datetime.now().strftime("%d.%m.%Y_%H.%M.%S")}'
with open(f'result/knn_result_{dt}.csv', 'wt') as f:
    f.write('user_id,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19\n')

def ger_result(top_indices):
    result = []
    i=0
    for top in top_indices:
        item = item_dict_to_index_revert[top]#conv - один из индексов от 1000 до 1999
        if len(items_dict[item].near_dots) > 0:
            nd = items_dict[item].near_dots
        else:
            nd = [item]
        for n in nd:
            if i > 19:
                return result
            result.append(n)
            i+=1
    return result

for t in test:
    id1 = t.user_id
    if t.user_id in users_helper:
        id1= users_helper[t.user_id]
    ind = user_dict_to_index[id1]
    top_indices = np.argsort(-rank_matrix[ind])
    result = ger_result(top_indices)


    s=''
    if len(result)!=20:
        print('bug')
    for r in result:
        s+=f'{r},'
    s=s.strip(',')
    with open(f'result/knn_result_{dt}.csv', 'at') as f:
        f.write(f'{t.user_id},{s}\n')



print('done..')
input()
print('exit..')
