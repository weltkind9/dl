import numpy as np
from scipy import sparse as sp
import collections
import implicit
import sys
import csv
import datetime
import random

train_row = collections.namedtuple("train_row", ["user_id", "item_id", "like"])
def load_train(csv_file):
    X=[]
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            X.append(train_row(user_id=int(row[0]), item_id = int(row[1]), like = int(row[2])))
    return X

def load_test(csv_file):
    X=[]
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            X.append(train_row(user_id=int(row[0]), item_id = 0, like = 0))
    return X

def make_coo_row(row, items = 444):
    values = []
    idx=[]
    for r in row:
        values.append(r.like*2 - 1)
        idx.append(r.item_id)

    return sp.coo_matrix(
        (np.array(values).astype(np.float32), ([0] * len(idx), idx)), shape=(1, items),
    )
dict_users = {}
train = load_train('data/train.csv')
for t in train:
    if not t.user_id in dict_users:
        dict_users[t.user_id] = [t]
    else:
        dict_users[t.user_id].append(t)

print(train[:2])
rows=[]
users_id_to_row={}
i=0
for t in dict_users:
    rows.append(make_coo_row(dict_users[t]))
    users_id_to_row[t] = i
    i+=1

print(rows[:2])
X_sparse = sp.vstack(rows).tocsr()

print(X_sparse.shape)
test = load_test('data/test.csv')
dt=f'{datetime.datetime.now().strftime("%d.%m.%Y_%H.%M.%S")}'
with open(f'result/trofimov/als_result_{dt}.csv', 'wt') as f:
    f.write('user_id,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19\n')

aggr_result = {}
i=0
while i<500:
    ft = random.randint(16, 32)
    r = random.uniform(0, 0.2)
    it = random.randint(5,50)

    model = implicit.als.AlternatingLeastSquares(factors=ft, regularization=r, iterations=it, calculate_training_loss = True)
    model.fit(X_sparse.T)

    for t in test:
        row_sparse = make_coo_row(dict_users[t.user_id]).tocsr()
        raw_recs = model.recommend(t.user_id, row_sparse, N=20, filter_already_liked_items=False, recalculate_user=False)
        if not t.user_id in aggr_result:
            aggr_result[t.user_id] = {}
        dct = aggr_result[t.user_id]
        j=0
        for rec in raw_recs:
            item=rec[0]
            if not item in dct:
                dct[item]=0
            dct[item] += 20 - j
            j+=1
    i+=1



for k in aggr_result:
    s=''
    dct2 = sorted(aggr_result[k].items(), key=lambda x: -x[1])
    i=0
    for l in dct2:
        if i > 19:
            break
        s+=f'{l[0]},'
        i+=1
    s=s.strip(',')
    with open(f'result/trofimov/als_result_{dt}.csv', 'at') as f:
        f.write(f'{k},{s}\n')