
import csv
import numpy as np
import csv
import collections
dot = collections.namedtuple("dot", ["id", "X", "near_dots"])
train_row = collections.namedtuple("train_row", ["user_id", "item_id", "like"])
def load_scv_list(csv_file):
    X=[]
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            X.append(dot(id= int(row[0]), X= np.array(row[1:], np.float), near_dots=[]) )
    return X

def load_scv_dict(csv_file):
    X={}
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0: i+=1; continue
            X[int(row[0])] = np.array(row[1:], np.float)
    return X

def load_train(csv_file):
    X=[]
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            X.append(train_row(user_id=int(row[0]), item_id = int(row[1]), like = int(row[2])))
    return X
def load_test(csv_file):
    X=[]
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            X.append(train_row(user_id=int(row[0]), item_id = 0, like = 0))
    return X
class MADEtask2DataLoader():
    def __init__(self):  
        self.items_dict_orig = load_scv_dict('data/item-features.csv')
        self.users_dict_orig = load_scv_dict('data/user-features.csv')
        self.train = load_train('data/train.csv')
        tmp_u={}
        tmp_i={}
        for row in self.train:
            if not row.like:
                continue
            if not row.item_id in tmp_i: tmp_i[row.item_id]=0
            if not row.user_id in tmp_u: tmp_u[row.user_id]=0

            tmp_i[row.item_id]+=1
            tmp_u[row.user_id]+=1

        items = load_scv_list('data/item-features.csv')
        cur_id=-1; rmv=True; item_value=0.000000000001
        while rmv:
            rmv=False; i=len(items)-1
            while i>0:
                j=i-1
                while j>=0:
                    new_X = sum( np.abs( items[i].X - items[j].X ))
                    if new_X <= item_value:
                        rmv=True
                        new_dot=dot(X = (items[i].X + items[j].X)/2, id=cur_id, near_dots=[])
                        cur_id-=1
                        if items[i].id<0: new_dot.near_dots.extend(items[i].near_dots)
                        else: new_dot.near_dots.append(items[i].id)
                        if items[j].id<0: new_dot.near_dots.extend(items[j].near_dots)
                        else: new_dot.near_dots.append(items[j].id)

                        t1=items.pop(i)
                        t2=items.pop(j)
                        items.append(new_dot)
                        break

                    j-=1
                i-=1
        self.items_helper={}#для каждой точки сохраняем контейнер в котором она находится
        self.items = items
        self.items_dict = {}
        for i in items:
            self.items_dict[i.id] = i
            if i.id<0:
                for n in i.near_dots:
                    self.items_helper[n] = i.id


        users = load_scv_list('data/user-features.csv')
        cur_id=-1; rmv=True; user_value=0.000000000001
        while rmv:
            rmv=False; i=len(users)-1
            while i>0:
                j=i-1
                while j>=0:
                    new_X = sum( np.abs( users[i].X - users[j].X ))
                    if new_X <= user_value:
                        rmv=True
                        new_dot=dot(X = (users[i].X + users[j].X)/2, id=cur_id, near_dots=[])
                        cur_id-=1
                        if users[i].id<0: new_dot.near_dots.extend(users[i].near_dots)
                        else: new_dot.near_dots.append(users[i].id)
                        if users[j].id<0: new_dot.near_dots.extend(users[j].near_dots)
                        else: new_dot.near_dots.append(users[j].id)

                        t1=users.pop(i)
                        t2=users.pop(j)
                        users.append(new_dot)
                        break

                    j-=1
                i-=1
        self.users_helper={}#для каждой точки сохраняем контейнер в котором она находится
        self.users = users
        self.users_dict = {}
        for u in users:
            self.users_dict[u.id] = u
            if u.id<0:
                for n in u.near_dots:
                    self.users_helper[n] = u.id

        self.test = load_test('data/test.csv')
        self.data_count = len(self.train)

        X = []; Y = []
        for row in self.train:
            if row.user_id in self.users_dict.keys():
                r1 = self.users_dict[row.user_id].X
            else:
                for k in self.users_dict:
                    if self.users_dict[k].id < 0:
                        if row.user_id in self.users_dict[k].near_dots:
                            r1= self.users_dict[k].X
                            break

            if row.item_id in self.items_dict.keys():
                r2 = self.items_dict[row.item_id].X
            else:
                for k in self.items_dict:
                    if self.items_dict[k].id < 0:
                        if row.item_id in self.items_dict[k].near_dots:
                            r2= self.items_dict[k].X
                            break

            X.append(np.concatenate((r1, r2)))
            Y.append(row.like)

        #self.X=torch.FloatTensor(X)
        #self.Y=torch.LongTensor(Y)

    def __len__(self):
        return self.data_count

    def __getitem__(self, i):
        return self.X[i], self.Y[i]
