def calc_score(test_choices, pred_choices, tk):
    s = 0
    for gt, p in zip(test_choices, pred_choices):
        s += int(gt in p)
    score = s / len(test_choices)
    return score

t1 =[[1,2,3,4,5]]
t2=[[1,3,4,5,6,7]]

c1= calc_score(t1,t2,'ololo')
c2= calc_score(t1,t1,'ololo')
import MADEtask2DataLoader
dl = MADEtask2DataLoader.MADEtask2DataLoader()

dct ={'d': 90, 'c': 30, 'a':18, 'b':24}
print(dct)
dct2 = sorted(dct.items(), key=lambda x: x[1])
print(dct)
print(dct2)