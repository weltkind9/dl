#include <iostream>
#include <cstdio>


struct MyCls {
    char c;
    double d;
    int i;
};
struct Cls {
    Cls(char c, double d, int i) {
        this->c = c;
        this->d = d;
        this->i = i;
    }
    Cls(int i,int i2, int i3) {
        this->c = c;
        this->d = d;
        this->i = i;
        //this->i2 = i2;
        //this->i3 = i3;
    }
private:
    char c;
    double d;
    int i;
    //int i2;
    //int i3;
};
char& get_c(Cls& cls) {
    return *(char*)(&cls);
}

// ��� ������� ������ ������������ ������ � ���� d ������� cls.
// �������� ��������, ��� ������������ ������ �� double, �. �.
// ������ ��������������� �� ������ � ������.
double& get_d(Cls& cls) {
    return ((MyCls*)&cls)->d;
}

// ��� ������� ������ ������������ ������ � ���� i ������� cls.
// �������� ��������, ��� ������������ ������ �� int, �. �.
// ������ ��������������� �� ������ � ������.
int& get_i(Cls& cls) {
    return ((MyCls*)&cls)->i;
}

void testAccseToPrivate()
{
    Cls cls('3', 1.5 ,5) ;
    //Cls cls(10, 20, 30);
    Cls& clsRef = cls;
    std::cout << get_c(cls) << std::endl;
    std::cout << get_d(cls) << std::endl;
    std::cout << get_i(cls) << std::endl;

    char* ch = (char*)(&clsRef);
    std::cout << *ch << std::endl;
    Cls cls2 = clsRef;

    double *d = (double*)(ch + sizeof(char));
    std::cout << *d << std::endl;

    int* ii = (int*)(d + sizeof(double));
    std::cout << *ii << std::endl;


    std::cout << std::endl << std::endl << std::endl;
    int j = 0;
    for (char *i = (char*)&cls; j < 24; i++, j++)
    {
        //std::cout << *i << std::endl;
        std::cout << *(int*)i << "     " << (i - (char*)&cls) << std::endl;
        //std::cout << *(double*)i << std::endl;
        //std::cout << std::endl << std::endl << std::endl;
    }
}