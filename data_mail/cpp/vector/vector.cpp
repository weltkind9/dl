﻿using namespace std;

#include <iostream>
#include <cstdio>
#include "StructString.cpp"
#include "Cls.h"

template <typename T>
T minimum(const T& lhs, const T& rhs)
{
    return lhs < rhs ? lhs : rhs;
}
void TemplateExample() {
    cout << minimum<int>(6, 2); cout << endl;
    void* addr = nullptr;
    cout << addr; cout << endl;
}
void PointerExample()
{//& - взятие адреса
    //* - разыменование - получение значения, находящегося по указанному адресу
    int a = 1;
    int* aptr = &a;
    cout << a; cout << endl;
    cout << aptr; cout << endl;
    cout << *aptr; cout << endl;
    cout << &aptr; cout << endl;


    size_t size = sizeof(a);
    cout << size; cout << endl;
    size_t sizeptr = sizeof(aptr);
    cout << sizeptr; cout << endl;

    cout << "aptr"; cout << endl;
    char* charPtr = (char*)aptr;
    cout << charPtr; cout << endl;
    cout << *charPtr; cout << endl;

    cout << "aptr + 1 int"; cout << endl;
    int* intPtr = (int*)aptr + 1;
    cout << intPtr; cout << endl;
    cout << *intPtr; cout << endl;

    cout << "aptr + 1 unsigned int"; cout << endl;
    unsigned int* uintPtr = (unsigned int*)aptr + 1;
    cout << uintPtr; cout << endl;
    cout << *uintPtr; cout << endl;

    unsigned short us = 1;
    unsigned short* usPtr = &us;
    cout << "us unsigned short"; cout << endl;
    cout << sizeof(us); cout << endl;
    cout << sizeof(usPtr); cout << endl;

    cout << "ptrdiff_t"; cout << endl;
    unsigned short us2 = 20;
    unsigned short* usPtr2 = &us2;
    ptrdiff_t d1 = usPtr - usPtr2;
    cout << d1; cout << endl;
    cout << sizeof(d1); cout << endl;

    cout << "ptrdiff_t char[]"; cout << endl;
    unsigned short usarr[5] = { 1,2,3,4,5 };
    for (auto c : usarr)
    {
        cout << c << " " << &c << endl;
        cout << &c - usPtr << endl;
    }
    cout << endl << endl << endl;
    for (int i = 0; i < 5; i++)
    {
        cout << usarr[i] << " " << &usarr[i] << endl;
        cout << &usarr[i] - usPtr << endl;
    }
}
int global = 0;
int globalStatic = 0;
void HeapExample();
void StringExample();





char* resize(const char* str, unsigned size, unsigned new_size)
{
    unsigned n = new_size > size ? size : new_size;
    char* new_str = new char[new_size];
    for (int i = 0; i < n; i++)
    {
        new_str[i] = str[i];
    }
    delete[] str;
    str = 0;
    return new_str;
}
char* add_symbol(char* str, char c, unsigned* size, unsigned position, unsigned bufferSize)
{
    if (position == *size)
    {
        str = resize(str, *size, (*size) + bufferSize);
        *size += bufferSize;
    }
    str[position] = c;
    return str;
}

char* getline()
{
    char input = '\0';
    unsigned position = 0;
    unsigned size = 3;
    unsigned* sizePtr = &size;
    char* str = new char[size];
    unsigned bufferSize = 3;
    while (cin.get(input)) {

        if (input == '\n')
            break;
        str = add_symbol(str, input, sizePtr, position, bufferSize);

        position++;
    }
    str = add_symbol(str, '\0', sizePtr, position, 1);
    return str;
}

void rezsize_test()
{
    int n = 10;
    char *str2 = new char[n];
    for(int i=0;i<6;i++)
        str2[i] = "Hello!"[i];    
    cout << str2 << endl;


    char *str2_rs = resize(str2, 6, 10);
    cout << str2_rs << endl;

    str2 = new char[n];
    for (int i = 0; i < 6; i++)
        str2[i] = "Hello!"[i];
    cout << str2 << endl;
    char* str3_rs = resize(str2, 6, 4);
    cout << str3_rs << endl;


    string ss = getline();
    cout << ss << endl;

//string *str3 = new string("foo");
  //  string str3_rs = resize(str3, 6, 10);
    //cout << str3_rs << endl;
}

int** transpose(const int* const* m, unsigned rows, unsigned cols)
{
    int** matrT = new int* [cols];
    for (int i = 0; i < cols; i++)
    {
        matrT[i] = new int[rows];
    }

    for (int c = 0; c < cols; c++)
    {
        for (int r = 0; r < rows; r++)
        {
            matrT[c][r] = m[r][c];
        }
    }
    return matrT;
}
void delete_matrix(int** m, unsigned rows, unsigned cols)
{
    for (int i = 0; i < rows; i++)
    {
        delete[] m[i];
        m[i] = 0;
    }
    delete[] m;
    m = 0;
}

void print_matr(const int* const* m, unsigned rows, unsigned cols)
{
    for (int r = 0; r < rows; r++)
    {
        for (int c = 0; c < cols; c++)
        {
            cout << m[r][c] << " ";
        }
        cout << endl;
    }
}
void swap_min(int* m[], unsigned rows, unsigned cols)
{
    int minElem = m[0][0];
    int minRowIndex = 0;
    for (int r = 0; r < rows; r++)
    {
        for (int c = 0; c < cols; c++)
        {
            if (m[r][c] < minElem)
            {
                minElem = m[r][c];
                minRowIndex = r;
            }
        }
    }
    int* tmp = m[0];
    m[0] = m[minRowIndex];
    m[minRowIndex] = tmp;
}
void matr_test_transpose()
{
    int rows = 3;
    int cols = 5;

    int** m = new int* [rows];
    for (int i = 0; i < rows; i++)
    {
        m[i] = new int[cols];
    }

    for (int r = 0; r < rows; r++)
    {
        for (int c = 0; c < cols; c++)
        {
            m[r][c] = c * 10 + r + 1;
        }
    }
    print_matr(m, rows, cols);
    int ** matrT = transpose(m, rows, cols);
    cout << endl;
    cout << endl;
    print_matr(matrT, cols, rows);
    delete_matrix(m, rows, cols);
    delete_matrix(matrT, cols,rows);

}
void matr_test_swap()
{
    int rows = 3;
    int cols = 5;

    int** m = new int* [rows];
    for (int i = 0; i < rows; i++)
    {
        m[i] = new int[cols];
    }

    for (int r = 0; r < rows; r++)
    {
        for (int c = 0; c < cols; c++)
        {
            m[r][c] = r * -10 + r + (c*2)+1;
        }
    }

    print_matr(m, rows, cols);
    swap_min(m, rows, cols);
    cout << endl;
    cout << endl;
    print_matr(m, rows, cols);
    delete_matrix(m, rows, cols);
}

struct Point
{
    Point():x(0), y(0)
    { 
    
    }
    Point(double x, double y) {
        this->x = x;
        this->y = y;
    }
    double x;
    double y;
};
void MemoryErrorTest()
{
    String s1(10, '*');
    String s2(20, '+');
    String s3 = s1;
    cout << s3.str<<endl;
    //s2 = s1;
    cout << s2.str << endl;
}
int main() {
    MemoryErrorTest();
    return 1;

    testAccseToPrivate();
    

    String s1 = "qwe";
    cout << s1.str << endl;
    String s2 (5,'*');
    cout << s2.str << endl;

    String s("Hello");
    s.append(s);
    cout << s.str << endl;

    matr_test_swap();
    //rezsize_test();
    //HeapExample();
    //StringExample();
    return 0;
}
void HeapExample()
{
    int* heap = (int*)malloc(sizeof(int));
    cout << hex << (uint64_t)main << endl;
    cout << hex << (uint64_t)HeapExample << endl;
    
    cout << hex << (uint64_t)&global << endl;
    cout << hex << (uint64_t)&globalStatic << endl;

    cout << hex << (uint64_t)heap << endl;
    cout << hex << (uint64_t)&heap << endl;
}
unsigned strlen(const char* str)
{
    for (unsigned i = 0; ; i++)
        if (*(str + i) == '\0')
            return i;
}
void strcat_my(char* to, const char* from)
{
    unsigned nTo = strlen(to);
    for (unsigned i = 0; ; i++)
    {
        if (*(from + i) == '\0')
            break;
        to[nTo + i] = from[i];
    }
}
int strstr_my(const char* text, const char* pattern)
{    
    for (int text_pos = 0; ; text_pos++)
    {
        for (int patt_pos = 0; ; patt_pos ++)
        {
            if (pattern[patt_pos] == '\0')
                return text_pos;

            if (pattern[patt_pos] != text[text_pos + patt_pos])
                break;
        }
        if (text[text_pos] == '\0')
            return -1;
    }
}

void StringExample()
{
    char str[] = "";
    cout << strlen(str) << endl;

    char str2[] = "Hello!";
    cout << strlen(str) << endl;


    char to[10] = "to";
    char from[] = "-> from";
    strcat_my(to, from);
    cout << to << endl;


    char to2[10] = "";
    char from2[] = "-> from";
    strcat_my(to2, from2);
    cout << to2 << endl;

    char to3[10] = "to";
    char from3[] = "";
    strcat_my(to3, from3);
    cout << to3 << endl;



    cout << strstr_my("abab", "ab") << endl;
    cout << strstr_my("edfabab", "ab") << endl;
    cout << strstr_my("edfabab", "rtyu") << endl;
    cout << strstr_my("edfabab", "dfc") << endl;
    cout << strstr_my("edfababdfc", "dfc") << endl;
    cout << strstr_my("", "") << endl;
    cout << strstr_my("dsfh", "") << endl;
    cout << strstr_my("", "dsh") << endl;


}