
#include <cstdio>
#include <iostream>
using namespace std;

class GIFT
{
    int i, j, k;

public:
    void Fun()
    {
        cout << i << " " << j << " " << k;
    }

};

int main1234()
{
    GIFT* obj = new GIFT(); // the value of i,j,k is 0
    int* ptr = (int*)obj;
    *ptr = 10;
    cout << *ptr;      // you also print value of I
    ptr++;
    *ptr = 15;
    cout << *ptr;      // you also print value of J
    ptr++;
    *ptr = 20;
    cout << *ptr;      // you also print value of K
    obj->Fun();
    return 1;
}