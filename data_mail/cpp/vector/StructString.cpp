#include <cstddef> // size_t
#include <cstring> // strlen, strcpy


struct String {

    /* ���������� ���� ����������� */
    String(const char* str = "")
    {
        size = std::strlen(str);
        this->str = new char[size+1];
        std::strcpy(this->str, str);
    }
    String(size_t n, char c)
    {
        size = n;
        this->str = new char[size+1];
        for (int i = 0; i < n; i++)
            this->str[i] = c;
        this->str[n] = '\0';
    }
    String(String const& other): size(other.size), str(new char[other.size+1])
    {
        for (int i = 0; i < other.size; i++)
            this->str[i] = other.str[i];
        this->str[other.size] = '\0';
    }
    String& operator =(String const& other)
    {
        if (this != &other)
        {
            delete [] str;
            this->size = other.size;
            this->str = new char[other.size + 1];
            for (int i = 0; i < other.size; i++)
                this->str[i] = other.str[i];
            this->str[other.size] = '\0';
        }
        return *this;
    }

    ~String()
    {
        delete[] str;
    }

    void append(String& other)
    {
         size_t newSize = other.size + this->size;
         char* newStr = new char[newSize + 1];
         for (int i = 0; i < size; i++)
         {
             newStr[i] = this->str[i];
         }
         for (int i = 0; i < other.size; i++)
         {
             newStr[this->size + i] = other.str[i];
         }
         newStr[newSize] = '\0';
         delete[]str;
         str = newStr;
         size = newSize;
    }

    size_t size;
    char* str;
};