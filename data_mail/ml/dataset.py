import os
import cv2
import numpy as np
import scipy.io as io
import csv

def save_csv(file, arr):
    i=0
    with open(file, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter='\n')
        spamwriter.writerow(arr)

def load_csv(trainX_file, trainY_file, testX_file, data_count, test_count, feature_count):
    X=np.ndarray((data_count, feature_count))
    i=0
    with open(trainX_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            X[i]=np.array(row)
            i+=1
    Y=np.ndarray((data_count), int)
    i=0
    with open(trainY_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            Y[i]=np.array(row)
            i+=1
    testX=np.ndarray((test_count, feature_count))
    i=0
    with open(testX_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            testX[i]=np.array(row)
            i+=1
    return X, Y, testX

def load_data_mat(filename, max_samples, seed=42):
    raw = io.loadmat(filename)
    X = raw['X']  # Array of [32, 32, 3, n_samples]
    y = raw['y']  # Array of [n_samples, 1]
    X = np.moveaxis(X, [3], [0])
    y = y.flatten()
    # Fix up class 0 to be 0
    y[y == 10] = 0
    
    np.random.seed(seed)
    samples = np.random.choice(np.arange(X.shape[0]),
                               max_samples,
                               replace=False)
    
    return X[samples].astype(np.float32), y[samples]


def load_svhn(folder, max_train, max_test):
    train_X, train_y = load_data_mat(os.path.join(folder, "train_32x32.mat"), max_train)
    test_X, test_y = load_data_mat(os.path.join(folder, "test_32x32.mat"), max_test)
    return train_X, train_y, test_X, test_y

def load_files(path, n):
    files = os.listdir(path)
    ln=len(files)
    X=np.ndarray((ln, 16,16, 3))
    y=np.ndarray((ln), int)
    for i in range(ln):
        f=files[i]
        img = cv2.imread(path+'/'+f)
        X[i]=img
        y[i]=n
    if ln<=1:
        num_v=-1
    elif ln<4:
        num_v=1
    else:
        num_v= ln/4
    #num_v=1
    return random_split_train_val(X, y, int(num_v))

def random_split_train_val(X, y, num_val, seed=42):
    np.random.seed(seed)

    indices = np.arange(X.shape[0])
    np.random.shuffle(indices)

    train_indices = indices[:-num_val]
    train_X = X[train_indices]
    train_y = y[train_indices]
    
    val_indices = indices[-num_val:]
    val_X = X[val_indices]
    val_y = y[val_indices]

    return train_X, train_y, val_X, val_y

def prepare_for_neural_network(train_X, test_X):
    train_flat = train_X.reshape(train_X.shape[0], -1).astype(np.float) / 255.0
    test_flat = test_X.reshape(test_X.shape[0], -1).astype(np.float) / 255.0
    
    # Subtract mean
    mean_image = np.mean(train_flat, axis = 0)
    train_flat -= mean_image
    test_flat -= mean_image
    
    return train_flat, test_flat
