import numpy as np
import methods
from layers import FullyConnectedLayer, ReLULayer, softmax_with_cross_entropy, l2_regularization


class TwoLayerNet:
    """ Neural network with two fully connected layers """

    def __init__(self, n_input, n_output, hidden_layer_size, reg):
        """
        Initializes the neural network

        Arguments:
        n_input, int - dimension of the model input
        n_output, int - number of classes to predict
        hidden_layer_size, int - number of neurons in the hidden layer
        reg, float - L2 regularization strength
        """
        self.reg = reg
        # TODO Create necessary layers
        self.l1=FullyConnectedLayer(n_input, hidden_layer_size)
        self.l2=ReLULayer()
        self.l3=FullyConnectedLayer(hidden_layer_size, n_output)
        #self.l4=ReLULayer()
        #self.l5=FullyConnectedLayer(hidden_layer_size, hidden_layer_size)

    def compute_loss_and_gradients(self, X, y):
        """
        Computes total loss and updates parameter gradients
        on a batch of training examples

        Arguments:
        X, np array (batch_size, input_features) - input data
        y, np array of int (batch_size) - classes
        """
        # Before running forward and backward pass through the model,
        # clear parameter gradients aggregated from the previous pass
        # TODO Set parameter gradient to zeros
        # Hint: using self.params() might be useful!
        #raise Exception("Not implemented!")
        N=X.shape[0]
        res1= self.l1.forward(X)
        res2= self.l2.forward(res1)
        res=self.l3.forward(res2)

        loss, grad = methods.softmax_with_cross_entropy(res, y)
        lN=loss
        loss/=N
        grad/=N

        grad*=0.3

        loss_reg1, grad_reg1 = methods.l2_regularization(self.l1.W.value, self.reg)
        loss_reg1 /= self.l1.W.value.size
        grad_reg1 /= self.l1.W.value.size

        loss_reg3, grad_reg3 = methods.l2_regularization(self.l3.W.value, self.reg)

        loss_reg3 /= self.l3.W.value.size
        grad_reg3 /= self.l3.W.value.size
   
        loss+= loss_reg1+loss_reg3

        g2 = self.l3.backward(grad)
        self.l3.params()['W'].value-=(self.l3.params()['W'].grad+grad_reg3)
        self.l3.params()['B'].value-=self.l3.params()['B'].grad

        g1 = self.l2.backward(g2)

        g = self.l1.backward(g1)
        self.l1.params()['W'].value-=(self.l1.params()['W'].grad+grad_reg1)
        self.l1.params()['B'].value-=self.l1.params()['B'].grad

        _res1= self.l1.forward(X)
        _res2= self.l2.forward(_res1)
        _res=self.l3.forward(_res2)
        _loss, _grad = methods.softmax_with_cross_entropy(_res, y)
        print(f"lN: {lN}; loss: {loss}; _loss: {_loss}")

        # TODO Compute loss and fill param gradients
        # by running forward and backward passes through the model
        
        # After that, implement l2 regularization on all params
        # Hint: self.params() is useful again!
        #raise Exception("Not implemented!")

        return loss

    def compute_loss_and_gradients2(self, X, y):
        """
        Computes total loss and updates parameter gradients
        on a batch of training examples

        Arguments:
        X, np array (batch_size, input_features) - input data
        y, np array of int (batch_size) - classes
        """
        # Before running forward and backward pass through the model,
        # clear parameter gradients aggregated from the previous pass
        # TODO Set parameter gradient to zeros
        # Hint: using self.params() might be useful!
        #raise Exception("Not implemented!")
        N=X.shape[0]
        res1= self.l1.forward(X)
        res2= self.l2.forward(res1)
        res=self.l3.forward(res2)

        loss, grad = methods.softmax_with_cross_entropy(res, y)
        lN=loss
        loss/=N
        grad/=N
        
        loss_reg1, grad_reg1 = methods.l2_regularization(self.l1.W.value, self.reg)
        loss_reg1 /= self.l1.W.value.size
        grad_reg1 /= self.l1.W.value.size

        loss_reg3, grad_reg3 = methods.l2_regularization(self.l3.W.value, self.reg)
        loss_reg3 /= self.l3.W.value.size
        grad_reg3 /= self.l3.W.value.size
   
        loss += loss_reg1+loss_reg3

        g2 = self.l3.backward(grad)
        self.l3.params()['W'].grad += grad_reg3

        g1 = self.l2.backward(g2)

        g = self.l1.backward(g1)
        self.l1.params()['W'].grad += grad_reg1

        #print(f"lN: {lN}; loss: {loss}")

        # TODO Compute loss and fill param gradients
        # by running forward and backward passes through the model
        
        # After that, implement l2 regularization on all params
        # Hint: self.params() is useful again!
        #raise Exception("Not implemented!")

        return loss

    def predict(self, X):
        """
        Produces classifier predictions on the set

        Arguments:
          X, np array (test_samples, num_features)

        Returns:
          y_pred, np.array of int (test_samples)
        """
        # TODO: Implement predict
        # Hint: some of the code of the compute_loss_and_gradients
        # can be reused
        result = np.zeros(X.shape[0], np.int)
        res1 = self.l1.forward(X)
        res2 = self.l2.forward(res1)
        pred = self.l3.forward(res2)
        y_pred=np.zeros(X.shape[0])
        probs = methods.softmax(pred)
        for i in range(X.shape[0]):
            result[i] = np.argmax(probs[i])

        return result

    def params(self):
        result = {'W1':self.l1.params()['W'], 'B1':self.l1.params()['B'],
                  'W3':self.l3.params()['W'], 'B3':self.l3.params()['B']}
        return result

    def fit(self, cur_ep, X, y, batch_size=100, epochs=1):
        '''
        Trains linear classifier        
        Arguments:
          X, np array (num_samples, num_features) - training data
          y, np array of int (num_samples) - labels
          batch_size, int - batch size to use
          learning_rate, float - learning rate for gradient descent
          reg, float - L2 regularization strength
          epochs, int - number of epochs
        '''

        num_train = X.shape[0]
        num_features = X.shape[1]
        num_classes = np.max(y)+1

        loss_history = []
        for epoch in range(epochs):
            shuffled_indices = np.arange(num_train)
            np.random.shuffle(shuffled_indices)
            sections = np.arange(batch_size, num_train, batch_size)
            batches_indices = np.array_split(shuffled_indices, sections)
            
            for batch in batches_indices:            
                batch_X=X[batch]
                batch_y=y[batch]
                loss = self.compute_loss_and_gradients(batch_X, batch_y)
                loss_history.append(loss)

            print(f"Epoch {cur_ep+epoch}, loss: {loss}")

        return loss_history

    
class ThreeLayerNet:
    """ Neural network with two fully connected layers """

    def __init__(self, n_input, n_output, hidden_layer_size, reg):
        """
        Initializes the neural network

        Arguments:
        n_input, int - dimension of the model input
        n_output, int - number of classes to predict
        hidden_layer_size, int - number of neurons in the hidden layer
        reg, float - L2 regularization strength
        """
        self.reg = reg
        # TODO Create necessary layers
        self.l1=FullyConnectedLayer(n_input, hidden_layer_size)
        self.l2=ReLULayer()
        self.l3=FullyConnectedLayer(hidden_layer_size, hidden_layer_size)
        self.l4=ReLULayer()
        self.l5=FullyConnectedLayer(hidden_layer_size,n_output )


    def compute_loss_and_gradients(self, X, y):
        """
        Computes total loss and updates parameter gradients
        on a batch of training examples

        Arguments:
        X, np array (batch_size, input_features) - input data
        y, np array of int (batch_size) - classes
        """
        # Before running forward and backward pass through the model,
        # clear parameter gradients aggregated from the previous pass
        # TODO Set parameter gradient to zeros
        # Hint: using self.params() might be useful!
        #raise Exception("Not implemented!")
        N=X.shape[0]
        
        res1 = self.l1.forward(X)
        res2 = self.l2.forward(res1)
        res3 = self.l3.forward(res2)
        res4=self.l4.forward(res3)
        res5=self.l5.forward(res4)

        loss, grad = methods.softmax_with_cross_entropy(res5, y)
        lN=loss
        loss/=N
        grad/=N

        grad*=0.3

        loss_reg1, grad_reg1 = methods.l2_regularization(self.l1.W.value, self.reg)
        loss_reg1 /= self.l1.W.value.size
        grad_reg1 /= self.l1.W.value.size

        loss_reg3, grad_reg3 = methods.l2_regularization(self.l3.W.value, self.reg)
        loss_reg3 /= self.l3.W.value.size
        grad_reg3 /= self.l3.W.value.size

        loss_reg5, grad_reg5 = methods.l2_regularization(self.l5.W.value, self.reg)
        loss_reg5 /= self.l5.W.value.size
        grad_reg5 /= self.l5.W.value.size

        loss+= loss_reg1+loss_reg3+loss_reg5



        g4 = self.l5.backward(grad)
        self.l5.params()['W'].value-=(self.l5.params()['W'].grad+grad_reg5)
        self.l5.params()['B'].value-=self.l5.params()['B'].grad

        g3 = self.l4.backward(g4)

        g2 = self.l3.backward(g3)
        self.l3.params()['W'].value-=(self.l3.params()['W'].grad+grad_reg3)
        self.l3.params()['B'].value-=self.l3.params()['B'].grad

        g1 = self.l2.backward(g2)

        g = self.l1.backward(g1)
        self.l1.params()['W'].value-=(self.l1.params()['W'].grad+grad_reg1)
        self.l1.params()['B'].value-=self.l1.params()['B'].grad

        _res1= self.l1.forward(X)
        _res2= self.l2.forward(_res1)
        _res3=self.l3.forward(_res2)
        _res4=self.l4.forward(_res3)
        _res5=self.l5.forward(_res4)
        _loss, _grad = methods.softmax_with_cross_entropy(_res5, y)
        print(f"lN: {lN}; loss: {loss}; _loss: {_loss}")

        # TODO Compute loss and fill param gradients
        # by running forward and backward passes through the model
        
        # After that, implement l2 regularization on all params
        # Hint: self.params() is useful again!
        #raise Exception("Not implemented!")

        return loss


    def compute_loss_and_gradients2(self, X, y):
        """
        Computes total loss and updates parameter gradients
        on a batch of training examples

        Arguments:
        X, np array (batch_size, input_features) - input data
        y, np array of int (batch_size) - classes
        """
        # Before running forward and backward pass through the model,
        # clear parameter gradients aggregated from the previous pass
        # TODO Set parameter gradient to zeros
        # Hint: using self.params() might be useful!
        #raise Exception("Not implemented!")
        N=X.shape[0]
        
        res1 = self.l1.forward(X)
        res2 = self.l2.forward(res1)
        res3 = self.l3.forward(res2)
        res4=self.l4.forward(res3)
        res5=self.l5.forward(res4)

        loss, grad = methods.softmax_with_cross_entropy(res5, y)
        lN=loss
        loss/=N
        grad/=N

        loss_reg1, grad_reg1 = methods.l2_regularization(self.l1.W.value, self.reg)
        loss_reg1 /= self.l1.W.value.size
        grad_reg1 /= self.l1.W.value.size

        loss_reg3, grad_reg3 = methods.l2_regularization(self.l3.W.value, self.reg)
        loss_reg3 /= self.l3.W.value.size
        grad_reg3 /= self.l3.W.value.size

        loss_reg5, grad_reg5 = methods.l2_regularization(self.l5.W.value, self.reg)
        loss_reg5 /= self.l5.W.value.size
        grad_reg5 /= self.l5.W.value.size

        loss+= loss_reg1+loss_reg3+loss_reg5



        g4 = self.l5.backward(grad)
        self.l5.params()['W'].value-=(self.l5.params()['W'].grad+grad_reg5)
        self.l5.params()['B'].value-=self.l5.params()['B'].grad

        g3 = self.l4.backward(g4)

        g2 = self.l3.backward(g3)
        self.l3.params()['W'].value-=(self.l3.params()['W'].grad+grad_reg3)
        self.l3.params()['B'].value-=self.l3.params()['B'].grad

        g1 = self.l2.backward(g2)

        g = self.l1.backward(g1)
        self.l1.params()['W'].value-=(self.l1.params()['W'].grad+grad_reg1)
        self.l1.params()['B'].value-=self.l1.params()['B'].grad

        _res1= self.l1.forward(X)
        _res2= self.l2.forward(_res1)
        _res3=self.l3.forward(_res2)
        _res4=self.l4.forward(_res3)
        _res5=self.l5.forward(_res4)
        _loss, _grad = methods.softmax_with_cross_entropy(_res5, y)
        #print(f"lN: {lN}; loss: {loss}; _loss: {_loss}")

        # TODO Compute loss and fill param gradients
        # by running forward and backward passes through the model
        
        # After that, implement l2 regularization on all params
        # Hint: self.params() is useful again!
        #raise Exception("Not implemented!")

        return loss


    def predict(self, X):
        """
        Produces classifier predictions on the set

        Arguments:
          X, np array (test_samples, num_features)

        Returns:
          y_pred, np.array of int (test_samples)
        """
        # TODO: Implement predict
        # Hint: some of the code of the compute_loss_and_gradients
        # can be reused
        result = np.zeros(X.shape[0], np.int)
        res1 = self.l1.forward(X)
        res2 = self.l2.forward(res1)
        res3 = self.l3.forward(res2)
        res4=self.l4.forward(res3)
        res5=self.l5.forward(res4)

        probs = methods.softmax(res5)
        for i in range(X.shape[0]):
            result[i] = np.argmax(probs[i])

        return result

    def params(self):
        result = {'W1':self.l1.params()['W'], 'B1':self.l1.params()['B'],
                  'W3':self.l3.params()['W'], 'B3':self.l3.params()['B']}
        return result

    def fit(self, cur_ep, X, y, batch_size=100, epochs=1):
        '''
        Trains linear classifier        
        Arguments:
          X, np array (num_samples, num_features) - training data
          y, np array of int (num_samples) - labels
          batch_size, int - batch size to use
          learning_rate, float - learning rate for gradient descent
          reg, float - L2 regularization strength
          epochs, int - number of epochs
        '''

        num_train = X.shape[0]
        num_features = X.shape[1]
        num_classes = np.max(y)+1

        loss_history = []
        for epoch in range(epochs):
            shuffled_indices = np.arange(num_train)
            np.random.shuffle(shuffled_indices)
            sections = np.arange(batch_size, num_train, batch_size)
            batches_indices = np.array_split(shuffled_indices, sections)
            
            for batch in batches_indices:            
                batch_X=X[batch]
                batch_y=y[batch]
                loss = self.compute_loss_and_gradients(batch_X, batch_y)
                loss_history.append(loss)

            print(f"Epoch {cur_ep+epoch}, loss: {loss}")

        return loss_history

