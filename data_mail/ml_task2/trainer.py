import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data.sampler import SubsetRandomSampler, Sampler
import numpy as np
from torchvision import transforms
import matplotlib.pyplot as plt
import csv
import datetime
import collections
import time
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2,  f_classif
from sklearn.metrics import roc_auc_score
import decimal

def compute_accuracy(model, loader):
    """
    Computes accuracy on the dataset wrapped in a loader
    
    Returns: accuracy as a float value between 0 and 1
    """
    model.eval() # Evaluation mode
    # TODO: Implement the inference of the model on all of the batches from loader,
    #       and compute the overall accuracy.
    # Hint: PyTorch has the argmax function!
    scores=[]
    for i_step, (x, y) in enumerate(loader):
        prediction = model(x)  
        score = roc_auc_score(y, prediction.detach()[:,1:])
        scores.append(score)
    return np.mean(scores)


def train_model(model, train_loader, val_loader, loss, optimizer, num_epochs, msg):    
    loss_history = []
    train_history = []
    val_history = []
    for epoch in range(num_epochs):
        model.train() # Enter train mode
        
        loss_accum = 0
        train_accuracies=[]
        for i_step, (x, y) in enumerate(train_loader):
            prediction = model(x)    
            loss_value = loss(prediction, y)
            optimizer.zero_grad()
            loss_value.backward()
            optimizer.step()
            train_accuracy= roc_auc_score(y, prediction.detach()[:,1:])
            train_accuracies.append(train_accuracy)
            
            loss_accum += loss_value

        ave_loss = loss_accum / (i_step + 1)
        train_accuracy = np.mean(train_accuracies)
        val_accuracy = compute_accuracy(model, val_loader)
        
        loss_history.append(float(ave_loss))
        train_history.append(train_accuracy)
        val_history.append(val_accuracy)
        
        print("%i %s Average loss: %f, Train accuracy: %f, Val accuracy: %f" % (epoch, msg, ave_loss, train_accuracy, val_accuracy))

    return train_history, val_history
