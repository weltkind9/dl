import dataset
import numpy as np
import pickle


def rank_matrix(user_count, user_neighbors, item_count, item_neighbors, train):
    m_like=np.zeros((user_count, item_count), np.int)
    m_unlike=np.zeros((user_count, item_count), np.int)
    for r in range(len(train)):
        row=train[r]
        if row.like: m=m_like
        else: m=m_unlike

        m[row.user_id, row.item_id]+=1
        un = user_neighbors[row.user_id]
        for u in un:
            m[u, row.item_id]+=1
        itn=item_neighbors[row.item_id]
        for i in itn:
            m[row.user_id, i]+=1

        for i in range(len(user_neighbors)):
            if row.user_id in user_neighbors[i]:
                m[i, row.item_id]+=1

        for i in range(len(item_neighbors)):
            if row.item_id in item_neighbors[i]:
                m[row.user_id, i]+=1
        print(f'{r} \t\t {row}')
    return m_like, m_unlike

#un = [[2,3],[3,0],[2,3],[1,2,3,0]]
#itn = [[1,2],[2,3],[0,3],[1,3], [4,0]]
#tr=[dataset.train_row( user_id =0, item_id=3, like = 1),dataset.train_row( user_id =3, item_id=1, like = 0)]
#L, UL = rank_matrix(4,un, 5, itn, tr)



items = dataset.load_scv2d('data/item-features.csv', 32)
user = dataset.load_scv2d('data/user-features.csv', 32)
train = dataset.load_train('data/train.csv')
#items = items[:10,]
#user = items[:10,]
#train=train[:10]

near_item=[]
near_user=[]
u=0;uu=0
for i in range(len(user)):
    near_user.append([])
    for j in range(len(user)):
        if i==j:
            continue
        X=sum( np.abs( user[i] - user[j]))
        uu+=1
        if X<0.005:#todo подобрать
            near_user[i].append(j)
            u+=1
b=0;bb=0
for i in range(len(items)):
    near_item.append([])
    for j in range(len(items)):
        if i==j:
            continue
        X=sum( np.abs( items[i] - items[j]))
        bb+=1
        if X<0.005:#todo подобрать
            near_item[i].append(j)
            b+=1

L, UL = rank_matrix(user.shape[0],near_user, items.shape[0], near_item, train)
with open('result/like_matrix.npy', 'ab') as f:
    np.save(f, L)
with open('result/like_matrix.npy', 'rb') as f:
    L1=np.load(f)

with open('result/unlike_matrix.npy', 'ab') as f:
    np.save(f, UL)
with open('result/unlike_matrix.npy', 'rb') as f:
    UL1=np.load(f)

with open('result/like_matrix.txt', 'at') as f:
    np.savetxt(f, L)
with open('result/like_matrix.txt', 'rt') as f:
    L2=np.loadtxt(f)

with open('result/unlike_matrix.txt', 'at') as f:
    np.savetxt(f, UL)
with open('result/unlike_matrix.txt', 'rt') as f:
    UL2=np.loadtxt(f)

with open('result/unlike_matrix2.txt', 'at') as f:
    for i in range(len(UL)):
        f.write(str(UL[0]).replace('\n','')+'\n')
with open('result/like_matrix2.txt', 'at') as f:
    for i in range(len(L)):
        f.write(str(L[0]).replace('\n','')+'\n')


i=0
#j=0
#usr=[]
#near=[]
#for i in range(444):
#    usr.append([])
#    near.append([])
#    for j in range(444):
#        if i==j:
#            continue
#        ut=dataset.unit(id=j, X=sum( np.abs( user[i] - user[j])) )
#        usr[i].append(ut)
#        if ut.X<0.005:
#            near[i].append(ut)
#i=0
#for n in near:
#    if len(n) == 0:
#        i+=1
#    print(len(n))


#cms=[]
#for nn in near:
#    r=np.zeros(32)
#    for n in nn:        
#        r+=user[n.id]
#    #r/=len(nn)
#    cms.append(dataset.cmass(vec=r, ind=nn, id=0, X=0))

#i=len(cms)-1
#rmv=True
#c=0
#while rmv:
#    rmv=False
#    c+=1
#    while i>0:
#        r1 = cms[i].vec + cms[i-1].vec

#        r2=np.zeros(32)
#        new_ind = []        
#        new_ind.extend(cms[i].ind)
#        new_ind.extend(cms[i-1].ind)
#        for v in new_ind:
#            r2+=user[v.id]
#        #r2/=len(new_ind)
        
#        r5=np.zeros(32)
#        for v in cms[i].ind:
#            r5+=user[v.id]
#        for v in cms[i-1].ind:
#            r5+=user[v.id]
#        #r5/=(len(cms[i].ind)+len(cms[i-1].ind))

        
#        r3=np.zeros(32)
#        for v in cms[i].ind:
#            r3+=user[v.id]
#        #r3/=len(cms[i].ind)
#        X3=sum( np.abs( r3 - cms[i].vec))


#        r4=np.zeros(32)
#        for v in cms[i-1].ind:
#            r4+=user[v.id]
#        #r4/=len(cms[i-1].ind)
#        X4=sum( np.abs( r4 - cms[i-1].vec))



#        X=sum( np.abs( r1 - r2))
#        r1/=len(new_ind)
#        X=sum( np.abs( r1 ))
#        print(X)
#        if X < 0.01:
#            rmv=True
#            t1=cms.pop(i)
#            t2=cms.pop(i-1)
#            cms.insert(i-1, dataset.cmass(vec=r1, ind=new_ind, id=0, X=0))

#        i-=1


#cms2=[]
#near2=[]
#for i in range(444):
#    cms2.append([])
#    near2.append([])
#    for j in range(444):
#        if i==j:
#            continue
#        ut=dataset.cmass(id=j, X=sum( np.abs( cms[i].vec - cms[j].vec)), ind = cms[i].ind+cms[j].ind, vec=0 )
#        cms2[i].append(ut)
#        if ut.X<0.01:
#            near2[i].append(ut)


#cms3=[]
#for nn in near2:
#    r=np.zeros(32)
#    for n in nn:        
#        r+=user[n.id]
#    r/=len(n)
#    cms3.append(dataset.cmass(vec=r, ind=n, id=0, X=0))




#cms4=[]
#near3=[]
#for i in range(444):
#    cms4.append([])
#    near3.append([])
#    for j in range(444):
#        if i==j:
#            continue
#        ut=dataset.cmass(id=j, X=sum( np.abs( cms3[i].vec - cms3[j].vec)), ind = cms3[i].ind+cms3[j].ind, vec=0 )
#        cms4[i].append(ut)
#        if ut.X<0.01:
#            near3[i].append(ut)

#for u in usr:
#    u.sort(key=lambda i: i.X)

#cm=[]
#for u in usr:
#    j=0
#    j2=0
#    f10mn = 0
#    for i in range(10):
#        f10mn+=u[i].X
#    f10mn/=10
#    for i in range(1, len(u)):
#        if u[i].X> 1.1 * f10mn:
#            j=i-1
#            break
#    for i in range(1, len(u)):
#        if u[i].X> u[0].X + (u[-1].X-u[0].X)/10 or u[i].X > 1.1 * u[0].X:
#            j2=i-1
#            break
#    continue
#    near = u[:j+1]
#    mn = np.zeros((32), np.float)
#    for n in near:
#        mn+=user[n.id]
#    mn /= len(near)
#    cm.append(mn)

#cmln=[]
#for i in range(444):
#    cmln.append([])
#    for j in range(444):
#        if i==j:
#            continue
#        cmln[i].append(dataset.unit(id=j, X=sum( np.abs( cm[i] - cm[j]))))
#for c in cmln:
#    u1 = c.sort(key=lambda i: i.X)
#input('done....')