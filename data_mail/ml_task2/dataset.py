import csv
import collections
import numpy as np

unit = collections.namedtuple("unit", ["id", "X"])
cmass = collections.namedtuple("cmass", ["id","vec", "ind", "X"])
train_row = collections.namedtuple("train_row", ["user_id", "item_id", "like"])


def load_scv1d(csv_file, data_count):
    Y=np.ndarray((data_count), np.float)
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            Y[i]=np.array(row, np.float)
            i+=1
    return Y

def load_scv2d(csv_file, feature_count):
    X=[]
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            X.append(row)

    XX=np.zeros((int(np.max(np.array(X, np.float))+1), feature_count), np.float)
    for i in range(len(X)):
        XX[int(X[i][0])] = np.array(X[i][1:])
    return XX

def load_train(csv_file):
    X=[]
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            X.append(train_row(user_id=int(row[0]), item_id = int(row[1]), like = int(row[2])))
    return X