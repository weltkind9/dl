import numpy as np
import pickle
import csv
import collections
import datetime
from dataload import *
import os
import decimal

ctx = decimal.Context()# create a new context for this task
ctx.prec = 20# 20 digits should be enough for everyone
def float_to_str(f):
    """Convert the given float to a string, without resorting to scientific notation"""
    d1 = ctx.create_decimal(repr(f))
    return format(d1, 'f')

def rank_matrix(users, user_count, items, item_count, train):
    m_like=np.zeros((user_count, item_count), np.int)
    m_unlike=np.zeros((user_count, item_count), np.int)
    for r in range(len(train)):
        row=train[r]
        if row.like: m=m_like
        else: m=m_unlike
        row_id=-1
        row_count_check=0
        for i in range(user_count):
            u=users[i]
            if u.id<0:
                if row.user_id in u.near_dots:
                    row_id=i
                    row_count_check+=1
            else:
                if row.user_id == u.id:
                    row_id=i
                    row_count_check+=1

        if row_count_check!=1:
            print('bug!')

        col_id=-1
        col_count_check=0
        for i in range(item_count):
            u=items[i]
            if u.id<0:
                if row.item_id in u.near_dots:
                    col_id=i
                    col_count_check+=1
            else:
                if row.item_id == u.id:
                    col_id=i
                    col_count_check+=1
        if col_count_check!=1:
            print('bug!')
        
        m[row_id, col_id]+=1
        #print(f'{r} \t\t {row}')
    return m_like, m_unlike
def work(item_value, user_value, folder):

    items = load_scv2d('data/item-features.csv', 32)
    cur_id=-1
    rmv=True
    c=0

    while rmv:
        rmv=False
        c+=1
        i=len(items)-1
        while i>0:
            j=i-1
            while j>=0:
                new_X = sum( np.abs( items[i].X - items[j].X ))
                if new_X < item_value:
                    rmv=True
                    new_dot=dot(X = (items[i].X + items[j].X)/2, id=cur_id, near_dots=[])
                    cur_id-=1
                    if items[i].id<0: new_dot.near_dots.extend(items[i].near_dots)
                    else: new_dot.near_dots.append(items[i].id)
                    if items[j].id<0: new_dot.near_dots.extend(items[j].near_dots)
                    else: new_dot.near_dots.append(items[j].id)

                    t1=items.pop(i)
                    t2=items.pop(j)
                    items.append(new_dot)
                    break

                j-=1
            i-=1
    check_count=0
    for d in items:
        if d.id<0:
            check_count+=len(d.near_dots)
        else:
            check_count+=1
    if check_count != 444:
        print('bug!')

    user = load_scv2d('data/user-features.csv', 32)
    cur_id=-1
    rmv=True
    c=0
    while rmv:
        rmv=False
        c+=1
        i=len(user)-1
        while i>0:
            j=i-1
            while j>=0:
                new_X = sum( np.abs( user[i].X - user[j].X ))
                if new_X < user_value:
                    rmv=True
                    new_dot=dot(X = (user[i].X + user[j].X)/2, id=cur_id, near_dots=[])
                    cur_id-=1
                    if user[i].id<0: new_dot.near_dots.extend(user[i].near_dots)
                    else: new_dot.near_dots.append(user[i].id)
                    if user[j].id<0: new_dot.near_dots.extend(user[j].near_dots)
                    else: new_dot.near_dots.append(user[j].id)

                    t1=user.pop(i)
                    t2=user.pop(j)
                    user.append(new_dot)
                    break

                j-=1
            i-=1
    check_count=0
    for d in user:
        if d.id<0:
            check_count+=len(d.near_dots)
        else:
            check_count+=1
    if check_count != 497:
        print('bug!')


    train = load_train('data/train.csv')
    test = load_test('data/test.csv')
    print('ranking')

    L, UL = rank_matrix(user, len(user), items, len(items), train)
    check_count=0
    for l in L:
        for i in l:
            check_count+=i
    for l in UL:
        for i in l:
            check_count+=i

    maxLike=np.max(L)
    maxUnLike=np.max(UL)
    LL=L/maxLike

    ULL=UL/maxUnLike

    #res = LL - ULL
    res = L
    with open(f'{folder}/res.npy', 'ab') as f:
        np.save(f, res)

    with open(f'{folder}/users.txt', 'at') as f:
        f.write(f'users:{len(user)}\n')
        for i in range(len(user)):
            if user[i].id<0:
                f.write(str(user[i].id)+ ' ' + str(user[i].near_dots).replace('\n','')+'\n')
            else:
                f.write(str(user[i].id).replace('\n','')+'\n')

    with open(f'{folder}/items.txt', 'at') as f:
        f.write(f'items: {len(items)}\n')
        for i in range(len(items)):
            if items[i].id<0:
                f.write(str(items[i].id)+ ' ' + str(items[i].near_dots).replace('\n','')+'\n')
            else:
                f.write(str(items[i].id).replace('\n','')+'\n')

    with open(f'{folder}/matr.txt', 'at') as f:
        f.write(f'users:{len(user)} items: {len(items)}\n')
        for i in range(len(res)):
            f.write(str(res[i]).replace('\n','')+'\n')


    user_pred=[]
    for i in range(len(res)):
        row = res[i]
        user_pred.append([])
        for j in range(len(row)):
            user_pred[i].append(pred(item_id = j, rank = res[i,j]))

    for up in user_pred:
        up.sort(key=lambda i: i.rank, reverse=True)

    prediction=[]
    for i in range(len( test)):
        t = test[i]
        cur_user=-1
        prediction.append([])
        for ii in range(len( user)):
            if user[ii].id<0:
                if t in user[ii].near_dots:
                    cur_user = ii
            else:
                if t == user[ii].id:
                    cur_user = ii
        if cur_user==-1:
            print('bug')

        cons_pred = user_pred[cur_user]
        while len(prediction[i]) < 20:
            for cp in cons_pred:
                if cp.item_id < 0:
                    for k in cp.near_dots:
                        prediction[i].append(k.id)
                        if len(prediction[i])>=20:
                            break
                else:
                    prediction[i].append(cp.item_id)
                    if len(prediction[i])>=20:
                        break

    with open(f'{folder}/r_i_{float_to_str(item_value)}_u_{float_to_str(user_value)}_{datetime.datetime.now().strftime("%d.%m_%H.%M")}.csv', 'at') as f:
        f.write('user_id,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19\n')
        for i in range(len(prediction)):
            if len(prediction[i]) != 20:
                print('bug')
            s=''
            for j in range(len(prediction[i])):
                s+=f'{prediction[i][j]},'
            s=s.strip(',')
            f.write(f'{test[i]},{s}\n')
    with open(f'{folder}/result.csv', 'at') as f:
        f.write('user_id,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19\n')
        for i in range(len(prediction)):
            if len(prediction[i]) != 20:
                print('bug')
            s=''
            for j in range(len(prediction[i])):
                s+=f'{prediction[i][j]},'
            s=s.strip(',')
            f.write(f'{test[i]},{s}\n')
j=0
i = 0.0#000005
while i < 0.026:
    u = 0.0000005
    while u < 0.026:
        f = f'result/result_i_{float_to_str(i)}_u_{float_to_str(u)}'
        if not  os.path.isdir(f):
            os.mkdir(f)
        print(j, i, u, f)
        work(i, u, f)
        j += 1
        u= round(u + 0.0005, 8)
    i=round(i+0.0005,8)