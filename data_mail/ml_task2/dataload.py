import csv
import numpy as np
import collections

pred = collections.namedtuple("pred", ["item_id", "rank"])
dot = collections.namedtuple("dot", ["id", "X", "near_dots"])
train_row = collections.namedtuple("train_row", ["user_id", "item_id", "like"])


def load_scv2d(csv_file, feature_count):
    X=[]
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            X.append(dot(id= int(row[0]), X= np.array(row[1:], np.float), near_dots=[]) )
    return X

def load_test(csv_file):
    X=[]
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            X.append(int(row[0]))
    return X

def load_train(csv_file):
    X=[]
    i=0
    with open(csv_file) as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            if i==0:
                i+=1
                continue
            X.append(train_row(user_id=int(row[0]), item_id = int(row[1]), like = int(row[2])))
    return X
