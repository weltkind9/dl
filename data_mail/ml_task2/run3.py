import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data.sampler import SubsetRandomSampler, Sampler
from MADEtask2DataLoader import *
import numpy as np
from torchvision import transforms
from sklearn.metrics import roc_auc_score
import matplotlib.pyplot as plt
from dataload import *
import csv
import datetime
import collections
import time
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2,  f_classif
import decimal
from trainer import *

# create a new context for this task
ctx = decimal.Context()

# 20 digits should be enough for everyone :D
ctx.prec = 20

def float_to_str(f):
    """
    Convert the given float to a string,
    without resorting to scientific notation
    """
    d1 = ctx.create_decimal(repr(f))
    return format(d1, 'f')


transition = collections.namedtuple("transition", ["n", "hide", "lr", "wd", "t_acc", "v_acc"])
prob = collections.namedtuple("prob", ["item_id", "p"])


def learn(msg, _lr, _weight_decay, layer_count, hide):
    data_train = MADEtask2DataLoader()
    batch_size = 500
    data_size = data_train.X.shape[0]
    validation_split = .2
    split = int(np.floor(validation_split * data_size))
    indices = list(range(data_size))
    np.random.shuffle(indices)
    train_indices, val_indices = indices[split:], indices[:split]
    train_sampler = SubsetRandomSampler(train_indices)
    val_sampler = SubsetRandomSampler(val_indices)

    train_loader = torch.utils.data.DataLoader(data_train, batch_size=batch_size, sampler=train_sampler)
    val_loader = torch.utils.data.DataLoader(data_train, batch_size=batch_size, sampler=val_sampler)
    feature_count = data_train.X.shape[1]
    if layer_count == 1:
        nn_model = nn.Sequential(
                nn.Linear(feature_count, 2),  nn.ReLU(inplace=True),
                nn.Softmax(1))
    elif layer_count==2:
        nn_model = nn.Sequential(
            nn.Linear(feature_count, hide), nn.ReLU(inplace=True),
            nn.Linear(hide, 2), nn.ReLU(inplace=True),
            nn.Softmax(1))
    elif layer_count==3:
        nn_model = nn.Sequential(
            nn.Linear(feature_count, hide), nn.ReLU(inplace=True),
            nn.Linear(hide, hide), nn.ReLU(inplace=True),
            nn.Linear(hide, 2), nn.ReLU(inplace=True),
            nn.Softmax(1))
    elif layer_count==4:
        nn_model = nn.Sequential(
            nn.Linear(feature_count, hide), nn.ReLU(inplace=True),
            nn.Linear(hide, hide), nn.ReLU(inplace=True),     
            nn.Linear(hide, hide), nn.ReLU(inplace=True),
            nn.Linear(hide, 2), nn.ReLU(inplace=True),
            nn.Softmax(1))
    else:
        nn_model = nn.Sequential(
            nn.Linear(feature_count, hide), nn.ReLU(inplace=True),
            nn.Linear(hide, hide), nn.ReLU(inplace=True),
            nn.Linear(hide, hide), nn.ReLU(inplace=True),     
            nn.Linear(hide, hide), nn.ReLU(inplace=True),
            nn.Linear(hide, 2), nn.ReLU(inplace=True),
            nn.Softmax(1))
    nn_model.type(torch.FloatTensor)

    loss = nn.CrossEntropyLoss().type(torch.FloatTensor)
    optimizer = optim.SGD(nn_model.parameters(), lr=_lr, weight_decay=_weight_decay)

    train_accuracy, val_accuracy = train_model(nn_model, train_loader, val_loader, loss, optimizer, 200, msg)
    return train_accuracy, val_accuracy


def learn_and_predict(msg, _lr, _weight_decay, layer_count, hide):
    data_train = MADEtask2DataLoader()
    batch_size = 500
    data_size = data_train.X.shape[0]
    validation_split = .2
    split = int(np.floor(validation_split * data_size))
    indices = list(range(data_size))
    np.random.shuffle(indices)
    train_indices, val_indices = indices[split:], indices[:split]
    train_sampler = SubsetRandomSampler(train_indices)
    val_sampler = SubsetRandomSampler(val_indices)

    train_loader = torch.utils.data.DataLoader(data_train, batch_size=batch_size, sampler=train_sampler)
    val_loader = torch.utils.data.DataLoader(data_train, batch_size=batch_size, sampler=val_sampler)
    feature_count = data_train.X.shape[1]
    if layer_count == 1:
        nn_model = nn.Sequential(
                nn.Linear(feature_count, 2),  nn.ReLU(inplace=True),
                nn.Softmax(1))
    elif layer_count==2:
        nn_model = nn.Sequential(
            nn.Linear(feature_count, hide), nn.ReLU(inplace=True),
            nn.Linear(hide, 2), nn.ReLU(inplace=True),
            nn.Softmax(1))
    elif layer_count==3:
        nn_model = nn.Sequential(
            nn.Linear(feature_count, hide), nn.ReLU(inplace=True),
            nn.Linear(hide, hide), nn.ReLU(inplace=True),
            nn.Linear(hide, 2), nn.ReLU(inplace=True),
            nn.Softmax(1))
    elif layer_count==4:
        nn_model = nn.Sequential(
            nn.Linear(feature_count, hide), nn.ReLU(inplace=True),
            nn.Linear(hide, hide), nn.ReLU(inplace=True),     
            nn.Linear(hide, hide), nn.ReLU(inplace=True),
            nn.Linear(hide, 2), nn.ReLU(inplace=True),
            nn.Softmax(1))
    else:
        nn_model = nn.Sequential(
            nn.Linear(feature_count, hide), nn.ReLU(inplace=True),
            nn.Linear(hide, hide), nn.ReLU(inplace=True),
            nn.Linear(hide, hide), nn.ReLU(inplace=True),     
            nn.Linear(hide, hide), nn.ReLU(inplace=True),
            nn.Linear(hide, 2), nn.ReLU(inplace=True),
            nn.Softmax(1))
    nn_model.type(torch.FloatTensor)

    loss = nn.CrossEntropyLoss().type(torch.FloatTensor)
    optimizer = optim.SGD(nn_model.parameters(), lr=_lr, weight_decay=_weight_decay)

    train_accuracy, val_accuracy = train_model(nn_model, train_loader, val_loader, loss, optimizer, 50, msg)
    with open(f'result_nn/result.csv', 'wt') as f:
        f.write('user_id,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19\n')    
    with open(f'result_nn/result2.csv', 'wt') as f:
        f.write('user_id,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19\n')    

    nn_model.eval()
    result = {}
    for u in range(len(data_train.test)):
        user_id=data_train.test[u].user_id
        result[user_id]=[]
        for k in data_train.items_dict_orig:
            print(f'user {user_id}\t\titem:{k}')
            #if user_id in data_train.users_dict.keys():
            #    r1 = data_train.users_dict[user_id].X
            #else:
            #    r1 = data_train.users_dict[data_train.users_helper[user_id]].X

            #if k in data_train.items_dict.keys():
            #    r2 = data_train.items_dict[k].X
            #else:
            #    r2 = data_train.items_dict[data_train.items_helper[k]].X
            r1 = data_train.users_dict_orig[user_id]
            r2 = data_train.items_dict_orig[k]

            x=np.concatenate((r1, r2))
            pred = nn_model(torch.FloatTensor([x]))
            if k<0:
                for itm in data_train.items_dict[k].near_dots:
                    result[user_id].append(prob(p=pred[:,1:].item(), item_id = itm))
            else:
                result[user_id].append(prob(p=pred[:,1:].item(), item_id = k))

        result[user_id].sort(key = lambda s: s.p, reverse = True)

        s=''; s2=''
        for r in range(20):
            s+=f'{result[user_id][r].item_id},'
            s2+=f'{result[user_id][r]}'
        s=s.strip(',')
        with open(f'result_nn/result.csv', 'at') as f:
            f.write(f'{user_id},{s}\n')

        with open(f'result_nn/result2.csv', 'at') as f:
            f.write(f'{user_id},{s2}\n')

        print(f'{u}: {user_id}')
    return train_accuracy, val_accuracy
  
trans=[]
layer_count=[1,2,3,4,5]
hide=[100, 500, 1000]
lr = [0.3, 0.1, 0.05, 0.01, 0.005, 0.001]
weight_decay = [0.3, 0.1, 0.05, 0.01]
t1=time.time()


train_accuracy, val_accuracy = learn_and_predict(f'2_100_0.005_0.01', _lr=0.005, _weight_decay=0.01, layer_count=2, hide=1000)
#for i in range(49,200,30):
    #print(i)
#for _n in layer_count:
    #for _h in hide:
        #for _lr in lr:
            #for _wd in weight_decay:
                #train_accuracy, val_accuracy = learn_and_write_result(f'{_n}_{_h}_{_lr}_{_wd}', _lr, _wd, _n, _h)
                #t=transition(n=_n, t_acc= np.round(train_accuracy,3)[range(49,200,30)], v_acc=np.round(val_accuracy,3)[range(49,200,30)], lr=_lr, wd=_wd, hide=_h)
                #trans.append(t)
                #file = open(f'result_nn/_stat.txt', 'at')
                #file.write(f'{datetime.datetime.now().strftime("%d.%m.%Y_%H.%M.%S")}_{t}\n')
                #file.close()
t2 = time.time()
print(f'time: {round(t2-t1)} sec')
s = ''
for t in trans:
    s+=str(t)+'\n'
file = open(f'result_nn/stat_{datetime.datetime.now().strftime("%d.%m.%Y_%H.%M.%S")}.txt', 'w')
file.write(s)
file.close()

input('done....')