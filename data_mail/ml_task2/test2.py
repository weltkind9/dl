import dataset
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import plotly.offline as py
import seaborn as sns
import warnings; warnings.filterwarnings(action='once')
import plotly.graph_objs as go

#large = 22; med = 16; small = 12
#params = {'axes.titlesize': large,
#          'legend.fontsize': med,
#          'figure.figsize': (16, 10),
#          'axes.labelsize': med,
#          'axes.titlesize': med,
#          'xtick.labelsize': med,
#          'ytick.labelsize': med,
#          'figure.titlesize': large}
#plt.rcParams.update(params)
#plt.style.use('seaborn-whitegrid')
#sns.set_style("white")


items = dataset.load_scv2d('data/item-features.csv', 444, 32)
user = dataset.load_scv2d('data/user-features.csv', 497, 32)
i=0
j=0
c=0
c1=0
usr=[]
for i in range(444):
    for j in range(444):
        #if i==j:
        #    continue
        x=sum( np.abs( user[i] - user[j]))
        usr.append(x)
        #if x==0:
        #    c1+=1
        #else:
        if x<0.01:
            c+=1
        if x<0.005:
            c1+=1

# Draw Plot
plt.figure(figsize=(16,10), dpi= 80)
sns.kdeplot(usr, shade=True, color="g", label="Cyl=4")
#plt.show()

sns_plot = sns.distplot(usr)
fig = sns_plot.get_figure()

# Decoration
plt.title('Density Plot of City Mileage by n_Cylinders', fontsize=22)
plt.legend()
plt.show()