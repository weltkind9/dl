import numpy as np
import random
import time

size = 14
def score(candidate ):
    res=0
    for i in range(size-1,-1,-1):
        if candidate.alleles[i]==1:
            res+=pow(2, size-1-i)
    rightAnswer = (496-37)/17
    return abs(res - rightAnswer)
def prob(p):
    pp = random.random()
    if pp > p:
        return True
    else:
        return False

class gene():
    def __init__(self, answerShape):
        self.alleles = np.zeros(( np.prod(answerShape)), np.int8)
        for i in range(np.prod(answerShape)):
            self.alleles[i] = random.randint(0,1)
        self.alleles = self.alleles.reshape(answerShape)
        #self.alleles = np.array((0,0,0,1,1,0,1,1))
        self.score = 100500
        self.score2 = 100500 #величина обратная ошибке и отнормированная на сумму ошибок в популяции. для определения выроятности выбора в следующее поколение
        self.beforeScore= 100500 # сумма велични обратных ошибке для всех предыдущих кандидатов в популяции нарастающим итогом

    def __str__(self):
        return f'{self.beforeScore}:{self.score}';

def method1():
    counter = 0
    gn = gene((size))
    sc = score(gn)
    while sc > 0:
        for i in range(gn.alleles.shape[0]):
            p = prob(0.9)
            if p:
                if gn.alleles[i] == 1:
                    gn.alleles[i] = 0
                else:
                    gn.alleles[i] = 1

        sc = score(gn)
        counter += 1
        print(f'{counter} {gn.alleles}')

#method1()

population = (gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size)),
              gene((size)),gene((size)),gene((size)),gene((size))
              )
sc = 100500
counter=1
while sc > 0:
    popScore = 0
    for gn in population:
        gn.score = score(gn)
        popScore += gn.score
    popScore2 = sum([o.score-popScore for o in population])
    for gn in population:
        gn.score2 = (gn.score - popScore)/popScore2
    c=0
    for gn in population:
        gn.beforeScore = c
        c+=gn.score2
    toNextGeneration = []
    for i in range(len(population)):
        randomScore = random.random()
        for j in range(len(population)-1,-1,-1):
            if randomScore > population[j].beforeScore:
                toNextGeneration.append(population[j])
                break
        else:
            toNextGeneration.append(population[0])
    nextGeneration=[]
    for i in range(0, len(population), 2):
        child1 = np.concatenate(( population[i].alleles[:5] , population[i+1].alleles[5:]))
        child2 = np.concatenate(( population[i].alleles[5:] , population[i+1].alleles[:5]))
        nextGeneration.append(child1)
        nextGeneration.append(child2)

    for gn in nextGeneration:
        for i in range(gn.shape[0]):
            p = prob(0.9)
            if p:
                if gn[i] == 1:
                    gn[i] = 0
                else:
                    gn[i] = 1

    for i in range(len( population)):
        population[i].alleles = nextGeneration[i]
    minScore=99999999999999999999
    for gn in population:
        gn.score = score(gn)
        if sc > gn.score:
            sc = gn.score
        if minScore > gn.score:
            minScore = gn.score
    print(f'{counter} {popScore} {minScore}')
    #time.sleep(0.1)
    counter+=1

for i in range(gn.alleles.shape[0]):
    print(gn.alleles[i])
    print()
for i in range(gn.alleles.shape[0]):
    for j in range(gn.alleles.shape[1]):
        print(gn.alleles[i][j])
    print()
print()