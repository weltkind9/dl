import random
import math

from common.bot import BotInterface
class Target():
    def __init__(self, position, ship, speed):
        self.position = position
        self.ship = ship
        self.speed = speed
        self.bolt_time = math.ceil((position[1] - 92) / 30) #в игровых тиках
        self.nextPos = position[0] + speed * self.bolt_time
        self.ship_time = math.ceil((self.nextPos - ship[0]) / 26)

        self.time = self.bolt_time + abs(self.ship_time)


    def __str__(self):
        return f'{self.position[0]} -> {self.nextPos}       bolt: {self.bolt_time}     ship: {self.ship_time}  time: {self.time}'




class Bot(BotInterface):
    def __init__(self, *args, **kwargs):
        self.prevAlienPos = None
        self.step_bolt=0
        return super().__init__(*args, **kwargs)

    def checkSavePosition(self, x, aliens_bolts):
        if x <= 22:
            return False
        if x >= 800-22:
            return False
        for ab in aliens_bolts:
            if abs(ab[0] - x) < 26 and ab[1] < 150:
                #print(f'danger bolt: {ab} ship: {x}')
                return False
        return True

    def make_move(self, aliens, aliens_bolts, ship_bolt, ship):
        try:
            r = self.move(aliens, aliens_bolts, ship_bolt, ship)
            return r
        except Exception as ex:
            print(ex)
            return 'pass'

    def nextAlienPosition(self, position, speed, time):
        x, y = position
        for t in range(time):
            if x + speed >= 195 or x + speed <= 32:
                y += 9
                speed *= -1
            else:
                x += speed
        return x, y


    def move(self, aliens, aliens_bolts, ship_bolt, ship):
        if not ship_bolt:
            self.step_bolt=0
        else:
            print(f'step:{self.step_bolt} {ship_bolt}')
            self.step_bolt += 1
        
        

        alienSpeed = None
        if self.prevAlienPos:
            alienSpeed = aliens[0][0] - self.prevAlienPos[0]
        self.prevAlienPos = aliens[0]


        c = self.checkSavePosition( ship[0], aliens_bolts)
        if not c:
            step = 26
            while 1:
                if ship[0] + step > 800 and ship[0] - step <0:
                    break
                c1 = self.checkSavePosition(ship[0] - step, aliens_bolts)
                if c1:
                    return 'left 26'
                c2 = self.checkSavePosition( ship[0] + step, aliens_bolts)
                if c2:
                    return 'right 26'
                step += 26




        aliensDict={}
        for a in aliens:
            if a[0] in aliensDict:
                if aliensDict[a[0]] > a:
                    aliensDict[a[0]] = a
            else:
                aliensDict[a[0]] = a

        targets = []
        for k in aliensDict:
            targets.append(Target(aliensDict[k], ship, alienSpeed))

        #print("\r\n".join([str(t) for t in targets]))
        nearTarget = targets[0]
        np = self.nextAlienPosition(nearTarget.position, alienSpeed, nearTarget.bolt_time)
        
        print(f'{nearTarget.position} -> {np}')



        if abs(np[0] - ship[0]) > 10:
            if np[0] > ship[0]:
                x = int(np[0] - ship[0])
                if x > 26:
                    x = 26
                if self.checkSavePosition(ship[0]+x, aliens_bolts):
                    return f'right {x}'
            else:
                x = int(ship[0] - np[0])
                if x > 26:
                    x = 26
                if self.checkSavePosition(ship[0]-x, aliens_bolts):
                    return f'left {x}'
        else:
            if not ship_bolt:
                return 'fire'
        return 'pass'
























class Bot2(BotInterface):
    def __init__(self, *args, **kwargs):
        self.prevAlienPos = None
        self.prevShipBoltPos = None
        return super().__init__(*args, **kwargs)

    def checkSavePosition(self, x, aliens_bolts):
        for ab in aliens_bolts:
            if abs(ab[0] - x) < 26 and ab[1] < 150:
                #print(f'danger bolt: {ab} ship: {x}')
                return False
        return True
    def make_move(self, aliens, aliens_bolts, ship_bolt, ship):
        try:
            r = self.move(aliens, aliens_bolts, ship_bolt, ship)
            return r
        except Exception as ex:
            print(ex)
            return 'pass'

    def move(self, aliens, aliens_bolts, ship_bolt, ship):
        ##print('ship:')
        
        alienSpeed = None
        if self.prevAlienPos:
            alienSpeed = aliens[0][0] - self.prevAlienPos[0]
        shipBotSpeed = None
        if ship_bolt and self.prevShipBoltPos:
            shipBotSpeed = ship_bolt[1] - self.prevShipBoltPos[1]

        #print(f'speed: {alienSpeed}; bolt_speed: {shipBotSpeed}; {self.prevAlienPos} -> {aliens[0]}')
        self.prevAlienPos = aliens[0]
        self.prevShipBoltPos = ship_bolt


        c = self.checkSavePosition( ship[0], aliens_bolts)
        if not c:
            step = 26
            while 1:
                if ship[0] + step > 800 and ship[0] - step <0:
                    break
                if ship[0] - step > 0:
                    c1 = self.checkSavePosition(ship[0] - step, aliens_bolts)
                    if c1:
                        #print('left')
                        return 'left 26'
                if ship[0] + step < 800:
                   c2 = self.checkSavePosition( ship[0] + step, aliens_bolts)
                   if c2:
                       #print('right')
                       return 'right 26'
                step += 26


        ##print('aliens_bolts:')
        ##print(aliens_bolts)
        if ship[0] < 250 and self.checkSavePosition(ship[0]+26, aliens_bolts):
            #print('less 250 right')
            return 'right 26'
        if ship[0] > 550 and self.checkSavePosition(ship[0]-26, aliens_bolts):
            #print('great 550 left')
            return 'left 26'









        if not ship_bolt:

            print(f'{ship};\t{aliens[0]}')

            if alienSpeed and ((aliens[0][0] > 40 and alienSpeed > 0) or (aliens[0][0] < 175 and alienSpeed < 0)):
                aliensDict={}
                for a in aliens:
                    if a[0] in aliensDict:
                        if aliensDict[a[0]] > a:
                            aliensDict[a[0]] = a
                    else:
                        aliensDict[a[0]] = a

                targets = []
                for k in aliensDict:
                    targets.append(Target(aliensDict[k], ship, alienSpeed))

                #print("\r\n".join([str(t) for t in targets]))
                nearTarget = targets[0]
                for t in targets:
                    if t. position[1]> nearTarget.position[1] :
                        nearTarget = t
                    if abs(nearTarget.nextPos - ship[0]) > abs(t.nextPos - ship[0]):
                        nearTarget = t
                #print(nearTarget)
                if abs(nearTarget.nextPos - ship[0]) > 20:
                    if nearTarget.nextPos > ship[0]:
                        x = int(nearTarget.nextPos - ship[0])
                        if x > 26:
                            x = 26
                        if self.checkSavePosition(ship[0]+x, aliens_bolts):
                            return f'right {x}'
                    else:
                        x = int(ship[0] - nearTarget.nextPos)
                        if x > 26:
                            x = 26
                        if self.checkSavePosition(ship[0]-x, aliens_bolts):
                            return f'left {x}'
                #print(targets.index(nearTarget))
                return 'fire'


 
            #return 'fire'

  
        return 'pass'

def olo():

        aliensDict={}
        for a in aliens:
            m = int(a[0] / 26)
            if m not in aliensDict:
                aliensDict[m] = 1
            else:
                aliensDict[m] += 1
        m = int(ship[0] / 26)
        s = aliensDict[m]
        for step in range(5):
            if m - step in aliensDict:
                s += aliensDict[m-step]
            if m + step in aliensDict:
                s += aliensDict[m+step]
        #print(f'alien count: {s}')
        sL=0; sR=0
        if s < 15:
            for k in aliensDict:
                if k < m: sL += aliensDict[k]
                if k > m: sR += aliensDict[k]
            if sL < sR:
                #print(f'{sL} < {sR} alien count: right')
                if self.checkSavePosition(ship[0]+26, aliens_bolts):
                    #print('right')
                    return 'right 26'
            else:
                #print(f'{sL} > {sR}  alien count: left')
                if self.checkSavePosition(ship[0]-26, aliens_bolts):
                    #print('left')
                    return 'left 26'

