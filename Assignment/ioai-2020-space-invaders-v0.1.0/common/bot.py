import time
import random

from typing import List, Tuple

class BotInterface:
    """
    Class of bot interface

    Inherit from this class and overload `make_move` function
    """
    def make_move(self, 
            aliens: List[Tuple[int, int]], 
            aliens_bolts: List[Tuple[int, int]], 
            ship_bolt: Tuple[int, int],
            ship: Tuple[int, int]) -> str:
        
        raise NotImplementedError()
