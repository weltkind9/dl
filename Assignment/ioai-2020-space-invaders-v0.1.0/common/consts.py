"""
Constants for Alien Invaders

Origin of the game is in lower left corner
"""

import sys

LOG_LEVEL = 'info'

# time in second to think for the bot
TIMEOUT = 1

### WINDOW CONSTANTS (all coordinates are in pixels) ###

# the width of the game display
GAME_WIDTH = 800
# the height of the game display
GAME_HEIGHT = 700
# Amount of tick for the game
AMOUNT_OF_TICKS = 720

# The y-coordinate of the defensive line the ship is protecting
DEFENSE_LINE = 100

### ALIEN CONSTANTS ###

# the width of an alien
ALIEN_WIDTH = 36
# the height of an alien
ALIEN_HEIGHT = 36
# the horizontal separation between aliens
ALIEN_H_SEP = 16
# the vertical separation between aliens
ALIEN_V_SEP = 16
# the number of horizontal pixels to move an alien
ALIEN_H_WALK = ALIEN_WIDTH // 4
# the number of vertical pixels to move an alien
ALIEN_V_WALK = ALIEN_HEIGHT // 4
# The distance of the top alien from the top of the window
ALIEN_CEILING = 100
# the number of rows of aliens, in range 1..10
ALIEN_ROWS = 5
# scores for killing alien for each row. Length should be ALIEN_ROWS
ALINES_SCORES = (11, 8, 5, 3, 3)
# the number of aliens per row
ALIENS_IN_ROW = 12
# max score that ship can get
MAX_SCORE_PER_GAME = sum(ALINES_SCORES) * ALIENS_IN_ROW
# the image files for the aliens (bottom to top)
ALIEN_IMAGES = ('alien1', 'alien2', 'alien3')
# the maximum multiplier for multiplying the step size
MAX_COEF_FOR_SPEED = 3


### SHIP CONSTANTS ###

# the width of the ship
SHIP_WIDTH = 44
# the height of the ship
SHIP_HEIGHT = 44
# the distance of the (bottom of the) ship from the bottom of the screen
SHIP_BOTTOM = 32
# The number of pixels to move the ship per update
# self.initial_speed * ((MAX_COEF_FOR_SPEED-1) * (initial_size - self.amount_of_alive) / initial_size + 1)
SHIP_MOVEMENT = int(ALIEN_H_WALK * ((MAX_COEF_FOR_SPEED-1) *
                                    (ALIENS_IN_ROW * ALIEN_ROWS - 1) / (ALIENS_IN_ROW * ALIEN_ROWS) + 1))

# The number of lives a ship has
SHIP_LIVES = 3

### BOLT CONSTANTS ###

# the width of a laser bolt
BOLT_WIDTH = 4
# the height of a laser bolt
BOLT_HEIGHT = 16
# the number of pixels to move the bolt per update
BOLT_SPEED = 30
# Chance for aliens to shoot on every tick
BOLT_RATE = .5


### GAME CONSTANTS ###

# state when the wave is activated and in play
STATE_ACTIVE = 1
# state when the game is complete (won)
STATE_COMPLETE = 2
#: state when the game is complete (lost)
STATE_LOSE = 3
