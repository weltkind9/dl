"""
Subcontroller module for Alien Invaders
"""

import time
import random
import logging

from typing import List

from common.bot import BotInterface
from common.consts import *
from common.models import Ship, Alien, Object, Bolt, GroupOfAliens

class Wave:
    """
    This class controls a single level or wave of Alien Invaders.
    """

    def __init__(self, bot: BotInterface):
        """
        Initializes the application.
        """
        self.bot = bot
        self.ship = Ship()
        self.dline = Object(GAME_WIDTH//2, DEFENSE_LINE,
                            4, GAME_WIDTH, 'rectangle')
        self.aliens = GroupOfAliens(
            n=ALIEN_ROWS, m=ALIENS_IN_ROW, initial_speed=ALIEN_H_WALK)
        self.lose = False
        self.wrong_move = False

        self.score = 0
        self.current_tick = 0
        self.max_ticks = AMOUNT_OF_TICKS

    def get_info_for_bot(self):
        def get_coords(ship): return (ship.x, ship.y)

        aliens = list(map(get_coords, self.aliens.ships))
        aliens_bolts = list(map(get_coords, self.aliens.bolts))
        ship = get_coords(self.ship) if self.ship else None
        ship_bolt = get_coords(self.ship.bolt) if self.ship.bolt else None

        return aliens, aliens_bolts, ship_bolt, ship

    
    def update(self) -> int:
        """
        Main function of updating game.

        Updates ship, aliens, laser bolt, checkes collisions
        """

        time_start = time.time()

        try:
            # GET ANSWER FROM BOT
            move_from_bot = self.bot.make_move(*self.get_info_for_bot())

        except Exception as exc:
            raise Exception(f'Solution got an exception: {exc}')

        if time.time() - time_start > 100500:
            raise Exception(f'The bot reach the timeout in {TIMEOUT} seconds. ' + \
                            f'Running for: {time.time() - time_start} seconds.')
    
        # MOVE SHIP
        try:
            self.ship.move(move_from_bot)
        except ValueError as e:
            self.wrong_move = True
            raise e

        # MOVE ALL ALIENS
        self.aliens.move()

        # MOVE ALL BOLTS
        self.move_bolts()

        # CHECK IF NEED TO REMOVE BOLTS
        self.remove_bolts()

        # CHECK BOLT COLLISIONS
        self.check_collision()

        # ALIENS SHOOT
        self.aliens.make_shoot()

        # CHECK IF ANY ALIEN REACH BOTTOM LINE
        if self.aliens.touch_defense_line:
            logging.info('alien touch the defense line')
            self.lose = True

        # end of update. increase ticks
        self.current_tick += 1
        if self.current_tick > self.max_ticks:
            logging.info(f'All {self.max_ticks} are done')
            self.lose = True

    @property
    def all_bolts(self):
        if self.ship.bolt:
            return self.aliens.bolts + [self.ship.bolt]
        else:
            return self.aliens.bolts

    def move_bolts(self):
        """
        Move all bolts according to their velocity
        """
        for bolt in self.all_bolts:
            bolt.y += bolt.velocity

    def remove_bolts(self) -> int:
        for bolt in self.aliens.bolts:
            if (bolt.y + BOLT_HEIGHT / 2 < 0):
                self.aliens.bolts.remove(bolt)

        if self.ship.bolt:
            if self.ship.bolt.y - BOLT_HEIGHT / 2 > GAME_HEIGHT:
                self.ship.bolt = None
                # remove one point for miss
                self.score -= 1

    def check_collision(self):
        # CHECK ALIEN BOLTS
        for bolt in self.aliens.bolts:
            if self.ship.collides(bolt):
                self.lose = True
                return

        if self.ship.bolt:
            for alien in self.aliens.ships:
                if alien.collides(self.ship.bolt):
                    # Ship hit the emeny
                    score = self.aliens.handle_hit(alien)
                    self.score += score
                    self.ship.bolt = None
                    break
