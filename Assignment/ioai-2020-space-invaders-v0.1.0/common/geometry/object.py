import logging

from .point import Point


class Object:
    def __init__(self, x=0, y=0, height=0, width=0, _type=''):
        self.x = x
        self.y = y
        self.height = height
        self.width = width
        self.type = _type

    @property
    def top_left(self):
        return Point(self.x - self.width / 2, self.y + self.height / 2)

    @property
    def bottom_right(self):
        return Point(self.x + self.width / 2, self.y - self.height / 2)

    def collides(self, other):
        """
        Returns True if other obj collides with this object
        """
        assert isinstance(other, Object)

        return not (self.top_left.x >= other.bottom_right.x
                    or self.bottom_right.x <= other.top_left.x
                    or self.top_left.y <= other.bottom_right.y
                    or self.bottom_right.y >= other.top_left.y)

    def copy(self):
        return Object(self.x, self.y, self.height, self.width, self.type)

    def __str__(self):
        return f"{self.__class__.__name__}{self.x, self.y}"
    
    def __repr__(self):
        return self.__str__()


if __name__ == "__main__":
    o1 = Object(0, 0, 2, 2)
    o2 = Object(2, 2, 2.1, 2.1)
    print(o1.bottom_right, o1.top_left)
    print(o2.bottom_right, o2.top_left)
    print(o1.collides(o2))
    print(o2.collides(o1))
