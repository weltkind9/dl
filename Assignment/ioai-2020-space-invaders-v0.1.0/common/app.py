"""
Primary module for Alien Invaders
"""

import time
import logging

from typing import List

from common.bot import BotInterface
from common.consts import *
from common.wave import Wave, Object


class Invaders:
    """
    The primary controller class for the Alien Invaders application
    """

    def __init__(self, bot: BotInterface):
        """
        Initializes the application.
        """
        logging.info('Initialize game')
        self.state = STATE_ACTIVE
        self.wave = Wave(bot)
        self.score = 0
        self.stop = False

    def update(self):
        """
        Animates a single frame in the game.
        """
        self.update_state()

        if self.state == STATE_ACTIVE:
            self.wave.update()
        elif self.state == STATE_LOSE:
            self.stop = True
        else:
            raise ValueError(f"Not found state {self.state}")
        
        return self.stop

    def get_all_objects(self):
        all_objects: List[Object] = []
        # ALIENS + BOLTS
        all_objects.extend(self.wave.aliens.ships)
        all_objects.extend(self.wave.aliens.bolts)

        # SHIP + BOLT
        all_objects.append(self.wave.ship)
        all_objects.append(self.wave.ship.bolt)

        # DEFENSE LINE
        all_objects.append(self.wave.dline)
        return list(filter(lambda x: x, all_objects))

    def update_state(self):
        if self.wave.lose:
            self.state = STATE_LOSE
