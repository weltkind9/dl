import random

from typing import Tuple, List
import logging

from common.consts import *
from common.geometry import Object


class Bolt(Object):
    def __init__(self, x, y, velocity):
        """
        Initializes the Bolt class.
        """
        super().__init__(x=x, y=y, width=BOLT_WIDTH, height=BOLT_HEIGHT, _type='rectangle')
        self.velocity = velocity


class Ship(Object):
    def __init__(self):
        super().__init__(x=random.randint(SHIP_WIDTH, GAME_WIDTH - SHIP_WIDTH),
                         y=SHIP_BOTTOM, width=SHIP_WIDTH, height=SHIP_HEIGHT, _type='ship')
        self.bolt = None
        logging.debug(f"Created ship: {self}")

    @staticmethod
    def parse_command(command: str) -> Tuple[str, int]:
        if 'left' in command or 'right' in command:
            cmd, arg = command.split()
            return cmd, int(arg)
        elif command == 'pass' or command == 'fire':
            return (command, 0)
        else:
            raise ValueError('Unknown command')

    def move(self, command: str):
        """
        Moves ship according to `commad`

        In case of invalid move, raises an exception
        """
        logging.debug(f"Trying to make move with commad : \"{command}\"")
        cmd, arg = Ship.parse_command(command)

        try:
            if cmd == 'left':
                assert 0 < arg <= SHIP_MOVEMENT, f"Bounds are (0, {SHIP_MOVEMENT}]"
                assert self.x - arg - SHIP_WIDTH/2 >= 0, "Ship has reached left edge"
                logging.debug(f"Move to the left on {arg}")
                self.x -= arg

            elif cmd == 'right':
                assert 0 < arg <= SHIP_MOVEMENT, f"Bounds are (0, {SHIP_MOVEMENT}]"
                assert self.x + arg + SHIP_WIDTH/2 < GAME_WIDTH, "Ship has reached right edge"
                logging.debug(f"Move to the right on {arg}")
                self.x += arg

            elif cmd == 'fire':
                assert not self.bolt, "It is forbidden to shoot while bolt is in play"
                logging.debug("Ship takes a shot")
                self.bolt = Bolt(self.x, self.y + SHIP_HEIGHT /
                                 2 + BOLT_HEIGHT / 2, BOLT_SPEED)

            elif cmd == 'pass':
                logging.debug("Pass the tick")

            else:
                raise ValueError(f"Command not found")

        except (AssertionError, ValueError) as e:
            raise ValueError(f"Invalid move {command}: {e.args[0]}")
    

class Alien(Object):
    def __init__(self, x, y, score, _type):
        """
        Initializes the Alien class
        """
        super().__init__(x=x, y=y, width=ALIEN_WIDTH, height=ALIEN_HEIGHT, _type=_type)
        self.score = score


class GroupOfAliens:
    def __init__(self, n, m, initial_speed, direction=None):
        if not direction:
            self.direction = random.randint(0, 1)  # 0 - right, 1 - left
        
        self.n = n
        self.m = m
        self.ships: List[Alien] = GroupOfAliens.generate_aliens(self.n, self.m)
        self.bolts = []
        self.prev_time = 0
        self.initial_speed = initial_speed
        self.speed = initial_speed
        
        logging.debug(f"Created group of aliens: ({self.most_left_alien.x}, {self.highest_alien.y}) , ({self.most_right_alien.x}, {self.lowest_alien.y})")

    @property
    def amount_of_alive(self):
        return len(self.ships)

    @staticmethod
    def generate_aliens(n, m) -> List[Alien]:
        '''
        Generates list of list of aliens
        '''
        list_of_aliens = []
        y = GAME_HEIGHT - (ALIEN_CEILING + ALIEN_HEIGHT/2)
        for row_number in range(n):
            x = (GAME_WIDTH - m * (ALIEN_H_SEP + ALIEN_WIDTH)) // 2 + ALIEN_H_SEP
            alien_type = ((ALIEN_ROWS - 1 - row_number) //
                          2) % len(ALIEN_IMAGES)

            for _ in range(m):
                alien = Alien(
                    x=x,
                    y=y,
                    score=ALINES_SCORES[row_number],
                    _type=ALIEN_IMAGES[alien_type],
                )
                list_of_aliens.append(alien)

                # next column
                x += ALIEN_H_SEP + ALIEN_WIDTH

            # next row
            y -= ALIEN_V_SEP + ALIEN_HEIGHT

        return list_of_aliens

    def get_most_distant_alien(self, reverse=True, x=True, rnd=True) -> Alien:
        """
        RETURNS the highest or lowest and most left or right alien 
        """
        def get_coord(alien):
            return alien.x if x else alien.y

        sorted_aliens = sorted(
            self.ships, key=lambda alien: get_coord(alien), reverse=reverse)

        top_aliens = list(filter(lambda alien: get_coord(
            alien) == get_coord(sorted_aliens[0]), sorted_aliens))

        if rnd:
            return random.choice(top_aliens)
        else:
            return top_aliens[0]

    @property
    def highest_alien(self) -> Alien:
        return self.get_most_distant_alien(reverse=True, x=False)

    @property
    def lowest_alien(self) -> Alien:
        return self.get_most_distant_alien(reverse=False, x=False)

    @property
    def most_left_alien(self):
        return self.get_most_distant_alien(reverse=False, x=True)

    @property
    def most_right_alien(self):
        return self.get_most_distant_alien(reverse=True, x=True)

    @property
    def touch_right_edge(self):
        return (GAME_WIDTH - ALIEN_H_SEP) <= (self.most_right_alien.x + ALIEN_WIDTH/2)

    @property
    def touch_left_edge(self):
        return (self.most_left_alien.x - ALIEN_WIDTH//2) <= ALIEN_H_SEP

    def move(self):
        '''
        Moves group of aliens, if delta time is sufficiently large
        '''
        self.__move_left_or_right(self.direction)

    def __move_left_or_right(self, direction):
        if (self.direction == 0 and self.touch_right_edge) or \
                (self.direction == 1 and self.touch_left_edge):

            # Make down move and change direction
            self.__move_down()
            self.direction = int(not self.direction)

        elif self.direction == 0 and not self.touch_right_edge:
            for alien in self.ships:
                alien.x += self.speed

        elif self.direction == 1 and not self.touch_left_edge:
            for alien in self.ships:
                alien.x -= self.speed

    def __move_down(self):
        logging.debug('Move group of aliens down')
        for alien in self.ships:
            alien.y -= ALIEN_V_WALK

    def make_shoot(self):
        if random.random() > BOLT_RATE:
            return

        alien = random.choice(self.ships)
        logging.debug('Make shoot from group of aliens')
        bolt_x, bolt_y = alien.x, alien.y - ALIEN_HEIGHT / 2
        bolt = Bolt(bolt_x, bolt_y, -BOLT_SPEED)
        self.bolts.append(bolt)

    def handle_hit(self, alien) -> int:
        '''
        Returns amount of scores for hit
        '''
        score = alien.score
        self.ships.remove(alien)
        initial_size = self.n * self.m
        self.speed = self.initial_speed * \
            ((MAX_COEF_FOR_SPEED-1) * (initial_size -
                                       self.amount_of_alive) / initial_size + 1)
        logging.debug(f"Collision! Remove alien from the game and set speed to {self.speed}")
        return score

    @property
    def touch_defense_line(self) -> bool:
        return self.lowest_alien.y - SHIP_HEIGHT/2 <= DEFENSE_LINE
