import time
import pygame
import logging

from common.app    import Invaders
from common.bot    import BotInterface as IBot
from common.models import Object
from common.consts import GAME_WIDTH, GAME_HEIGHT, LOG_LEVEL

class DefaultBot(IBot):
    pressed_keys = []
    
    def make_move(self, aliens:list, aliens_bolts:list, ship_bolt:tuple, ship:tuple):
        if not ship_bolt and DefaultBot.pressed_keys[pygame.K_SPACE]:
            return 'fire'
        
        if DefaultBot.pressed_keys[pygame.K_LEFT]:
            return 'left 20'
        
        if DefaultBot.pressed_keys[pygame.K_RIGHT]:
            return 'right 20' 
        
        return 'pass'

class Vizualizator:
    IMAGES_PATH = './Images/%s.png'

    def __init__(self, update: callable, get_coordinates: callable, **kwargs: dict) -> object:
        self.resolution = (kwargs.pop('width',  GAME_WIDTH),
                           kwargs.pop('height', GAME_HEIGHT))

        self.fps = kwargs.pop('fps', 5)
        # Pink.
        self.background = kwargs.pop('background', (227, 100, 202))

        # Purple.
        self.bullet_color = kwargs.pop('bullet_color', (125, 3, 156))

        if kwargs:
            raise ValueError(f'Bad keywords: {kwargs}')

        # Delegates.
        self.update = update
        self.get_coordinates = get_coordinates

        # Optimization.
        self.__cache = {}

    def loop(self) -> None:
        display = pygame.display.set_mode(self.resolution)
        clock = pygame.time.Clock()

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return
                        
            display.fill(self.background)

            for obj in self.get_coordinates():
                rect = pygame.Rect(
                    obj.top_left.x, self.resolution[1] - obj.top_left.y, obj.width, obj.height)

                # Bullet
                if obj.type == 'rectangle':
                    pygame.draw.rect(display, self.bullet_color, rect)

                    continue

                # Miss cache.
                if obj.type not in self.__cache:
                    self.__cache[obj.type] = pygame.image.load(
                        Vizualizator.IMAGES_PATH % obj.type).convert_alpha()

                display.blit(self.__cache[obj.type], rect)

            pygame.display.flip()
            
            DefaultBot.pressed_keys = pygame.key.get_pressed()
            
            if self.update():
                return
          
            clock.tick(self.fps)
            #time.sleep(0.2)


if __name__ == '__main__':
    LOG_FORMAT = (
        '%(levelname) -10s %(asctime)s %(name) -15s %(funcName) -20s: %(message)s')
    LOG_LEVEL = logging.DEBUG if LOG_LEVEL == 'debug' else logging.INFO
    logging.basicConfig(level=LOG_LEVEL, format=LOG_FORMAT)
    
    try:
        from solution import Bot
        
        bot = Bot()
        
    except Exception as ex:
        logging.info('Use default bot.')
        
        bot  = DefaultBot()
        
    game = Invaders(bot)

    try:
        Vizualizator(game.update, game.get_all_objects).loop()

    except Exception as exc:
        print(f'Exception: {exc}')

    finally:
        print(f'SCORE: {game.wave.score} points.')

        pygame.quit()
