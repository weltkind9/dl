import random

from common.bot import BotInterface

class Bot(BotInterface):
    def __init__(self, *args, **kwargs):
        self.step=1
        return super().__init__(*args, **kwargs)

    def checkSavePosition2(self, x, aliens_bolts):
        for ab in aliens_bolts:
            if abs(ab[0] - x) < 26 and ab[1] < 150:
                #print(f'danger bolt: {ab} ship: {x}')
                return False
        return True

    def checkSavePosition(self, x, aliens_bolts):
        for ab in aliens_bolts:
            if abs(ab[0] - x) < 26 and ab[1] < 180:
                #print(f'danger bolt: {ab} ship: {x}')
                return False
        return True

    def aliens_count(self, aliens):
        try:
            aliensDict={}
            for a in aliens:
                if a[0] not in aliensDict:
                    aliensDict[a[0]] = 1
                else:
                    aliensDict[a[0]] += 1
            vls = list(aliensDict.values())
            res=[]
            for i in range(0, len(vls), 4):
                res.append(vls[i]+vls[i+1]+vls[i+2]+vls[i+3])
            return res[0], res[1], res[2]
        except Exception as exc:
            print(exc)
            return 0,0,0

        m = int(ship[0] / 26)
        s = aliensDict[m]
        for step in range(5):
            if m - step in aliensDict:
                s += aliensDict[m-step]
            if m + step in aliensDict:
                s += aliensDict[m+step]

        #print(f'alien count: {s}')
        sL=0; sR=0
        if s < 15:
            for k in aliensDict:
                if k < m: sL += aliensDict[k]
                if k > m: sR += aliensDict[k]
            if sL < sR:
                #print(f'{sL} < {sR} alien count: right')
                if self.checkSavePosition(ship[0]+26, aliens_bolts):
                    #print('right')
                    return 'right 26'
            else:
                #print(f'{sL} > {sR}  alien count: left')
                if self.checkSavePosition(ship[0]-26, aliens_bolts):
                    #print('left')
                    return 'left 26'

    def aliens_count2(self, aliens, ship):
        aliensDict={'l':0, 'c':0, 'r':0}
        for a in aliens:
            if abs(a[0] - ship[0]) < 18+16+36+8+3: aliensDict['c'] += 1
            elif a[0] < ship[0]: aliensDict['l'] += 1
            else: aliensDict['r'] += 1

        return aliensDict['l'], aliensDict['c'], aliensDict['r']
 
    def make_move(self, aliens, aliens_bolts, ship_bolt, ship):
        try:
            ##print('ship:')
            #print(ship)

            c = self.checkSavePosition2( ship[0], aliens_bolts)
            if not c:
                step = 26
                while 1:
                    if ship[0] + step > 800 and ship[0] - step <0:
                        break
                    if ship[0] - step > 0:
                        c1 = self.checkSavePosition(ship[0] - step, aliens_bolts)
                        if c1:
                            #print('left')
                            return 'left 26'
                    if ship[0] + step < 800:
                       c2 = self.checkSavePosition( ship[0] + step, aliens_bolts)
                       if c2:
                           #print('right')
                           return 'right 26'
                    step += 26


            if not ship_bolt:
                return 'fire'

            l, c, r = self.aliens_count2(aliens, ship)
            
            if ship[0] > 173 and ship[0] < 217:
                if c < 6 and self.step == 1:
                    print('step 2')
                    self.step = 2
                elif c < 2 and self.step == 4:
                    print('step 5')
                    self.step = 5

            elif ship[0] > 378 and ship[0] < 422:
                if c < 8 and self.step == 2:
                    print('step 3')
                    self.step = 3
                elif c < 2 and self.step == 5:
                    print('step 6')
                    self.step = 6

            elif ship[0] > 568 and ship[0] < 602:
                if  c < 2 and self.step == 3:
                    print('step 4')
                    self.step = 4
                elif  c < 2 and self.step == 6:
                    print('step 7')
                    self.step = 7

    
            if ship[0] < 190 and self.checkSavePosition(ship[0]+26, aliens_bolts):
                return 'right 26'
            if  (self.step == 1 or self.step == 4 or self.step == 7) and ship[0] > 200 and self.checkSavePosition(ship[0]-26, aliens_bolts):
                return 'left 26'

            if (self.step == 2 or self.step == 5) and ship[0] < 395 and self.checkSavePosition(ship[0]+26, aliens_bolts):
                return 'right 26'
            if  (self.step == 2 or self.step == 5) and ship[0] > 405 and self.checkSavePosition(ship[0]-26, aliens_bolts):
                return 'left 26'

            if (self.step == 3 or self.step == 6) and ship[0] < 580 and self.checkSavePosition(ship[0]+26, aliens_bolts):
                return 'right 26'
            if  (self.step == 3 or self.step == 6) and ship[0] > 595 and self.checkSavePosition(ship[0]-26, aliens_bolts):
                return 'left 26'

            return 'pass'

            print(6)
        except Exception as exc:
            print(exc)
            return 'pass'