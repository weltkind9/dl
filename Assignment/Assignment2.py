import time

class Position():
    def __init__(self, field, r, c, path, cost, chestDist, fallingSecond):
        self.end = False
        self.field = field                # current game screen
        self.fieldStr=''
        for _r in range(R):
            for _c in range(C): self.fieldStr+=field[_r][_c]
        self.jc = c                       # current position of Jety, int
        self.jr = r                       # current position of Jety, int
        self.path = path                  # path to current game screen, string
        self.cost = cost                  # summary duration for all moves in path
        self.chestDist = chestDist        # distance to goal
        self.fallingSecond = fallingSecond
        self.l = None                       #left neighbor in sorted list
        self.r = None                       #rigth neighbor in sorted list
        self.f = self.cost + self.chestDist
        self.discovered = False

    def __str__(self):
        return f'{self.path} (r{self.jr},c{self.jc}) cost {self.cost}'
    def __eq__(self, other):
        if other == None:
            return False
        return self.fieldStr == other.fieldStr and self.jr == other.jr and self.jc == other.jc

startPos = None

def arrCopy(a, R,C):
    cc=[]
    for r in range(R):
        cc.append([])
        for c in range(C):
            cc[r].append(a[r][c])
    return cc


def IsVisitable(a, r , c, R, C):
    if r < 0 or c < 0 or r >= R or c >= C:
        return False
    if a[r][c] == '%' or a[r][c] =='*':
        return False
    return True

def NewCost(f, t, dir):
    if f == ' ' and dir == 'D': return 1#the fall to one lower segment takes 1 second and (t == ' ' or t == '@' or t == '-')
    if f == ' ' and (dir == 'L' or dir == 'R') and t != '#': return 1.5 #the move to the left or to the right from an empty segment takes 1.5 seconds
    if f == '-' and (dir == 'L' or dir == 'R'): return 2 #the move to the left or to the right from a chain road segment takes 2 seconds
    if f == '-' and dir == 'D': return 1 #the move to the lower (empty) segment from a chain road segment takes 1 second
    if f == ' ' and (dir == 'L' or dir == 'R') and t == '#': return 2.5 #the move to the lower rope-ladder segment takes 2.5 seconds
    if f == '#' and (dir == 'D' or dir == 'U') and t == '#': return 2.5 #the move to the lower or upper rope-ladder segment from a rope-ladder segment takes 2.5 seconds
    if f == '#' and (dir == 'D' or dir == 'U'): return 2.5 #the move to the lower or upper non rope-ladder segment from a rope-ladder segment takes 2.5 seconds
    if f == '#' and (dir == 'L' or dir == 'R') and t != '-': return 1.5 #the move to the left or to the right from a rope-ladder segment takes 1.5 seconds
    if f == '#' and (dir == 'L' or dir == 'R') and t == '-': return 2 #the move to the left or to the right from a rope-ladder segment to a chain road segment takes 2 seconds
    if t == '*': return 1 #the action to break the brick wall takes 1 second. If the wall is being broken when Jety is falling these two actions takes 1 second
    raise Exception()
def findNextPosition(queue):
    for q in queue:
        if q.discovered == False:
            return q
    return None
        
def work(currPos, R, C, r, c, chestR, chestC, fields, result, depth, queue):

    for rr in result:
        if result[rr].cost <= currPos.cost:
            currPos.discovered = True
            nextPos = findNextPosition(queue)
            if nextPos == None:
                return
            work(nextPos, R, C, nextPos.jr, nextPos.jc, chestR, chestC, fields, result, depth+1, queue)

    if (r,c) in fields:
        prevF = fields[(r,c)]
        if currPos.fieldStr in prevF:
            prevPos = prevF[currPos.fieldStr]
            if currPos.cost > prevPos.cost:
                currPos.discovered = True
                nextPos = findNextPosition(queue)
                if nextPos == None:
                    return
                work(nextPos, R, C, nextPos.jr, nextPos.jc, chestR, chestC, fields, result, depth+1, queue)



            else:
                prevF[currPos.fieldStr] = currPos
        else:
            prevF[currPos.fieldStr] = currPos
    else:
        fields[(r,c)] = {}
        fields[(r,c)][currPos.fieldStr] = currPos


    a = currPos.field
    newPositions = []



    
    if r < R-1 and a[r][c] == ' ' and (a[r+1][c] == ' ' or a[r+1][c] == '-' or a[r+1][c] == '@'):     #falling
        newChestDist = abs(c - chestC) + abs(r + 1 - chestR)
        if c-1 > 0 and a[r+1][c-1] == '*' and a[r][c-1] == ' ':
            nextField = arrCopy(a, R,C)
            nextField[r+1][c-1] = ' '
            nextPos = Position(nextField, r + 1, c, currPos.path + f'{currPos.fallingSecond}/', currPos.cost + 1, newChestDist, currPos.fallingSecond+1)
            newPositions.append(nextPos)
        if c < C-1 and a[r+1][c+1] == '*' and a[r][c+1] == ' ':
            nextField = arrCopy(a, R,C)
            nextField[r+1][c+1] = ' '
            nextPos = Position(nextField, r + 1, c, currPos.path + f'{currPos.fallingSecond}\\', currPos.cost + 1, newChestDist, currPos.fallingSecond+1)
            newPositions.append(nextPos)

        nextPos = Position(a, r + 1, c, currPos.path, currPos.cost + 1, newChestDist, currPos.fallingSecond+1)
        newPositions.append(nextPos)

    else:
        #блок1 ищем соседей справа и слева
        vl = IsVisitable(a, r, c - 1, R, C)
        if vl == True:
            newChestDist = abs(c - 1 - chestC) + abs(r - chestR)
            newCost = NewCost(a[r][c], a[r][c-1], 'L')
            nextPos = Position(a, r, c - 1, currPos.path + 'L', currPos.cost + newCost, newChestDist, 0)
            newPositions.append(nextPos)

        vr = IsVisitable(a, r, c + 1, R, C)
        if vr == True:
            newChestDist = abs(c + 1 - chestC) + abs(r - chestR)
            newCost = NewCost(a[r][c], a[r][c+1], 'R')
            nextPos = Position(a, r, c + 1, currPos.path + 'R', currPos.cost + newCost, newChestDist, 0)
            newPositions.append(nextPos)
        #блок1
        #блок2 ищем соседей снизу для цепи
        if a[r][c] == '-':
            vd = IsVisitable(a, r + 1, c, R, C)
            if vd == True:      
                newChestDist = abs(c - chestC) + abs(r + 1 - chestR)
                newCost = NewCost(a[r][c], a[r+1][c], 'D')
                nextPos = Position(a, r+1, c, currPos.path + 'D', currPos.cost + newCost, newChestDist, 0)
                newPositions.append(nextPos)
        #блок2
        #блок3 ищем соседей сверху и снизу для лестницы
        if a[r][c] == '#':
            vd = IsVisitable(a, r+1, c, R, C)
            if vd == True:
                newChestDist = abs(c - chestC) + abs(r + 1 - chestR)
                newCost = NewCost(a[r][c], a[r+1][c], 'D')
                nextPos = Position(a, r+1, c, currPos.path + 'D', currPos.cost + newCost, newChestDist, 0)
                newPositions.append(nextPos)
                

            vu = IsVisitable(a, r-1, c, R, C)
            if vu == True:      
                newChestDist = abs(c - chestC) + abs(r - 1 - chestR)
                newCost = NewCost(a[r][c], a[r-1][c], 'U')
                nextPos = Position(a, r-1, c, currPos.path + 'U', currPos.cost + newCost, newChestDist, 0)
                newPositions.append(nextPos)
        #блок3
        #блок4 ищем соседей снизу для пустой клетки с лестницей внизу
        if r < R-1 and a[r+1][c] == '#':
            newChestDist = abs(c - chestC) + abs(r + 1 - chestR)
            newCost = NewCost(a[r][c], a[r+1][c], 'D')
            nextPos = Position(a, r+1, c, currPos.path + 'D', currPos.cost + newCost, newChestDist, 0)
            newPositions.append(nextPos)
        #блок4


        if r < R-1 and c-1 > 0 and a[r+1][c-1] == '*' and a[r][c-1] == ' ':
            nextField = arrCopy(a, R,C)
            nextField[r+1][c-1] = ' '
            nextPos = Position(nextField, r, c, currPos.path + '/', currPos.cost + 1, newChestDist, currPos.fallingSecond)
            newPositions.append(nextPos)
        if r < R-1 and c < C-1 and a[r+1][c+1] == '*' and a[r][c+1] == ' ':
            nextField = arrCopy(a, R,C)
            nextField[r+1][c+1] = ' '
            nextPos = Position(nextField, r, c, currPos.path + '\\', currPos.cost + 1, newChestDist, currPos.fallingSecond)
            newPositions.append(nextPos)
 
    #удаляем текущий элемент из очереди
    currPos.discovered = True

    for np in newPositions:
        if np.jr == chestR and np.jc == chestC:
            np.end = True
            np.discovered = True
            result[np.path] = np
        for i in range(len(queue)):
            if np.chestDist == queue[i].chestDist:
                if np == queue[i]:
                    break
            if np.f < queue[i].f:
                queue.insert(i, np)
                break
        else:
            queue.append(np)

    nextPos = findNextPosition(queue)
    if nextPos == None:
        return
   # nps = sorted(newPositions, key=lambda pos: pos.cost+pos.chestDist)
  #  for np in nps:
  #      if np.end == True:
  #          continue
    work(nextPos, R, C, nextPos.jr, nextPos.jc, chestR, chestC, fields, result, depth+1, queue)


def start(startField, C, R, startR, startC, result):
    for r in range(R):
        for c in range(C):
            if a[r][c] == '@':
                chestR = r
                chestC = c
    chestDist = abs(startR-chestR) + abs(startC- chestC)
    global startPos
    startPos = Position(startField,startR ,startC, '', 0, chestDist, 0)
    fds={}
    queue=[]
    queue.append(startPos)
    work(startPos, R, C, startR, startC, chestR, chestC, fds, result, 1, queue)

C, R, c, r = list(map(int, input().split()))
a=[]
for _ in range(R):
    a.append(list(input()))

t1 = time.time()
result = {}
start(a, C, R, r, c, result)
nps = dict( sorted(result.items(), key=lambda item: item[1].cost))
print(list(nps.values())[-1].path)
#print(time.time() - t1)
#if len(result) > 0:
#    nps =dict( sorted(result.items(), key=lambda item: item[1].cost))
#    print(list(nps.values())[-1].path)
#else:
#    print()